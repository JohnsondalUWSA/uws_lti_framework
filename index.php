<?php

use League\Plates\Engine;
use Analog\Analog;

require 'vendor/autoload.php';

require_once('settings.php');
require_once('lib/encrypt/key.php');
require_once('lib/encrypt/uws_encrypt.php');

require_once ('lib/uws_canvas.php');
require_once ('lib/logger.php');

session_name("UWS");
session_set_cookie_params(
    ['secure'=>true,
        'samesite'=> 'None'
    ]
);

session_start();

header('Content-Type: text/html; charset=utf-8');

// anitize the post params!
$post_input = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
$post_input['custom_canvas_user_id'] = filter_input(INPUT_POST, 'custom_canvas_user_id', FILTER_SANITIZE_NUMBER_INT);
$post_input['custom_canvas_course_id'] = filter_input(INPUT_POST, 'custom_canvas_course_id', FILTER_SANITIZE_NUMBER_INT);

// Using UCF Templates to display error messages to users and other general items.
$templates = new League\Plates\Engine('templates');

// Init Logger Class
$logger = new uws_logger($mysql_server, $mysql_db,$mysql_user, uws_encrypt::encrypt_decrypt('decrypt',$mysql_password,$mykey));

// Is Request a post?  If not send user a message
if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
    // The request is using the POST method

    $render_params = [
        'msg' => "\"LTI TOOL MUST USE LTI TO LAUNCH! \""
    ];
    echo($templates->render('msg', $render_params));
   $logger->launchError();
    exit();
}

if ( isset($post_input["oauth_consumer_key"]) || ! isset($_SESSION["valid"])) {
   $_SESSION["valid"]= false; // force BLTI verification
}


// Validate LTI and store Launch Params in session
if ($_SESSION['valid'] == false) {
    require_once('lib/ims-blti/blti.php');

    // Initialize, all secrets are 'secret', do not set session, and do not redirect
    $context = new BLTI($consumer_key, $shared_secret, false, false);

    if ( ! $context->valid) {
        // Authentication error
        $render_params = [
            'msg' => "An error occurred, please refresh and try again. If this error persists, please contact support."
        ];
        echo($templates->render('msg', $render_params));
        $logger->ltiAuthError(serialize($context));
    } else {


        $_SESSION['launch_params']['custom_canvas_user_id'] = $post_input['custom_canvas_user_id'];
        $_SESSION['launch_params']['custom_canvas_course_id'] = $post_input['custom_canvas_course_id'];
        $_SESSION['launch_params']['custom_canvas_user_login_id'] = $post_input['custom_canvas_user_login_id'];
        $_SESSION['launch_params']['roles'] = $post_input['roles'];
        $_SESSION['launch_params']['UWSAToolName'] = isset($post_input['custom_uwsa_tool_name'])?$post_input['custom_uwsa_tool_name']:null;
        $_SESSION['OAuthDomain'] = $post_input['custom_canvas_api_domain'];
        $_SESSION['OAuthKey'] = $post_input['oauth_signature'];

        $_SESSION['valid'] = true;
        $_SESSION['post'] = $post_input;


        session_commit();

         //create array of sanitized inputs to add to log
        $render_params = [
            "valid" =>true,
            "user_id" => $_SESSION['launch_params']['custom_canvas_user_id'],
            "course_id" => $_SESSION['launch_params']['custom_canvas_course_id'],
            "roles" => $_SESSION['launch_params']['roles'],
            "UWSAToolName" => $_SESSION['launch_params']['UWSAToolName']

        ];

        // Combine sanitized inputs with post_inputs
        $render_params = array_merge($render_params,$post_input);

        // Determine PHP Page too load
        $page = $_SESSION['launch_params']['UWSAToolName'] != null?$_SESSION['launch_params']['UWSAToolName']:'enroll';

        // Note Logger Calls inits Static Analog Class.  Call Analog direct to log events.
        Analog::log("Tool Launch:".$page.": LTI Info:".serialize($render_params), Analog::DEBUG);

        // Load page via require
        require ('pages/'.$page.".php");




    }
}

?>