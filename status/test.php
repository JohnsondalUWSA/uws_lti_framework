<?php

require '../vendor/autoload.php';

$xlsx = SimpleXLSX::parse('DigtialLearningExternalTools.xlsx');
$headerRow = 0;



//print_r ($xlsx->rows());

echo '<pre>';
if ( $xlsx = SimpleXLSX::parse( 'DigtialLearningExternalTools.xlsx' ) ) {
    foreach ( $xlsx->rows() as $r => $row ) {
        foreach ( $row as $c => $cell ) {
            echo ($c > 0) ? ', ' : '';
            echo ( $r === 0 ) ? '<b>'.$cell.'</b>' : $cell;
        }
        echo '<br/>';
    }
} else {
    echo SimpleXLSX::parseError();
}
echo '</pre>';