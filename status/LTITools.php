<?php

require '../vendor/autoload.php';

$xlsx = SimpleXLSX::parse('DigtialLearningExternalTools.xlsx');

$headerRow = 0;

$colIDStatus = array_search("STATUS",$xlsx->rows()[$headerRow]);
$colIDProgress = array_search("PROGRESS",$xlsx->rows()[$headerRow]);
$colIDExternalTool = array_search("EXTERNAL TOOL",$xlsx->rows()[$headerRow]);
$colIDKBID =  array_search("KB ID",$xlsx->rows()[$headerRow]);
$colIDDateRequested  = array_search("DATE REQUESTED",$xlsx->rows()[$headerRow]);
$colIDLastUpdated = array_search("LAST UPDATE",$xlsx->rows()[$headerRow]);
$colIDLicenseType =    array_search("LICENSE TYPE",$xlsx->rows()[$headerRow]);
$colIDCostModel   = array_search("COST MODEL",$xlsx->rows()[$headerRow]);

$colIDComments  = array_search("COMMENTS",$xlsx->rows()[$headerRow]);
$colIDVendorLink = array_search("VENDORLINK",$xlsx->rows()[$headerRow]);

$kbLink = "https://kb.wisconsin.edu/dle/";






?>

<html>
<head>
    <script
            src="https://code.jquery.com/jquery-3.4.1.min.js"
            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
            crossorigin="anonymous"></script>

    <script
            src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
            integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
            crossorigin="anonymous"></script>
    <link href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet" />

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.22/af-2.3.5/b-1.6.4/b-colvis-1.6.4/b-html5-1.6.4/b-print-1.6.4/cr-1.5.2/fc-3.3.1/fh-3.1.7/r-2.2.6/rg-1.1.2/rr-1.2.7/sc-2.0.3/sp-1.2.0/sl-1.3.1/datatables.min.css"/>

    <link rel="stylesheet" type="text/css" href="progress.css" />

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.22/af-2.3.5/b-1.6.4/b-colvis-1.6.4/b-html5-1.6.4/b-print-1.6.4/cr-1.5.2/fc-3.3.1/fh-3.1.7/r-2.2.6/rg-1.1.2/rr-1.2.7/sc-2.0.3/sp-1.2.0/sl-1.3.1/datatables.min.js"></script>

    <script>
        function GetProgress (percent)
        {
            let color = "blue";

            switch (true)
            {

                case (percent > 25 && percent < 51):
                    color = "blue";
                    break;
                case (percent > 50 && percent < 76):
                    color = "orange";
                    break;
                case (percent > 75 ):
                    color = "green";
                    break;
                default:
                    color = "pink";
                    break;

            }

            let progress = '<div class="c100 p' + percent.toString() + " " + color + '"> <span>' + percent.toString() + '%</span> <div class="slice"> <div class="bar"></div> <div class="fill"></div> </div> </div>';

            //progress = progress.replace("@percent",percent.toString());

            //progress = progress.replace("@color", color);

            return progress;


        }

        $(document).ready( function () {
            $('#tbTools').DataTable(
                {"paging":false,
                    fixedHeader:true,

                    "columns": [

                        {"data" :"Status", "title":"Status",
                            "render": function (oObj, type, full, meta) {
                                var rtn = full["Status"];

                                switch  (full["Status"])
                                {
                                    case "1 - Approved":
                                        rtn =  "<p hidden>1 - Approved</p><img src='green.png' alt='1 - Approved' width='32' height='32'>";
                                        break;
                                    case "2 - Final Review":
                                        rtn =  "<p hidden>2 - Final Review</p>" + GetProgress(full["PROGRESS"]);
                                        break;

                                    case "3 - Functional Testing":
                                        rtn =  "<p hidden>3 - Functional Testing</p>" + GetProgress(full["PROGRESS"]);
                                        break;

                                    case "4 - Intake Review":
                                        rtn =  "<p hidden>4 - Intake Review</p>" + GetProgress(full["PROGRESS"]);
                                        break;

                                    case "5 - Hold":
                                        rtn =  "<p hidden>5 - Hold</p><img src='red.png' alt='5 - Hold' width='32' height='32'>";
                                        break;
                                    default:
                                        break;
                                }

                                return rtn;

                            }



                        },
                        {"data":"PROGRESS", "visible":false}

                        ,
                        {"data" :"EXTERNAL TOOL", "title":"Tool Name"},
                        {"data" :"DATE REQUESTED", "title":"Requested"},
                        {"data" :"LICENSE TYPE", "title":"License Type"},
                        {"data" :"COST MODEL", "title":"Cost Type"},

                        {"data" :"COMMENTS", "title":"Comments"}]}

            );
        } );

    </script>
</head>

<body>

<table id="tbTools" class="display" style="width: 2008px; height: 639px;">
    <thead>

    <th>Status</th>
    <th>PROGRESS</th>
    <th style="text-align:left;">EXTERNAL TOOL</th>
    <th style="text-align:left;">DATE REQUESTED</th>

    <th style="text-align:left;">LICENSE TYPE</th>
    <th style="text-align:left;">COST MODEL</th>

    <th style="text-align:left;">COMMENTS</th>

    </thead>
    <tbody>
    <?php

    $i = 0;
    foreach ( $xlsx->rows() as $r => $row ) {
        if ($i > $headerRow)
        {
            echo ("<tr>");
            echo ("<td style='text-align: center'>".$row[$colIDStatus]."</td>");
            echo ("<td>".$row[$colIDProgress]."</td>");
            if ($row[$colIDKBID] == "")
                echo ("<td>".$row[$colIDExternalTool]."</td>");
            else
                echo ("<td>".$row[$colIDExternalTool]."&nbsp;<a href='".$kbLink.$row[$colIDKBID]."' target='_blank'><img src='document-32.png' alt='".$kbLink.$row[$colIDKBID]."' style='width:16px; height:16px;'></a>");

            echo ("<td>".$row[$colIDDateRequested]."</td>");
            echo ("<td>".$row[$colIDLicenseType]."</td>");
            echo ("<td>".$row[$colIDCostModel]."</td>");

            echo ("<td>".$row[$colIDComments]."</td>");
            echo ("</tr>");
        }
        $i++;

    }

    ?>


    </tbody>


</table>
</body>
</html>