<?php
?>

<html>
<head>
    <script
            src="https://code.jquery.com/jquery-3.4.1.min.js"
            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
            crossorigin="anonymous"></script>

    <script
            src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
            integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
            crossorigin="anonymous"></script>
    <link href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet" />

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.22/af-2.3.5/b-1.6.4/b-colvis-1.6.4/b-html5-1.6.4/b-print-1.6.4/cr-1.5.2/fc-3.3.1/fh-3.1.7/r-2.2.6/rg-1.1.2/rr-1.2.7/sc-2.0.3/sp-1.2.0/sl-1.3.1/datatables.min.css"/>

    <link rel="stylesheet" type="text/css" href="progress.css" />

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.22/af-2.3.5/b-1.6.4/b-colvis-1.6.4/b-html5-1.6.4/b-print-1.6.4/cr-1.5.2/fc-3.3.1/fh-3.1.7/r-2.2.6/rg-1.1.2/rr-1.2.7/sc-2.0.3/sp-1.2.0/sl-1.3.1/datatables.min.js"></script>

    <script>
        function GetProgress (percent)
        {
            let color = "blue";

            switch (true)
            {

                case (percent > 25 && percent < 51):
                    color = "blue";
                    break;
                case (percent > 50 && percent < 76):
                    color = "orange";
                    break;
                case (percent > 75 ):
                    color = "green";
                    break;
                default:
                    color = "red";
                    break;

            }

            let progress = '<div class="c100 p' + percent.toString() + " " + color + '"> <span>' + percent.toString() + '%</span> <div class="slice"> <div class="bar"></div> <div class="fill"></div> </div> </div>';

            //progress = progress.replace("@percent",percent.toString());

            //progress = progress.replace("@color", color);

            return progress;


        }




    </script>
</head>

<body>

<table>
    <tr><td><div class="c100 p24 pink"> <span>0-24%</span> <div class="slice"> <div class="bar"></div> <div class="fill"></div> </div> </div></td></tr>
    <tr><td><div class="c100 p45 blue"> <span>25-49%</span> <div class="slice"> <div class="bar"></div> <div class="fill"></div> </div> </div></td></tr>
    <tr><td><div class="c100 p70 orange"> <span>50-74%</span> <div class="slice"> <div class="bar"></div> <div class="fill"></div> </div> </div></td></tr>
    <tr><td><div class="c100 p90 green"> <span>75-99%</span> <div class="slice"> <div class="bar"></div> <div class="fill"></div> </div> </div></td></tr>


</table>
</body>
</html>