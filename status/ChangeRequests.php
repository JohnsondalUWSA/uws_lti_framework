<?php

require '../vendor/autoload.php';

$xlsx = SimpleXLSX::parse('Change Request Log.xlsx');

$headerRow = 3;

$colIDChange = array_search("Change", $xlsx->rows()[$headerRow]);
$colIDComments = array_search("Comments", $xlsx->rows()[$headerRow]);
$colIDRequestDate = array_search("Request Submission Date", $xlsx->rows()[$headerRow]);
$colIDStatus = array_search("Status (Recommend, Input - SM, Input - AC, Decide, Execute/Not Approved/Hold)", $xlsx->rows()[$headerRow]);
$colIDApplication = array_search("Application", $xlsx->rows()[$headerRow]);
$colIDStatusP = array_search("Status Percent", $xlsx->rows()[$headerRow]);
$colIDImplementedDate = array_search("Date Change Implemented", $xlsx->rows()[$headerRow]);


$kbLink = "https://kb.wisconsin.edu/dle/";


?>

<html>
<head>
    <script
            src="https://code.jquery.com/jquery-3.4.1.min.js"
            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
            crossorigin="anonymous"></script>

    <script
            src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
            integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
            crossorigin="anonymous"></script>
    <link href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet"/>

    <link rel="stylesheet" type="text/css"
          href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.22/af-2.3.5/b-1.6.4/b-colvis-1.6.4/b-html5-1.6.4/b-print-1.6.4/cr-1.5.2/fc-3.3.1/fh-3.1.7/r-2.2.6/rg-1.1.2/rr-1.2.7/sc-2.0.3/sp-1.2.0/sl-1.3.1/datatables.min.css"/>

    <link rel="stylesheet" type="text/css" href="progress.css"/>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript"
            src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.22/af-2.3.5/b-1.6.4/b-colvis-1.6.4/b-html5-1.6.4/b-print-1.6.4/cr-1.5.2/fc-3.3.1/fh-3.1.7/r-2.2.6/rg-1.1.2/rr-1.2.7/sc-2.0.3/sp-1.2.0/sl-1.3.1/datatables.min.js"></script>

    <script>
        function GetProgress(percent) {
            let color = "blue";

            switch (true) {

                case (percent > 25 && percent < 51):
                    color = "blue";
                    break;
                case (percent > 50 && percent < 76):
                    color = "orange";
                    break;
                case (percent > 75):
                    color = "green";
                    break;
                default:
                    color = "pink";
                    break;

            }

            let progress = '<div class="c100 p' + percent.toString() + " " + color + '"> <span>' + percent.toString() + '%</span> <div class="slice"> <div class="bar"></div> <div class="fill"></div> </div> </div>';

            //progress = progress.replace("@percent",percent.toString());

            //progress = progress.replace("@color", color);

            return progress;


        }

        $(document).ready(function () {
            $('#tbTools').DataTable(
                {
                    "paging": false,
                    fixedHeader: true,

                    "columns": [

                        {
							"width":"75px",
                            "data": "Status", "title": "Status",
                            "render": function (oObj, type, full, meta) {
                                var rtn = "";

                                switch (full["Progress"]) {
                                    case "100":
                                        rtn = "<img src='green.png' alt='1 - Approved' width='32' height='32'>";
                                        break;

                                    case "-1":
                                        rtn = "<p hidden>5 - Hold</p><img src='red.png' alt='5 - Hold' width='32' height='32'>";
                                        break;
                                    default:
                                        rtn = GetProgress(full["Progress"]);
                                        break;
                                }

                                return rtn;

                            }


                        },
                        {"data": "Progress", "visible": false}

                        ,
                        {"data": "Change", "title": "Change" , "width": "350px"},
                        {"data": "Comments", "title": "Comments", "width":"350px"},
                        {"data": "Requested", "title": "Requested"},
                        {"data": "Implemented", "title": "Implemented"}]
                }
            );
        });


    </script>
</head>

<body>

<table id="tbTools" class="display" style="width: 2008px; height: 639px;">
    <thead>

    <th>Status</th>
    <th>Progress</th>
    <th style="text-align:left;">Change</th>
    <th style="text-align:left;">Comments</th>
    <th style="text-align:left;">Requested</th>
    <th>Implemented</th>

    </thead>
    <tbody>
    <?php

    $i = 0;
    foreach ($xlsx->rows() as $r => $row) {
        if ($i > $headerRow) {
            $requested = "";
            $implemented = "";

            try {
                if (isset ($row[$colIDRequestDate])) {
                    if (strlen($row[$colIDRequestDate]) > 0) {
                        $requestedDate = new DateTime($row[$colIDRequestDate]);
                        $requested = $requestedDate->format('m-d-Y');
                    }
                }
            } catch (Exception $e) {
            }

            try {
                if (isset ($row[$colIDImplementedDate])) {
                    if (strlen($row[$colIDImplementedDate]) > 0) {
                        $implementedDate = new DateTime($row[$colIDImplementedDate]);
                        $implemented = $implementedDate->format('m-d-Y');
                    }
                }
            } catch (Exception $e) {
            }
            echo("<tr>");
            echo("<td style='text-align: center'>" . $row[$colIDStatus] . "</td>");
            echo("<td>" . $row[$colIDStatusP] . "</td>");
            echo("<td>" . $row[$colIDChange] . "</td>");
            echo("<td>" . $row[$colIDComments] . "</td>");
            echo("<td>" . $requested . "</td>");
            echo("<td>" . $implemented . "</td>");

            echo("</tr>");
        }

        $i++;

    }

    ?>


    </tbody>


</table>
</body>
</html>