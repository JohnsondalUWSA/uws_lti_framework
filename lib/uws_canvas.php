<?php
require_once ('canvas-php-curl/class.curl.php');

class uws_canvas
{
    public $uws_canvas;
    public $get_users;

    private $mtoken ="";
    private $mtokenRaw = "";
    private $mdomain = "";



/* API Constants */

    const api_course_get = "/courses/{0}?include[]=term";

    const api_enroll_user = "/sections/{0}/enrollments";
    const api_enroll_sections = "/courses/{0}/sections";
    const api_enroll_create_section = "/courses/{0}/sections";
    const api_enroll_enrollment = "/courses/{0}/users?include[]=email&enrollment_state[]=active&include[]=enrollments";
    const api_users = "/users/{0}";
    const api_term = "/accounts/1/terms/{0}";
    const api_users_search = "/accounts/1/users?search_term={0}&include[]=email";

    /**
     * Contructor function
     * @param $token: Canvas User Token; note do not use personal admin token, utilize a service account
     * @param $domain: Canvas Base Canvas URL; note should be like domain.instructure.com
     */

    public function __construct($token, $domain)
{
    $this->mdomain = $domain;

    $this->mtoken = "authorization: Bearer ".$token;
    $this->mtokenRaw = $token;
}

    public function get_users ($courseID)
    {

        $apiCall = str_replace("{0}",$courseID,self::api_enroll_enrollment);
        $cURL = new Curl($this->mtoken, $this->mdomain);

        $courseUsers = $cURL->get($apiCall, array(
          "sort" => "name",
          "order" => "desc"));

        $cURL->closeCurl();

        return $courseUsers;

    }

    public function isAdminforAccount ( $accountId, $userCanvasLoginId)
    {
       $apiCall = "/accounts/".$accountId."/admins";
       $cURL =  new Curl($this->mtoken, $this->mdomain);

       $admins = $cURL->get($apiCall, array(
            "sort" => "id"));

        foreach ($admins as $item)
        {
            if ($item->user -> login_id == $userCanvasLoginId)
                 return true;

        }
        return false;
    }

    public function isAdminRole ($accountId, $memberships)
    {
        $userMemberships = explode(",",$memberships);

        $apiCall = "/accounts/".$accountId."/roles";
        $cURL =  new Curl($this->mtoken, $this->mdomain);

        $roles = $cURL->get($apiCall, array(
            "sort" => "id"));

        foreach ($roles as $item)
        {
            foreach ($userMemberships as $userMembership)
            {
                if ($userMembership == $item -> label && $item -> base_role_type == "AccountMembership")
                    return true;

            }
        }
        return false;




    }
    public function post ($apiURL, $data)
    {
        $cURL = new Curl($this->mtoken, $this->mdomain);;

        $results = $cURL -> post($apiURL,$data);
        return $results;
    }

    public function put ($apiURL, $data)
    {
        $cURL = new Curl($this->mtoken, $this->mdomain);;

        $results = $cURL -> put($apiURL,$data);
        return $results;
    }



    public function get ($apiURL, $data,$maxrecords=null, $subclass = null)
    {
        $cURL = new Curl($this->mtoken, $this->mdomain);;

        $results = $cURL -> get($apiURL,$data, $maxrecords,$subclass);
        return $results;


    }



    public function get_user ($userID)
    {

        $apiCall = str_replace("{0}",$userID,self::api_users);
        $cURL = new Curl($this->mtoken, $this->mdomain);

        $courseUser = $cURL->get($apiCall, array(
            "sort" => "name",
            "order" => "desc"));

        $cURL->closeCurl();

        return $courseUser;

    }


    public function userSearch ($searchterm)
    {

        $apiCall = str_replace("{0}",$searchterm,self::api_users_search);
        $cURL = new Curl($this->mtoken, $this->mdomain);

        $Users = $cURL->get($apiCall, array(
            "sort" => "name",
            "order" => "desc"));

        $cURL->closeCurl();

        return $Users;

    }


    public function get_term ($id)
    {
        $apiCall = str_replace("{0}",$id,self::api_term);
        $cURL = new Curl($this->mtoken, $this->mdomain);

        $term = $cURL->get($apiCall);
        $cURL->closeCurl();

        return $term;



    }

    public function get_course ($courseID)
    {


        $apiCall = str_replace("{0}",$courseID,self::api_course_get);
        $cURL = new Curl($this->mtoken, $this->mdomain);

        $course = $cURL->get($apiCall);


        $cURL->closeCurl();

        return $course;

    }

    public function create_course ($accountID, $courseName, $courseCode)
    {
        $curl = curl_init();

        $apiCall = "https://".$this->mdomain."/api/v1/accounts/".$accountID."/courses";
        curl_setopt_array($curl, array(
            CURLOPT_URL => $apiCall,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_SSL_VERIFYHOST =>  false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => array('course[name]' => $courseName,'course[course_code]' => $courseCode),
            CURLOPT_HTTPHEADER => array(   "authorization: Bearer ".$this->mtokenRaw
            )
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return array("error"=> $err);
        } else {
            return json_decode( $response,true);
        }



    }

    public function create_course_enrollment ($courseID, $canvas_user_id)
    {



        $curl = curl_init();

        $apiCall = "https://".$this->mdomain."/api/v1/courses/".$courseID."/enrollments";
        curl_setopt_array($curl, array(
            CURLOPT_URL => $apiCall,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_SSL_VERIFYHOST =>  false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS =>array('enrollment[user_id]'=>$canvas_user_id,
                'enrollment[role_id]'=>4,
                'enrollment[enrollment_state]'=>'active',
                'enrollment[type]'=>'TeacherEnrollment',
                'enrollment[notify]'=>true ),
            CURLOPT_HTTPHEADER => array(   "authorization: Bearer ".$this->mtokenRaw
            )
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return array("error"=> $err);
        } else {
            return json_decode( $response,true);
        }



    }

    public function create_section ($courseID, $end_date, $section_name)
    {

        //Convert Date


     //   $newDate =  DateTime::createFromFormat('m/d/Y',$end_date)->format('Y-m-d');
        $newDate =  DateTime::createFromFormat('m/d/Y H:i:s',$end_date. " 23:59:59")->format('Y-m-d H:i:s');

       // date_add($newDate, date_interval_create_from_date_string('5 hours'));

        $apiCall = "https://".$this->mdomain."/api/v1".str_replace("{0}",$courseID,self::api_enroll_create_section);

        $curl = curl_init();



        curl_setopt_array($curl, array(
            CURLOPT_URL => $apiCall,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_SSL_VERIFYHOST =>  false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => array("course_section[name]" => $section_name,
                "course_section[end_at]" => $newDate,
                "course_section[restrict_enrollments_to_section_dates]" => true),
            CURLOPT_HTTPHEADER => array(
                "authorization: Bearer ".$this->mtokenRaw,
                "cache-control: no-cache",
                "content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
                "postman-token: a92e9759-38a7-1582-38bb-a0761df429cc"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return json_decode( $response);
        }
//        $cURL = new Curl($this->mtoken, $this->mdomain);
//
//        $apiData  =[
//            "course_section[name]"=> $section_name,
//            "course_section[end_at]"=>$end_date
//
//
//
//        ];
//        $section= $cURL->post($apiCall, $apiData);
//
//
//        $cURL->closeCurl();
//        return $section[0];

    }

    public function grade_assignment ($courseID, $assignmentID, $lmsID)
    {
        $curl = curl_init();


        $apiCall = "https://".$this->mdomain."/api/v1/courses/".$courseID."/assignments/".$assignmentID."/submissions/update_grades";

        curl_setopt_array($curl, array(
            CURLOPT_URL => $apiCall,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_SSL_VERIFYHOST =>  false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => array('grade_data['.$lmsID.'][posted_grade]' => 'complete'),
            CURLOPT_HTTPHEADER => array(   "authorization: Bearer ".$this->mtokenRaw
            )
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return array("error"=> $err);
        } else {
            return json_decode( $response,true);
        }



    }
    public function create_enrollment($section, $user)
    {
            $apiCall = "https://".$this->mdomain."/api/v1/sections/".$section."/enrollments";
        $data = array(
            'enrollment[user_id]' => $user,
            'enrollment[notify]' => 'false',
            'enrollment[enrollment_state]' => 'active'
        );

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $apiCall,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_SSL_VERIFYHOST =>  false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            //CURLOPT_POSTFIELDS => "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"enrollment[user_id]\"\r\n\r\n".$user."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"enrollment[notify]\"\r\n\r\nfalse\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--",
            CURLOPT_POSTFIELDS =>$data,
            CURLOPT_HTTPHEADER => array(
                "authorization: Bearer ".$this->mtokenRaw,
                "cache-control: no-cache",
                "content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",

            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return $response;
        }
       // $apiCall = str_replace("{0}",$section,self::api_enroll_user);
        //$cURL = new Curl($this->mtoken, $this->mdomain);

        //$apiData  =[
          //  "enrollment[user_id]"=> $user,
           // "enrollment[notify]" => false

        //];
        //$enrollment= $cURL->post($apiCall, $apiData);


       // $cURL->closeCurl();

        //return $enrollment;



    }





}