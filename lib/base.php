<?php
use Analog\Analog;

require_once $_SERVER["DOCUMENT_ROOT"]."/".Explode("/",$_SERVER["REQUEST_URI"])[1]."/vendor/autoload.php";

$mySession = new fkooman\SeCookie\Session(
    fkooman\SeCookie\SessionOptions::init()->withName('UWSSID'),
    fkooman\SeCookie\CookieOptions::init()->withSameSiteNone()
);
$mySession->start();

$encoded_session = $mySession -> get("session");
$SESSION = array();

if (isset($encoded_session))
    $SESSION = unserialize(($encoded_session));


require_once ($_SERVER["CONTEXT_DOCUMENT_ROOT"]."/".explode('/',$_SERVER["PHP_SELF"])[1].'/settings.php');
require_once ($_SERVER["CONTEXT_DOCUMENT_ROOT"]."/".explode('/',$_SERVER["PHP_SELF"])[1].'/lib/encrypt/key.php');
require_once ($_SERVER["CONTEXT_DOCUMENT_ROOT"]."/".explode('/',$_SERVER["PHP_SELF"])[1].'/lib/encrypt/uws_encrypt.php');

require_once ($_SERVER["CONTEXT_DOCUMENT_ROOT"]."/".explode('/',$_SERVER["PHP_SELF"])[1].'/lib/uws_canvas.php');
require_once ($_SERVER["CONTEXT_DOCUMENT_ROOT"]."/".explode('/',$_SERVER["PHP_SELF"])[1].'/lib/MeekroDB/meekrodb.2.3.class.php');


require_once ($_SERVER["CONTEXT_DOCUMENT_ROOT"]."/".explode('/',$_SERVER["PHP_SELF"])[1]."/lib/logger.php");

// Set MySQL Info
DB::$host = $mysql_server;
DB::$user = $mysql_user;
DB::$password = uws_encrypt::encrypt_decrypt('decrypt', $mysql_password, $mykey);
DB::$dbName = $mysql_db;

$lms_url = "";
$token = "";

if (isset ($SESSION['OAuthDomain']))
    {$lms_url = $SESSION['OAuthDomain'];}
else{
    $lms_url = "uwsa.instructure.com";
}
if (isset ($lms_token[$lms_url]))
    {$token = $lms_token[$lms_url];}




// Init Canvas
$canvas = new uws_canvas(uws_encrypt::encrypt_decrypt('decrypt', $token, $mykey), $lms_url);


// Init Logger Class
$logger = new uws_logger($mysql_server, $mysql_db,$mysql_user, uws_encrypt::encrypt_decrypt('decrypt',$mysql_password,$mykey));
