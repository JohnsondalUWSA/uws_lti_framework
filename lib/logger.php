<?php
use Analog\Analog;
use Analog\Handler\PDO;

require_once ($_SERVER["CONTEXT_DOCUMENT_ROOT"]."/".explode('/',$_SERVER["PHP_SELF"])[1]."/vendor/analog/analog/lib/Analog/Analog.php");
require_once ($_SERVER["CONTEXT_DOCUMENT_ROOT"]."/".explode('/',$_SERVER["PHP_SELF"])[1]."/vendor/analog/analog/lib/Analog/Handler/PDO.php");

//require 'vendor/autoload.php';


class uws_logger
{

   // Hold PDO connection;
    private $pdo;

    public function __construct($host, $db, $user, $password)
    {
        $charset = 'utf8mb4';

        $options = [
            \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
            \PDO::ATTR_EMULATE_PREPARES => false,];

        $dsn = "mysql:host=" . $host . ";dbname=" . $db;

        try {
            $pdo = new \PDO($dsn, $user, $password, $options);
        } catch (\PDOException $e) {
            throw new \PDOException($e->getMessage(), (int)$e->getCode());
        }
        $table = 'logs';
        // Helper method for creating the database table
        // Analog\Handler\PDO::createTable($pdo, $table);

        ///  Initialize Analog with your PDO connection and table
        Analog::handler(PDO::init($pdo, $table));

        // Log some messages
      //  Analog::log('53w5');
        //Analog::log('Debug info', Analog::DEBUG);
    }

        public function ltiAuthError($msg)
        {
            Analog::log("Invaild OAUTH! Remote IP:".$this->get_client_ip(). " Message:".serialize($msg),\Analog::URGENT   );



        }
        public function launchError()
        {
            Analog::log("Invaild launch attempt! Remote IP:".$this->get_client_ip(),\Analog::URGENT);


        }

    function get_client_ip() {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

}

        ?>