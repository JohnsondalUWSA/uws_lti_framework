<?php


class uws_token_util
{
    static function generateToken($loginID, $accountId, $restGateway, $apiToken, $dataBase)
    {
        $token = sha1(uniqid($loginID . $accountId));

        $params = array();

        $params["token"] = $token;
        $params["loginID"]=$loginID;
        $params["accountID"]=$accountId;
        $params["tstamp"] =time();
        $params["databaseName"]= $dataBase;
        
        
        
        $apiClient = new dleAPIData($restGateway, $apiToken,$dataBase, $accountId, 'asp_ReportingInsertToken', $params);

        $rtnData = $apiClient->PullData();


        return $token;
    }

    static function getToken($token, $accountId, $restGateway, $apiToken, $dataBase)
    {
        $rtnValue = array();

        $rtnValue["isValid"] = false;

        $params = array();

        $params["token"] = $token;

        $apiClient = new dleAPIData($restGateway, $apiToken,$dataBase, $accountId, 'asp_ReportingGetToken', $params);

        $rtnData = json_decode ($apiClient->PullData(),true);





        if (isset($rtnData[0])) {

                // Check Used
                if ($rtnData[0]["used"] > 10)
                    $rtnValue["errMSG"] = "Token exceeds max usage, generate a fresh token";

                if (!isset ($rtnValue["errMSG"])) {
                    if (time() - $rtnData[0]["tstamp"] > (60 * 60) * 2)
                        $rtnValue["errMSG"] = "Token expired, generate a fresh token";
                }

                if (!isset ($rtnValue["errMSG"])) {
                    $rtnValue["token"] = $token;
                    $rtnValue["loginID"] = $rtnData[0]["loginID"];
                    $rtnValue["accountID"] = $rtnData[0]["accountID"];
                    $rtnValue["tstamp"] = $rtnData[0]["tstamp"];
                    $rtnValue["used"] = intVal($rtnData[0]["used"]) + 1;
                    $rtnValue["isValid"] = true;
                    $rtnValue["databaseName"] = $rtnData[0]["databaseName"];

                }




        } else  $rtnValue["errMSG"] = "Token not found";

        return $rtnValue;

    }

}