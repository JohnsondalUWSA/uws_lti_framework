<?php

require (dirname(__FILE__) . "/../settings.php");
//require_once('../../settings.php');
require_once(dirname(__FILE__).'/encrypt/key.php');
require_once(dirname(__FILE__).'/encrypt/uws_encrypt.php');
require_once (dirname(__FILE__).'/MeekroDB/meekrodb.2.3.class.php');


class OAuthHandler
{
    // Canvas host name
    public $OAuthDomain ;

    public $client_id = "";
    public $client_key = "";
    public $redirectLink = "";

    public $refresh_token = "";
    public $OAuthTokenInfo;
    public $access_token ;

    public function __construct($Domain, $ClientID, $ClientKey, $RedirectLNK) {
        $this ->OAuthDomain = $Domain;
        $this ->client_id = $ClientID;
        $this -> client_key = $ClientKey;
        $this -> redirectLink = $RedirectLNK;

         }

    public function getRefreshToken ($code)
    {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL =>  "https://".$this->OAuthDomain."/login/oauth2/token",
            CURLOPT_SSL_VERIFYHOST =>  false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => ['grant_type'=> 'authorization_code',
                'client_id' => $this ->client_id,
                'client_secret'=>$this ->client_key,
                'redirect_uri'=>$this ->redirectLink ,
                'code'=>$code],
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: multipart/form-data;"
            )
        ));


        $response = curl_exec($curl);



        $err = curl_error($curl);

        curl_close($curl);


        if ($err) {
            System.debugger_print("cURL Error #:" . $err);
            return false;
        }


        $OAuthDevKeyInfo = json_decode($response);
        $this -> SetOAuthTokenInfo(($OAuthDevKeyInfo));
        $this -> SaveToken();
        return true;

    }

    public function RefreshToken ()
    {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL =>  "https://".$this->OAuthDomain."/login/oauth2/token",
            CURLOPT_SSL_VERIFYHOST =>  false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => ['grant_type'=> 'refresh_token',
                'client_id' => $this ->client_id,
                'client_secret'=>$this ->client_key,
                'refresh_token'=>$this ->refresh_token],
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: multipart/form-data;"
            )
        ));


        $response = curl_exec($curl);



        $err = curl_error($curl);

        $ob = json_decode($response);

        $repErr = false;
        if (isset($ob ->error)== true)
        {
            $repErr = true;

        }


        curl_close($curl);

        if ($err or $repErr) {

            return false;
        }


        $response = substr($response, 0, -1).",\"refresh_token\":\"".$this->refresh_token."\"}";

        $OAuthDevKeyInfo = json_decode($response);
        //$OAuthDevKeyInfo -> refresh_token = $this->refresh_token;

        $this -> SetOAuthTokenInfo(($OAuthDevKeyInfo));
        $this -> SaveToken();

        return True;

    }

    public  function CheckToken()
    {

        // Use generic API call to validate token is still active.  Tokens are good for 1 hour.

        $curl = curl_init();

        $url = "https://".$this->OAuthDomain."/api/v1/users/self";
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_SSL_VERIFYHOST =>  false,
            CURLOPT_SSL_VERIFYPEER => false,

            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "authorization: Bearer ".$this -> access_token,
                "cache-control: no-cache"
               ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        $ob = json_decode($response);

        $repErr = false;
        if (isset($ob->errors))
        {
            if (count($ob->errors) > 0) {
                $repErr = true;

            }
        }
        curl_close($curl);


        if ($err or $repErr) {
            // Attempt Refresh of Token
            if (!$this -> RefreshToken()) {
                $this->DeleteToken($this->OAuthTokenInfo->user->id);
                return false;
            }else return true;
        } else {
            return true;
        }


    }

    public function ValidateToken()
    {
        if (! $this -> CheckToken())
        {
            return $this->refresh_token();
       }
        else {return false;}

    }

    public function SaveToken ()
    {
        // Set MySQL Info
            DB::$host = $GLOBALS["mysql_server"];
            DB::$user = $GLOBALS["mysql_user"];
            DB::$password = uws_encrypt::encrypt_decrypt('decrypt', $GLOBALS["mysql_password"],$GLOBALS["mykey"]);
            DB::$dbName = $GLOBALS["mysql_db"];


                // Log Token.  Statement Inserts / Updates
                DB::insertUpdate('tboauthlog', array(
                    'userID' => $this->OAuthTokenInfo -> user-> id,
                    'AuthInfo' => uws_encrypt::encrypt_decrypt('encrypt',json_encode($this->OAuthTokenInfo),$GLOBALS["mykey"] ),
                    'LastUpdate' => new DateTime ('now'),
                    'ClientID' => $this -> client_id
                ));


    }

    public function DeleteToken($UserID)
    {
        // Set MySQL Info
        DB::$host = $GLOBALS["mysql_server"];
        DB::$user = $GLOBALS["mysql_user"];
        DB::$password = uws_encrypt::encrypt_decrypt('decrypt', $GLOBALS["mysql_password"],$GLOBALS["mykey"]);
        DB::$dbName = $GLOBALS["mysql_db"];


        DB::delete('tboauthlog', "userID=%s and clientID='".$this->client_id."'" , $UserID);

    }

    public function GetSavedToken ($userID, $clientID)
    {

        // Set MySQL Info
        DB::$host =$GLOBALS["mysql_server"];
        DB::$user = $GLOBALS["mysql_user"];
        DB::$password = uws_encrypt::encrypt_decrypt('decrypt', $GLOBALS["mysql_password"],$GLOBALS["mykey"]);
        DB::$dbName = $GLOBALS["mysql_db"];


        // Get AuthInfo from Authlog -- Returns null if no record is found
        $AuthInfo = DB::queryFirstField("SELECT AuthInfo from tboauthlog where userID = ".$userID." and clientID='".$clientID."'");

        if (isset($AuthInfo)) {
            $token = json_decode(uws_encrypt::encrypt_decrypt("decrypt", $AuthInfo, $GLOBALS["mykey"]));

            $this->SetOAuthTokenInfo($token);

            return true;
        }else return false;
    }

    public function SetOAuthTokenInfo ($TokenInfo)
    {


        $this->refresh_token = $TokenInfo -> refresh_token;
        $this -> OAuthTokenInfo = $TokenInfo;
        $this -> access_token = $TokenInfo -> access_token;



    }


}