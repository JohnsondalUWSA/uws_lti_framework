<?php
  /**
   * Canvas API cURL Class
   *
   * This class was built specifically for use with the Instructure Canvas RESST
   * API.
   *
   * PHP version >= 5.2.0
   *
   * @author Christopher Esbrandt <cesbrandt@ecpi.edu>
   */
  class Curl {
    public $curl;
    public $get;
    public $put;
    private $token;
    private $baseURL;
    private $initCurl;
    private $restartCurl;
    private $closeCurl;
    private $setOpt;
    private $setURLData;
    private $urlPath;
    private $callAPI;
    private $exec;

    /**
     * Contructor function
     *
     * @param $base_url
     */
    public function __construct($token, $domain) {
      if(is_null($token)) {
        throw new \ErrorException('No admin token supplied.');
      }
      if(is_null($domain)) {
        throw new \ErrorException('No domain supplied.');
      }
      $this->token = $token;
      $this->baseURL = 'https://' . $domain . '/api/v1';
      $this->initCurl();
    }

    /**
     * Initialize a cURL call
     */
    private function initCurl() {
      $this->curl = curl_init();
      $this->setOpt(CURLOPT_RETURNTRANSFER, true);
      $this->setOpt(CURLOPT_HEADER, true);
      $this->setOpt(CURLOPT_HTTPHEADER, array('Content-Type: application/json', $this->token));
      $this->setOpt( CURLOPT_SSL_VERIFYHOST, false);
      $this ->setOpt (CURLOPT_SSL_VERIFYPEER, false);
    }

    /**
     * Restart cURL for multiple calls
     */
    private function restartCurl() {
      $this->closeCurl();
      $this->initCurl();
    }

    /**
     * Close cURL after all calls have been made
     */
    public function closeCurl() {
      curl_close($this->curl);
    }

    /**
     * Execute cURL function
     *
     * @return array
     */
    private function exec($url = NULL) {
      if(!is_null($url)) {
        $this->setURLData($url);
      }
      $results = curl_exec($this->curl);
      $err = curl_error($this ->curl);
      $errB = curl_getinfo($this -> curl);
      $headerSize = curl_getinfo($this->curl, CURLINFO_HEADER_SIZE);
      $header = substr($results, 0, $headerSize);
      $results = json_decode(substr($results, $headerSize));
      $this->restartCurl();
      $nextLink = $this->getNextPage($header);

      return array($header, $results, $nextLink);
    }

    /**
     * Calls exec() for each page of the API results
     *
     * @return array
     */
    private function callAPI($maxRecords = null, $subClass = null) {
      $results = array();
      $call = $this->exec();
      if(substr($call[0], 0, 12) != 'HTTP/1.1 302' && substr($call[0], 0, 12) != 'HTTP/1.1 404') {
        if(is_array($call[1])) {
          foreach($call[1] as $result) {
            array_push($results, $result);
          }
        } else {
            if ($subClass === null)
                array_push($results, $call[1]);
            else
                foreach ($call[1]->{$subClass} as $result){

                    array_push($results, $result);}

        }
      }


      //Page through results
      while ($call[2] !== "") {
          $call = $this->exec($call[2]);
          $this->restartCurl();
          if (substr($call[0], 0, 12) != 'HTTP/1.1 302' && substr($call[0], 0, 12) != 'HTTP/1.1 404') {
              if (is_array($call[1])) {
                  foreach ($call[1] as $result) {
                      array_push($results, $result);
                  }
              } else {

                  if ($subClass === null)
                    array_push($results, $call[1]);
                  else
                      foreach ($call[1]->{$subClass} as $result){

                            array_push($results, $result);}
              }
          }

          if ($maxRecords !== null)
          {
              if (count($results)>$maxRecords)
                break;
          }

      }



      return $results;
    }

    /**
     * POST function
     *
     * @param $url, $data
     *
     * @return array
     */
    public function post($url, $data = NULL) {
        $this->restartCurl();
      if($data !== null) {
            if ($data !== "[]")
            {$this->setURLData($url, json_encode($data));}
            else $this-> setURLData($url,null);
      }else $this->setURLData($url,null);

      $this->setOpt(CURLOPT_CUSTOMREQUEST, "POST");
      $this->setOpt(CURLOPT_RETURNTRANSFER,true);
      $this->setOpt(CURLOPT_ENCODING,"");
      $this->setOpt(CURLOPT_MAXREDIRS ,10);
      $this->setOpt(CURLOPT_TIMEOUT,30);
      $this->setOpt(CURLOPT_HTTP_VERSION,CURL_HTTP_VERSION_1_1);
      $this->setOpt(CURLOPT_FOLLOWLOCATION ,true);
      $this->setOpt(CURLOPT_POST,1);

      return $this->callAPI();
    }

    /**
     * PUT function
     *
     * @param $url, $data
     *
     * @return array
     */
    public function put($url, $data = NULL) {
      if(is_null($data)) {
        throw new \ErrorException('No data supplied.');
      }
      $this->setURLData($url, json_encode($data));
      $this->setOpt(CURLOPT_CUSTOMREQUEST, 'PUT');
      return $this->callAPI();
    }

    /**
     * GET function
     *
     * @param $url, $data
     *
     * @return array
     */
    public function get($url, $data = NULL, $maxRecords = null ,$subclass = null) {
      $this->setURLData($url . (!is_null($data) ? (((strpos($url, '?') !== false) ? '&' : '?') . http_build_query($data)) : ''));
      $this->setOpt(CURLOPT_CUSTOMREQUEST, 'GET');
      return $this->callAPI($maxRecords, $subclass);
    }

    /**
     * Set the target URL and supplied data function
     *
     * @param $url
     * @param $data
     */
    private function setURLData($url, $data = NULL) {
      if(is_null($url)) {
        throw new \ErrorException('No target URL supplied.');
      }
      $this->urlPath = $url;
      if (strpos($url ,"https") === false ) {
          $this->setOpt(CURLOPT_URL, $this->baseURL . $this->urlPath . ((strpos($url, '?') !== false) ? '&' : '?') . 'per_page=100');
      }else   { $this->setOpt(CURLOPT_URL, $url);}


      if(!is_null($data)) {
        $this->setOpt(CURLOPT_POSTFIELDS, $data);
      }
    }

    /**
     * Set cURL Options function
     *
     * @param $option
     * @param $value
     */
    private function setOpt($options, $value = null) {
      if(is_array($options)) {
        foreach($options as $option => $value) {
          curl_setopt($this->curl, $option, $value);
        }
      } else {
        curl_setopt($this->curl, $options, $value);
      }
    }

 private function getNextPage ($headers)
 {

    $rtnValue = "";

     if ( preg_match( '/^link: (.*)$/im', $headers, $matches ) ) {
         $link_info = $matches[1];
         $links = explode(',', $link_info);

         foreach ($links as $link) {
             if (strpos($link,"next") !==false)
             {
                 $rtnValue = substr(explode(";", $link)[0],1,-1);
             }
             }

     }
     return $rtnValue;
     }
  private function getNextPageB ($headers)
{
    $rtnValue = "";
 //   explode(",", explode("\r\n",$call[0])[15])
    // Step 1 explode $header

    $hArray = explode("\r\n",$headers);

    $link = "";

    foreach ($hArray as $item)
    {
        if (strpos($item,"link:") !== false)
        {
            $link = ltrim($item, "link:");
            foreach (explode(",", $link) as $item2)
            {
                if (strpos ($item2, 'rel="next"') !== false)
                {
                    $rtnValue = trim(trim(explode (";", $item2)[0],"<"),">");

                }

            }


        }



    }



    return $rtnValue;
}
  }
?>