<?php

require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/vendor/autoload.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/settings.php";

session_name("UWS");
session_set_cookie_params(
    ['secure' => true,
        'samesite' => 'None'
    ]
);
session_start();

if (!isset($_SESSION['isKalturaPowerUser']))
    $_SESSION['isKalturaPowerUser'] = false;

if ($_SESSION["isKalturaPowerUser"] === false) {

    echo("no access");
    exit;
}

$entry_id = $_GET["entryId"];

$toTinyURL =  $kaltura_tokens[$_SESSION['OAuthDomain']]["kaltura_service_url"]."/index.php/extwidget/preview/partner_id/".$kaltura_tokens[$_SESSION['OAuthDomain']]["kaltura_partner_id"]
    ."/uiconf_id/".$kaltura_tokens[$_SESSION['OAuthDomain']]["kaltura_uiconf_id"]."/entry_id/".$entry_id."/embed/dynamic?";

echo file_get_contents('http://tinyurl.com/api-create.php?url='.$toTinyURL);