<?php

require_once $_SERVER["DOCUMENT_ROOT"]."/".Explode("/",$_SERVER["REQUEST_URI"])[1]."/vendor/autoload.php";
require_once $_SERVER["DOCUMENT_ROOT"]."/".Explode("/",$_SERVER["REQUEST_URI"])[1]."/settings.php";

require_once $_SERVER["DOCUMENT_ROOT"]."/".Explode("/",$_SERVER["REQUEST_URI"])[1]."/lib/encrypt/key.php";
require_once $_SERVER["DOCUMENT_ROOT"]."/".Explode("/",$_SERVER["REQUEST_URI"])[1]."/lib/encrypt/uws_encrypt.php";

require_once $_SERVER["DOCUMENT_ROOT"]."/".Explode("/",$_SERVER["REQUEST_URI"])[1]."/lib/logger.php";

require_once  ($_SERVER["DOCUMENT_ROOT"]."/".Explode("/",$_SERVER["REQUEST_URI"])[1]."/lib/uws_canvas.php");
require_once ($_SERVER["DOCUMENT_ROOT"]."/".Explode("/",$_SERVER["REQUEST_URI"])[1]."/lib/MeekroDB/meekrodb.2.3.class.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/pages/Kaltura/Biz/uwsKalturaClient.php");



class datatableData
{
    public $data = array();

}

session_name("UWS");
session_set_cookie_params(
    ['secure'=>true,
        'samesite'=> 'None'
    ]
);
session_start();

if (!isset($_SESSION['isKalturaPowerUser']))
    $_SESSION['isKalturaPowerUser'] = false;

if ($_SESSION["isKalturaPowerUser"]=== false)
{

    echo("no access");
    exit;
}

$lms_url = $_SESSION['OAuthDomain'];
$token = $lms_token[$lms_url];

$canvas = new uws_canvas(uws_encrypt::encrypt_decrypt('decrypt', $token,$mykey), $lms_url);
$sharedsecret = uws_encrypt::encrypt_decrypt('decrypt', $kaltura_tokens[$_SESSION['OAuthDomain']]["kaltura_admin_secret"], $mykey);


$users = $canvas->userSearch($_REQUEST["login_id"]);

$rtnValue = new datatableData();

// Remove results not containing a name or login_id
foreach ($users as $user)
{
    if (isset($user ->name) and isset($user ->login_id))
    {
        $user -> AccountType = "User";
        array_push ($rtnValue -> data, $user);

    }


}

//Init Kaltura Group Search
$kaltura = new uwsKalturaclient($kaltura_tokens[$_SESSION['OAuthDomain']]["kaltura_partner_id"], $sharedsecret);

// Get List of Users Media
$GroupList = $kaltura->searchGroups($_REQUEST["login_id"]);

foreach ( $GroupList-> objects as $group)
{
    $kUser = new stdClass();
    $kUser->AccountType = "Group";
    $kUser ->login_id = $group-> id;
    $kUser ->name = $group->screenName;
    array_push ($rtnValue -> data, $kUser);

}

header('Content-type: application/json');
echo json_encode($rtnValue);
