<?php

require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/vendor/autoload.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/settings.php";

require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/lib/encrypt/key.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/lib/encrypt/uws_encrypt.php";

require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/lib/logger.php";

require_once($_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/lib/uws_canvas.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/lib/MeekroDB/meekrodb.2.3.class.php");

require_once($_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/pages/Kaltura/Biz/uwsKalturaClient.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/pages/Kaltura/Biz/uwsKalturaLog.php");

class datatableData
{
    public $data = array();

}

session_name("UWS");
session_set_cookie_params(
    ['secure' => true,
        'samesite' => 'None'
    ]
);
session_start();

if (!isset($_SESSION['isKalturaPowerUser']))
    $_SESSION['isKalturaPowerUser'] = false;

if ($_SESSION["isKalturaPowerUser"] === false) {

    echo("no access");
    exit;
}

$lms_url = $_SESSION['OAuthDomain'];
$token = $lms_token[$lms_url];

$sharedsecret =  uws_encrypt::encrypt_decrypt('decrypt', $kaltura_tokens[$lms_url]["kaltura_admin_secret"],$mykey);


$kaltura = new uwsKalturaclient($kaltura_tokens[$lms_url]["kaltura_partner_id"],$sharedsecret,$_POST["LoginId"], $_POST["ks"]);
$kLogger = new uwsKalturaLog($mysql_server,$mysql_user, uws_encrypt::encrypt_decrypt('decrypt', $mysql_password,$mykey),$mysql_db);


$rtnValue = "Good";
$rtnValue = $kaltura -> addMedia($_POST["name"], $_POST["description"],$_POST["LoginId"], $_POST["AutoCaption"], $_POST["UploadToken"] , $_POST["tags"]);
$kLogger -> log ("Added Media",$_SESSION["post"]["custom_canvas_user_login_id"], "","");

echo $rtnValue;