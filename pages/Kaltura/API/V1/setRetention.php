<?php

require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/vendor/autoload.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/settings.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/pages/Kaltura/settings.php";


require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/lib/encrypt/key.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/lib/encrypt/uws_encrypt.php";

require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/lib/logger.php";

require_once($_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/lib/uws_canvas.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/lib/MeekroDB/meekrodb.2.3.class.php");

require_once($_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/pages/Kaltura/Biz/uwsKalturaClient.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/pages/Kaltura/Biz/uwsKalturaLog.php");

session_name("UWS");
session_set_cookie_params(
    ['secure' => true,
        'samesite' => 'None'
    ]
);
session_start();

if (!isset($_SESSION['isKalturaPowerUser']))
    $_SESSION['isKalturaPowerUser'] = false;

if ($_SESSION["isKalturaPowerUser"] === false) {

    echo("no access");
    exit;
}

$sharedsecret = uws_encrypt::encrypt_decrypt('decrypt', $kaltura_tokens[$_SESSION['OAuthDomain']]["kaltura_admin_secret"], $mykey);

$entries = json_decode($_REQUEST["entries"]);
$action = $_REQUEST["action"];
$admin_login_id = $_SESSION["post"]["custom_canvas_user_login_id"];
$reason = $_REQUEST["reason"];


//Init Kaltura
$kaltura = new uwsKalturaclient($kaltura_tokens[$_SESSION['OAuthDomain']]["kaltura_partner_id"], $sharedsecret);
$kLogger = new uwsKalturaLog($mysql_server, $mysql_user, uws_encrypt::encrypt_decrypt('decrypt', $mysql_password, $mykey), $mysql_db);


foreach ($entries as $entry) {

    $mediaItem = null;
    $mediaItem = $kaltura->kclient->media->get($entry);
    //$metadata = $kaltura->getRetentionInfo($entry,$RetentionData);

    $defaultXML = "<metadata><ExcludeMedia>%s</ExcludeMedia><Requestor>%s</Requestor><DateRequested>%s</DateRequested><Reason>%s</Reason></metadata>";

    // Create XML Metadata String.
    $metadata = sprintf($defaultXML, $action == "add" ? "Yes" : "No", $admin_login_id, time(), $reason);

    $cats = "";
    if (isset ($mediaItem->categoriesIds))
        $cats = $mediaItem->categoriesIds;
    //else
     //   $cats = $RetentionCat;

    $kaltura->setRetentionCat($mediaItem->id, $cats, $RetentionCat, $RetentionUnSetCat , $action);

    // Check for existing Metadata first
    $rtnValue =  $kaltura-> getRetentionInfo($mediaItem->id,$RetentionData);
    if ($rtnValue->totalCount == 1)
    {
        $metadataID = $rtnValue->objects[0]-> id;
        $kaltura->updateRetentionInfo($metadataID,$metadata);
    }
    else
    $kaltura->setRetentionInfo($mediaItem->id, $RetentionData, $metadata);

    $kLogger->log("Set Retention", $_SESSION["post"]["custom_canvas_user_login_id"], $mediaItem->id,"");
}


