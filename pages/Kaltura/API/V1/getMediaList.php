<?php

require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/vendor/autoload.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/settings.php";

require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/lib/encrypt/key.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/lib/encrypt/uws_encrypt.php";

require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/lib/logger.php";

require_once($_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/lib/uws_canvas.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/lib/MeekroDB/meekrodb.2.3.class.php");

require_once($_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/pages/Kaltura/Biz/uwsKalturaClient.php");

require_once($_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/pages/Kaltura/settings.php");

class datatableData
{
    public $data = array();

}

session_name("UWS");
session_set_cookie_params(
    ['secure' => true,
        'samesite' => 'None'
    ]
);
session_start();

if (!isset($_SESSION['isKalturaPowerUser']))
    $_SESSION['isKalturaPowerUser'] = false;

if ($_SESSION["isKalturaPowerUser"] === false) {

    echo("no access");
    exit;
}

$lms_url = $_SESSION['OAuthDomain'];
$token = $lms_token[$lms_url];

$sharedsecret =  uws_encrypt::encrypt_decrypt('decrypt', $kaltura_tokens[$lms_url]["kaltura_admin_secret"],$mykey);


$kaltura = new uwsKalturaclient($kaltura_tokens[$lms_url]["kaltura_partner_id"],$sharedsecret);



$mediaList = $kaltura->getMedia($_REQUEST["login_id"]);

//$ConvertJson = json_encode($mediaList);

//$ConvertJson = str_replace('"objects":','"media":',$ConvertJson);
//$Converted = json_decode($ConvertJson);

$rtnValue = new datatableData();



foreach ($mediaList -> objects as $mediaItem)
{
    $mediaItem ->retain = 0;
    if (isset($mediaItem->categoriesIds))
      if(strpos($mediaItem->categoriesIds, $RetentionCat) !== false){
          $mediaItem -> retain = 1;
            }

    $mediaItem ->createDate = date("m-d-y", substr($mediaItem-> createdAt, 0, 10));
    $mediaItem ->durationMin =  gmdate("H:i:s", $mediaItem-> duration);

    //$english_format_number = number_format(floatval($mediaItem->duration) / 60.00, 2, '.', ',');
    // Convert Status
    switch ($mediaItem ->status) {
        case -2:
            $mediaItem->statusDesc = "ERROR_IMPORTING [-2]";
            break;
        case -1:
            $mediaItem->statusDesc = "ERROR_CONVERTING [-1]";
            break;
        case 0:
            $mediaItem->statusDesc = "IMPORT [0]";
            break;
        case 1:
            $mediaItem->statusDesc = "PRECONVERT [1]";
            break;
        case 2:
            $mediaItem->statusDesc = "READY [2]";
            break;
        case 3:
            $mediaItem->statusDesc = "DELETED [3]";
            break;
        case 4:
            $mediaItem->statusDesc = "PENDING [4]";
            break;
        case 5:
            $mediaItem->statusDesc = "MODERATE [5]";
            break;
        case 6:
            $mediaItem->statusDesc = "BLOCKED [6]";
        case 7:
            $mediaItem->statusDesc = "NO_CONTENT [7]";
            break;
        default:
            $mediaItem->statusDesc = "Unknown !!";
    }
    $mediaItem ->action = "";
    array_push ($rtnValue -> data,$mediaItem);

}

$json = json_encode($rtnValue);
header('Content-type: application/json');
echo $json;