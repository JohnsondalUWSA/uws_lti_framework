<?php

require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/vendor/autoload.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/settings.php";

require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/lib/encrypt/key.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/lib/encrypt/uws_encrypt.php";

require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/lib/logger.php";

require_once($_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/lib/uws_canvas.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/lib/MeekroDB/meekrodb.2.3.class.php");

require_once($_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/pages/Kaltura/Biz/uwsKalturaClient.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/pages/Kaltura/Biz/uwsKalturaLog.php");


session_name("UWS");
session_set_cookie_params(
    ['secure' => true,
        'samesite' => 'None'
    ]
);
session_start();

if (!isset($_SESSION['isKalturaPowerUser']))
    $_SESSION['isKalturaPowerUser'] = false;

if ($_SESSION["isKalturaPowerUser"] === false) {

    echo("no access");
    exit;
}

$sharedsecret = uws_encrypt::encrypt_decrypt('decrypt', $kaltura_tokens[$_SESSION['OAuthDomain']]["kaltura_admin_secret"], $mykey);

$entries = json_decode($_REQUEST["entries"]);
$action = $_REQUEST["action"];
$admin_login_id  = $_REQUEST["admin"];

//Init Kaltura
$kaltura = new uwsKalturaclient($kaltura_tokens[$_SESSION['OAuthDomain']]["kaltura_partner_id"], $sharedsecret);

$kLogger = new uwsKalturaLog($mysql_server,$mysql_user, uws_encrypt::encrypt_decrypt('decrypt', $mysql_password,$mykey),$mysql_db);
// Get List of Users Media
//$mediaList = $kaltura->getMedia($_REQUEST["login_id"]);

// Note: Kaltura returns array of objects which PHP does not like.  Converting to JSON and convert objects to media
//$ConvertJson = json_encode($mediaList);
//$ConvertJson = str_replace('"objects":', '"media":', $ConvertJson);
//$Converted = json_decode($ConvertJson);


foreach ($entries as $entry) {

    $mediaItem = null;
    $mediaItem = $kaltura->kclient->media->get($entry);
    $publishers = $mediaItem -> entitledUsersPublish;

    if ($action == "Add")
    {
        if (strlen($publishers) == 0)
            $publishers = $admin_login_id;
        else
            if (strpos ($publishers,$admin_login_id) === false)
            {
                $publishers = $publishers."," . $admin_login_id;
            }
    }
    else {
        if ($publishers == @$admin_login_id)
            $publishers = "";
        else
        {
        if (strpos($publishers, $admin_login_id) !== false) {
            if (strpos($publishers,$admin_login_id . ",") !== false) {
                $publishers = str_replace($admin_login_id . ",", "", $publishers);
            } else $publishers = str_replace($admin_login_id, "", $publishers);

        }}
    }

        $kaltura -> setCoPublisher($mediaItem->id,$publishers);
        $kLogger -> log ("Set CoPub",$_SESSION["post"]["custom_canvas_user_login_id"], $mediaItem-> id,$publishers);
    }


