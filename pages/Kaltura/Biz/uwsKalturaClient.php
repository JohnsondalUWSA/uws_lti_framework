<?php

use League\Plates\Engine;
use Analog\Analog;


require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/vendor/autoload.php";

require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/lib/encrypt/key.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/lib/encrypt/uws_encrypt.php";

require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/lib/logger.php";

require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/lib/kaltura/KalturaClient.php";


class uwsKalturaclient
{

    public $kclient;
    public $ksession;
    public $kconf;
    public $partnerID;
    public $sharedSecret;

    public function __construct($partnerID, $sharedSecret, $onBehalfOf = null, $existingSession = null)
    {
        $this->partnerID = $partnerID;
        $this->sharedSecret = $sharedSecret;


        $user = ""; //"ltiservice@uwsa.edu";

        if (is_null($onBehalfOf) == false)
            $user = $onBehalfOf;


        $this->kconf = new KalturaConfiguration($this->partnerID);
        $this->kconf->setServiceUrl("http://www.kaltura.com");
        $this->kclient = new KalturaClient($this->kconf);

        if ($existingSession == null)
            $this->ksession = $this->kclient->session->start($this->sharedSecret, $user, KalturaSessionType::ADMIN, $this->partnerID);
        else
            $this->ksession = $existingSession;

        if (!isset($this->ksession)) {
            die("Could not establish Kaltura session. Please verify that you are using valid Kaltura partner credentials.");
        }

        $this->kclient->setKS($this->ksession);


    }


    public function searchGroups($search)
    {

        $groupPlugin = KalturaGroupClientPlugin::get($this->kclient);
        $filter = new KalturaGroupFilter();
        $filter->idOrScreenNameStartsWith = $search;
        $pager = new KalturaFilterPager();

        try {
            $result = $groupPlugin->group->listAction($filter, $pager);
            return $result;
        } catch (Exception $e) {
            die($e->getMessage());
        }

    }

    public function getMedia($userid)
    {

        $filter = new KalturaMediaEntryFilter();
        $filter->userIdEqual = $userid;
        $filter->statusIn = "0,1,2,3,4,5,6,7";
        $pager = new KalturaFilterPager();
        $pager->pageSize = 500;
        $pager->pageIndex = 1;

        $rtnValue = null;
        $done = false;

        while ($done == false) {
            try {
                $result = $this->kclient->media->listAction($filter, $pager);

                if ($rtnValue == null)
                    $rtnValue = $result;
                else
                    $rtnValue->objects = array_merge($rtnValue->objects, $result->objects);

                $pager->pageIndex++;

                if ($result->totalCount == 0)
                    $done = true;
            } catch (Exception $e) {
                $done = true;
            }

        }

        return $rtnValue;

    }

    public function setCoPublisher($entryId, $pubUsers)
    {


        $mediaEntryA = new KalturaMediaEntry();
        $mediaEntryA->entitledUsersPublish = $pubUsers;

        try {
            $result = $this->kclient->media->update($entryId, $mediaEntryA);
            return $result;
        } catch (Exception $e) {
            die($e->getMessage());
        }


    }

    public function setCoEditor($entryId, $pubUsers)
    {


        $mediaEntryA = new KalturaMediaEntry();
        $mediaEntryA->entitledUsersEdit = $pubUsers;

        try {
            $result = $this->kclient->media->update($entryId, $mediaEntryA);
            return $result;
        } catch (Exception $e) {
            die($e->getMessage());
        }


    }

    public function setOwner($entryID, $ownerID)
    {

        $mediaEntryA = new KalturaMediaEntry();
        $mediaEntryA->userId = $ownerID;

        try {
            $result = $this->kclient->media->update($entryID, $mediaEntryA);
            return $result;
        } catch (Exception $e) {
            die($e->getMessage());
        }

    }

    public function setRetentionCat($entryID, $categories, $retentionCategoryID,$unSetCategoryID, $action)
    {

        $mediaEntryA = new KalturaMediaEntry();

        if (!isset($categories))
            $categories = "";

        if ($action == 'add') {
            if (strpos($categories, $retentionCategoryID) == false) {
                $categories = $categories . ',' . $retentionCategoryID;
            }
        } else {
            $catsA = explode(",", $categories);
            $categories = implode(",", array_diff($catsA, [$retentionCategoryID]));
            if ($categories == "")
                $categories = $unSetCategoryID;


        }


        $mediaEntryA->categoriesIds = $categories;


        try {
            $result = $this->kclient->media->update($entryID, $mediaEntryA);

        } catch (Exception $e) {
            $result = $e->getMessage();
        }

        return $result;
    }

    public function setRetentionInfo($entryID, $dataProfileID, $xmlData)
    {
        $result = "";
        $metadataPlugin = KalturaMetadataClientPlugin::get($this->kclient);
        $metadataProfileId = $dataProfileID;
        $objectType = KalturaMetadataObjectType::ENTRY;
        $objectId = $entryID;
        $xmlData = $xmlData;

        try {
            $result = $metadataPlugin->metadata->add($metadataProfileId, $objectType, $objectId, $xmlData);

        } catch (Exception $e) {
            $result = $e->getMessage();
        }

        return $result;

    }

    public function updateRetentionInfo($metadataID, $xmlData)
    {
        $result = "";
        $metadataPlugin = KalturaMetadataClientPlugin::get($this->kclient);
        $id = $metadataID;
        $xmlData = $xmlData;
        $version = 0;

        try {
            $result = $metadataPlugin->metadata->update($id, $xmlData, $version);

        } catch (Exception $e) {
            $result = $e->getMessage();
        }

        return $result;

    }

    public function getRetentionInfo($entryID, $dataProfileID)
    {
        $rtnValue = "";
        $metadataPlugin = KalturaMetadataClientPlugin::get($this->kclient);

        $filter = new KalturaMetadataFilter();
        $filter->objectIdEqual = $entryID;
        $filter->metadataProfileIdEqual = $dataProfileID;
        $pager = new KalturaFilterPager();

        try {
            $rtnValue = $metadataPlugin->metadata->listAction($filter, $pager);

        } catch (Exception $e) {
            $rtnValue = $e->getMessage();
        }

        return $rtnValue;


    }

    public function addMedia($name, $description, $ownerID, $autoCaption, $uploadToken, $tags)
    {
        $entry = new KalturaMediaEntry();

        if ($autoCaption == "true")
            $entry->categoriesIds = "159365161";

        $entry->creatorId = $ownerID;
        $entry->userId = $ownerID;
        $entry->description = $description;
        $entry->name = $name;
        $entry->tags = $tags;
        $entry->mediaType = KalturaMediaType::VIDEO;

        $rtnValue = "true";
        try {
            $result = $this->kclient->media->add($entry);
            $entryId = $result->id;
            $resource = new KalturaUploadedFileTokenResource();
            $resource->token = $uploadToken;
            $resultB = $this->kclient->media->addContent($entryId, $resource);
        } catch (Exception $e) {
            $rtnValue = $e->getMessage();
        }

        return $rtnValue;


    }

    public function addGroup($name)
    {
        $rtnValue = false;
        $groupPlugin = KalturaGroupClientPlugin::get($this->kclient);
        $group = new KalturaGroup();
        $group->id = $name;

        try {
            $result = $groupPlugin->group->add($group);
            $rtnValue = true;
        } catch (Exception $e) {

        }
        return $rtnValue;
    }

    public function addGroupMember($groupID, $userID)
    {
        $rtnValue = false;
        $groupUser = new KalturaGroupUser();
        $groupUser->groupId = $groupID;
        $groupUser->userId = $userID;
        $groupUser->userRole = KalturaGroupUserRole::MEMBER;

        try {
            $result = $this->kclient->groupUser->add($groupUser);
            $rtnValue = true;
        } catch (Exception $e) {

        }
        return $rtnValue;

    }

    public function delGroupMember($groupID, $userID)
    {
        $rtnValue = false;
        $userId = $userID;
        $groupId = $groupID;

        try {
            $result = $this->kclient->groupUser->delete($userId, $groupId);
            $rtnValue = true;
        } catch (Exception $e) {

        }
        return $rtnValue;
    }


}