<?php

require_once ($_SERVER["CONTEXT_DOCUMENT_ROOT"]."/".explode('/',$_SERVER["PHP_SELF"])[1]."/lib/MeekroDB/meekrodb.2.3.class.php");

class uwsKalturaLog
{

    public function __construct( $mySQLServer, $mySQLUser, $mySQLPassword, $mySQLDB  )
    {
        // Set MySQL Info
        DB::$host = $mySQLServer;
        DB::$user = $mySQLUser;
        DB::$password = $mySQLPassword;
        DB::$dbName = $mySQLDB;

    }



    public function log($Action, $AdminUser, $EntryID, $NewValue)
    {

        DB::insert('tbkalturatoolslog', array(
            'Action' => $Action,
            'AdminUser' => $AdminUser,
            'EntryID' => $EntryID,
            'NewValue' => $NewValue

        ));



    }

}