<?php


// Init Session
require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/vendor/autoload.php";

session_name("UWS");
session_set_cookie_params(
    ['secure' => true,
        'samesite' => 'None'
    ]
);

session_start();

if (!isset($_SESSION["isKalturaPowerUser"])) {
    echo("Must be identified as a Kaltura Power User to use this tool!");
    http_response_code(403);
    exit();
} else {
    if ($_SESSION["isKalturaPowerUser"] != true) {
        echo("Must be identified as a Kaltura Power User to use this tool!");
        http_response_code(403);
        exit();
    }

}

?>

<html>
<head>


    <script
            src="https://code.jquery.com/jquery-3.4.1.min.js"
            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
            crossorigin="anonymous"></script>

    <script
            src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
            integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
            crossorigin="anonymous"></script>

    <!-- Bootstrap 4.0 -->
    <!-- CSS only -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <!-- JS, Popper.js, and jQuery -->
   <!-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
   -->
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>

    <!--
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"  integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
-->

    <link href="/<?php echo explode("/", $_SERVER["PHP_SELF"])[1]; ?>/pages/Kaltura/css/jquery.fileupload-ui.css"
          media="screen" rel="stylesheet" type="text/css"/>
    <link href="/<?php echo explode("/", $_SERVER["PHP_SELF"])[1]; ?>/pages/Kaltura/css/jquery.fileupload-ui-kaltura.css"
          media="screen" rel="stylesheet" type="text/css"/>

    <link href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet"/>

    <link rel="stylesheet" type="text/css"
          href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.1/b-colvis-1.6.1/b-flash-1.6.1/b-html5-1.6.1/b-print-1.6.1/cr-1.5.2/kt-2.5.1/r-2.2.3/rr-1.2.6/sc-2.0.1/sp-1.0.1/sl-1.3.1/datatables.min.css"/>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript"
            src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.1/b-colvis-1.6.1/b-flash-1.6.1/b-html5-1.6.1/b-print-1.6.1/cr-1.5.2/kt-2.5.1/r-2.2.3/rr-1.2.6/sc-2.0.1/sp-1.0.1/sl-1.3.1/datatables.min.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>

    <script src="https://cdnapi.kaltura.com/p/2370711/sp/237071100/embedIframeJs/uiconf_id/42910141/partner_id/2370711"></script>

    <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.4/clipboard.min.js"></script>


    <!--  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.
-->

    <style>

        #overlay {

            color: #666666;
            position: fixed;
            height: 100%;
            width: 100%;
            z-index: 5000;
            top: 0;
            left: 0;
            float: left;
            text-align: center;
            padding-top: 25%;
        }

        #mwPlayerContainer
        {
            align-content:center !important;

        }

        #videoOverlay {

            position: fixed;
            height: 100%;
            width: 100%;
            z-index: 5000;
            top: 0;
            left: 0;
            float: left;
            text-align: center;
            padding-top: 25%;

        }

        div.dt-buttons{
            display: none;

        }


    </style>

    <script>

        var adminUser = '<?php echo $_SESSION["post"]["custom_canvas_user_login_id"];?>';
        var action = 0; //0:Change Owner
        var SelectedEntries = "";

        var tbUsers;
        var tbSearchUsers;
        var tbMedia;
        var  selectedUserLoginId;







        $(document).ready(function () {

            tbMedia = $('#tbKalturaMedia').DataTable(
                {
                    dom:'Bfrtip',
                    buttons:['excelHtml5'],
                    fixedColumns:
                        {
                            leftColumns:1

                        },
                    select: {
                        style: 'multi',
                        selector:'td:first-child'
                    },
                    order: [
                        [1, 'asc']
                    ],
                    "columns": [
                        {
                            "data" :"action",

                            "className": "select-checkbox",
                            "orderable":false

                        },
                        {
                            "data": "thumbnailUrl", "title": "",

                            "render": function (oObj, type, full, meta) {
                                try{return  "<img src=\"" + full["thumbnailUrl"].replace('http://','https://') + "\" onclick=\"showVideo('" + full["id"] + "');\">";}
                                catch (err){return;}
                            }
                        },
                        {"data":"retain","title":"Retain", 'render': function (data, type, full, meta){

                            if (full["retain"] == 0)
                                return '<input type="checkbox"  disabled="disabled" >';
                            else
                                return '<input type="checkbox" checked  disabled="disabled">';
                            } },
                        {"data": "id", "title": "EntryID"},
                        {"data":"statusDesc","title":"Status"},
                        {"data": "name", "title": "Name"},
                        {"data": "description", "title": "Description"},
                        {"data":"createDate", "title":"Created"},
                        {"data": "tags", "title": "Tags"},
                        {"data":"durationMin", "title":"duration"},
                        {"data": "entitledUsersPublish", "title":"CoPublishers"},
                        {"data":"entitledUsersEdit", "title":"CoEditors"},
                        {"data":"downloadUrl", "title":"Download",
                        "render":function(oObj, type, full, meta){
                            return '<a target = "_blank" href="' + full["downloadUrl"] + '">'+ full["downloadUrl"] +'</a>';

                        }
                        },
                        {"data":"categories", "visible":false}




                    ]

                }
            );


            tbMedia.on('click', 'th.select-checkbox', function() {
                if ($('#chkSelectAll').prop('checked')) {
                    tbMedia.rows().select();
                }
                else {
                    tbMedia.rows().deselect();
                }

            });
        });



        $(document).ready(function () {

            new ClipboardJS('.btn');



            tbSearchUsers = $('#tbSearchUsers').DataTable(
                    {
                        "searching":false,
                        select: {
                            style: 'single'
                        },
                        "columns": [
                            {"data":"name","title":"Name",
                                "render":function(oObj, type, full, meta){
                                    if (full["AccountType"]== "User")
                                        return '<i class="icon-user"></i> ' + full["name"];
                                    else
                                        return '<i class="icon-group"></i> ' + full["name"];
                                }},


                            {"data": "login_id", "title": "Login"}]


                    }

                );

            $('#tbSearchUsers').on('click', 'tr', function () {
                var data = tbSearchUsers.row(this).data();
                $("#chUser").val(data["login_id"]);

            });


            tbUsers = $('#tbUsers').DataTable(
                {
                    select: {
                        style: 'single'
                    },
                    "columns": [
                        {"data":"name","title":"Name",
                            "render":function(oObj, type, full, meta){
                            if (full["AccountType"]== "User")
                                return '<i class="icon-user"></i> ' + full["name"];
                            else
                                return '<i class="icon-group"></i> ' + full["name"];
                            }},


                        {"data": "login_id", "title": "Login"}]


                }

            );

            $('#tbUsers').on('click', 'tr', function () {
                var data = tbUsers.row(this).data();
                selectedUserLoginId = data["login_id"];

                GetUserMedia(data["login_id"]);
            });

        });


        $(document).keyup(function (event) {
            if ($("#userID").is(":focus") && event.key == "Enter") {
                LookupUser();
            }
        });

        $(document).keyup(function (event) {
            if ($("#SearchuserID").is(":focus") && event.key == "Enter") {
                LookupUserSearch();
            }
        });

        function addRemovePub() {

            var data = tbMedia.rows(".selected").data();
            var entries = [];

            if ( data.length < 1)
            {$.alert({"title":"", content:"No Media Selected!"}); return;}

            for (let i = 0; i< data.length;i++)
            {
                entries.push(data[i]["id"]);
            }

            var jsonData = JSON.stringify(entries);

            $.confirm({
                title: 'Removing Co-Publisher',
                content: 'Confirm you are removing Co-Publisher',
                buttons: {
                    confirm: function () {
                        UpdatePublisher("Remove", jsonData);
                    },
                    cancel: function () {

                    }
                }
            });
        }

        function addVideo() {

            var data = tbUsers.rows({selected: true}).data();

            $.confirm({
                title: 'Adding Video',
                content: 'Confirm you wish to upload video for login:' + data[0]["login_id"],
                buttons: {
                    confirm: function () {
                        window.location = '/<?php echo explode("/", $_SERVER["PHP_SELF"])[1];?>/pages/Kaltura/upload.php?LoginId=' + data[0]["login_id"];
                    },
                    cancel: function () {

                    }
                }
            });
        }

        function UpdatePublisher(action, entries, adminUser ){
            var queryParms = "?entries=" + entries + "&action=" + action + "&admin=" + adminUser;

            var url = "<?php echo("https://" . $_SERVER["SERVER_NAME"] . "/" . explode("/", $_SERVER["REQUEST_URI"])[1] .
                "/pages/Kaltura/API/V1/setCoPublisher.php");?>" + queryParms;

            ShowBusy(true);
            //$("#overlay").show();
            $.ajax({
                url: url
            }).done(function () {
              ShowBusy(false);
                //$("#overlay").hide();
                var data = tbUsers.row(this).data();

                GetUserMedia(data["login_id"]);
            });


        }

        function UpdateEditor(action, entries, adminUser ){
            var queryParms = "?entries=" + entries + "&action=" + action + "&admin=" + adminUser;

            var url = "<?php echo("https://" . $_SERVER["SERVER_NAME"] . "/" . explode("/", $_SERVER["REQUEST_URI"])[1] .
                "/pages/Kaltura/API/V1/setCoEditor.php");?>" + queryParms;

            ShowBusy(true);
            //$("#overlay").show();
            $.ajax({
                url: url
            }).done(function () {
                ShowBusy(false);
                //$("#overlay").hide();
                var data = tbUsers.row(this).data();

                GetUserMedia(data["login_id"]);
            });


        }

        function UpdateOwner(entries, adminUser ){
            var queryParms = "?entries=" + entries + "&action=" + action + "&admin=" + adminUser;

            var url = "<?php echo("https://" . $_SERVER["SERVER_NAME"] . "/" . explode("/", $_SERVER["REQUEST_URI"])[1] .
                "/pages/Kaltura/API/V1/setOwner.php");?>" + queryParms;

            ShowBusy(true);
            //$("#overlay").show();
            $.ajax({
                url: url
            }).done(function () {
                ShowBusy(false);
                //$("#overlay").hide();
                var data = tbUsers.row(this).data();

                GetUserMedia(data["login_id"]);
            });


        }

        function LookupUser() {

            tbUsers.clear().draw();
            ShowBusy (true);
            var loookupUser = $("#userID").val();
            var URL = "<?php echo("https://" . $_SERVER["SERVER_NAME"] . "/" . explode("/", $_SERVER["REQUEST_URI"])[1] . "/pages/Kaltura/API/V1/getUserInfo.php?login_id=");?>" + loookupUser;
            tbUsers.ajax.url(URL).load(function (settings, json) {
                //$('#overlay').hide();
                ShowBusy(false);
                if (tbUsers.rows().count() > 0) {
                    tbUsers.row(':eq(0)', {page: 'current'}).select();
                    var data = tbUsers.rows({selected: true}).data();
                    selectedUserLoginId = data[0]["login_id"];
                    GetUserMedia(data[0]["login_id"]);
                }
            });
        }

        function GetUserMedia(loginID) {
            tbMedia.clear().draw();
            $('#chkSelectAll').prop('checked',false);
            ShowBusy(true);
            var loookupUser = loginID;
            var URL = "<?php echo("https://" . $_SERVER["SERVER_NAME"] . "/" . explode("/", $_SERVER["REQUEST_URI"])[1] . "/pages/Kaltura/API/V1/getMediaList.php?login_id=");?>" + loookupUser;
            tbMedia.ajax.url(URL).load(function (settings, json) {
               ShowBusy(false);
            });
        }

        function hideOverLay() {
            ShowBusy(false);

        }

        function ShowBusy (running)
        {
            if (running)
                document.getElementById("overlay").style.display = "block";
            else
                document.getElementById("overlay").style.display = "none";

        }


        function showVideo(videoURL) {

            $('#VideoModal').modal('show');
            //$("#videoOverlay").show();

           kWidget.embed({
                'targetId': 'myEmbedTarget',
                'wid': '_2370711',
                'uiconf_id' : '42910141',
                'entry_id' : videoURL,
                'flashvars':{ // flashvars allows you to set runtime uiVar configuration overrides.
                    'autoPlay': false
                },
                'params':{ // params allows you to set flash embed params such as wmode, allowFullScreen etc
                    'wmode': 'transparent'
                }
            });

        }

        function HideVideo() {


            $('#VideoModal').modal('hide');
            kWidget.destroy( 'myEmbedTarget' );
        }

        function showCommonModal (userAction) {
            action = userAction;

            // Get Select Entries
            var data = tbMedia.rows(".selected").data();
            var entries = [];

            // Exit if now Entries are selected
            if ( data.length < 1)
            {$.alert({"title":"", content:"No Media Selected!"}); return;}

            // Push Entries to Array
            for (let i = 0; i< data.length;i++)
            {
                entries.push(data[i]["id"]);
            }

            // Convert Array to JSON;
            SelectedEntries= JSON.stringify(entries);

            switch (action) {
                case 0:
                {
                    //Change Owner
                    $("#hChangeTitle").text ("Change video owner");
                    break;
                }
                case 1:
                {
                    //Add Co Publisher
                    $("#hChangeTitle").text ("Add Co Publisher");
                    break;
                }
                case 2:
                {
                    //Remove Co Publisher
                    $("#hChangeTitle").text ("Remove Co Publisher");
                    break;
                }
                case 3:
                {
                    //Remove Co Publisher
                    $("#hChangeTitle").text ("Add Co Editor");
                    break;
                }
                case 4:
                {
                    //Remove Co Publisher
                    $("#hChangeTitle").text ("Remove Co Editor");
                    break;
                }

                default:
                    return;
            }

            // Set Value to Current User
            $("#chUser").val(adminUser);

            // Clear the Search Box
            $("#SearchuserID").val ("");

            //Clear Search Results
            tbSearchUsers.clear().draw();

            // Set Active Tab
            $("#tabs").tabs("option","active",0);

            // Show Dialog
            $('#ChangeModal').modal('show');


        }

        function getTinyURL()
        {
            // Get Select Entries
            var data = tbMedia.rows(".selected").data();

            // Exit if now Entries are selected
            if ( data.length < 1)
            {$.alert({"title":"", content:"No Media Selected!"}); return;}
            if ( data.length > 1)
            {$.alert({"title":"", content:"TinyURL supports one media item at a time."}); return;}

            var entry =data[0]["id"];


            var URL = "<?php echo("https://" . $_SERVER["SERVER_NAME"] . "/" . explode("/", $_SERVER["REQUEST_URI"])[1] . "/pages/Kaltura/API/V1/getTinyURL.php?entryId=");?>" + entry;
            $.get(URL, function(data, status){
                $("#tinyurl").val(data);

            });


            $('#TinyUrlModal').modal('show');

        }

        function showRetention(isAdd)
        {

            // Get Select Entries
            var data = tbMedia.rows(".selected").data();
            var entries = [];

            // Exit if now Entries are selected
            if ( data.length < 1)
            {$.alert({"title":"", content:"No Media Selected!"}); return;}

            if ( data.length >10)
            {$.alert({"title":"", content:"Retention limited to 10 items!"}); return;}

            // Push Entries to Array
            for (let i = 0; i< data.length;i++)
            {
                entries.push(data[i]["id"]);
            }

            // Convert Array to JSON;
            SelectedEntries= JSON.stringify(entries);


            if(isAdd == 1) {
                $("#txtRetentionTitle").text("Enter reason to retain media.");
                $("#txtRetentionAction").val("add");
            }else {
                $("#txtRetentionTitle").text("Enter reason to remove retention policy");
                $("#txtRetentionAction").val("remove");}

            $("#txtRetentionReason").val("");

            $('#RetentionModal').modal('show');



        }

        function SetRetention ()
        {
            var reasonText = "";
            reasonText = $("#txtRetentionReason").val();
            if (reasonText == "") {
                    {$.alert({"title":"", content:"Must provide a reason code"}); return;}

            }
            action = $("#txtRetentionAction").val();

            var data = tbMedia.rows(".selected").data();
            var entries = [];

            if ( data.length < 1)
            {$.alert({"title":"", content:"No Media Selected!"});}

            for (let i = 0; i< data.length;i++)
                 {
                     entries.push(data[i]["id"]);
                    }

        var jsonData = JSON.stringify(entries);



            var queryParms = "?entries=" + jsonData + "&action=" + $('#txtRetentionAction').val() + "&admin=" + $("#chUser").val() + "&reason=" + reasonText;

            var url = "<?php echo("https://" . $_SERVER["SERVER_NAME"] . "/" . explode("/", $_SERVER["REQUEST_URI"])[1] .
                "/pages/Kaltura/API/V1/setRetention.php");?>" + queryParms;

            ShowBusy(true);
            //$("#overlay").show();
            $.ajax({
                url: url
            }).done(function () {
                $('#RetentionModal').modal('hide')
                GetUserMedia(selectedUserLoginId);
            });



        }




       function ExportTable()
       {
           $('.dt-button, .buttons-excel, .buttons-html5').click();


       }
        function DialogAction()
        {



            var lookupUser = $("#chUser").val();
            var URL = "<?php echo("https://" . $_SERVER["SERVER_NAME"] . "/" . explode("/", $_SERVER["REQUEST_URI"])[1] . "/pages/Kaltura/API/V1/ValidateUserInfo.php?login_id=");?>" + lookupUser;

            if (lookupUser == "") {
                $.alert({"title": "", content: "User or Group Not Found!"});
                return;
            }
            ShowBusy (true);

                $.ajax({
                url: URL
            }).done(function(data) {
                ShowBusy(false);
                if ( data < 1)
                     {

                        $.alert({"title":"", content:"User or Group Not Found!"});
                        return;
                     }

                    // Show Dialog
                    $('#ChangeModal').modal('hide');

                     switch (action) {
                        case 0:
                       {
                          //Change Owner
                           UpdateOwner(SelectedEntries,$("#chUser").val() )

                        break;
                    }
                       case 1:
                      {
                            //Add Co Publisher
                            UpdatePublisher("Add",SelectedEntries,$("#chUser").val());
                            break;
                     }
                    case 2:
                    {
                           //Remove Co Publisher
                            UpdatePublisher("Remove",SelectedEntries,$("#chUser").val());
                            break;

                    }
                         case 3:
                         {
                             //Add Co Editor
                             UpdateEditor("Add",SelectedEntries,$("#chUser").val());
                             break;

                         }
                         case 4:
                         {
                             //Remove Co Editor
                             UpdateEditor("Remove",SelectedEntries,$("#chUser").val());
                             break;

                         }
                    default:
                      return;
                    }


            });





        }


        $( function() {
            $( "#tabs" ).tabs();
        } );


        function LookupUserSearch() {

            ShowBusy (true);
            var loookupUser = $("#SearchuserID").val();
            var URL = "<?php echo("https://" . $_SERVER["SERVER_NAME"] . "/" . explode("/", $_SERVER["REQUEST_URI"])[1] . "/pages/Kaltura/API/V1/getUserInfo.php?login_id=");?>" + loookupUser;
            tbSearchUsers.ajax.url(URL).load(function (settings, json) {
                //$('#overlay').hide();
                ShowBusy(false);

            });
        }





    </script>

</head>
<body>

<input id="selectedUser" hidden/>
<h3>Kaltura Admin Tool</h3>

<div class="well bg-light">
    <div class="wrap_me">
        <div class="relative">
            <div class="border border-light ">
                <div>
                    <label>User Search</label>
                    <input type="text" id="userID">
                    <button class="btn btn-primary" type="button" id="btnSearch" onclick="LookupUser();">Search</button>
                 <!--   <button class="btn btn-default" type="button" id="btnAddCoPub" onclick="addCoPub();">Add CoPub</button>
                    <button class="btn btn-default" type="button" id="btnRemoveCoPub" onclick="addRemovePub();">Remove CoPub</button>
                    -->

                </div>
            </div>
        </div>
    </div>
</div>


<div>
    <table id="tbUsers" class="display">
        <thead>
        <tr>

            <th>name</th>
            <th>login_id</th>
        </tr>

        </thead>
        <tbody></tbody>
    </table>
</div>


<hr>

<div class="dropdown">

    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Action
    </button>
    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
        <a class="dropdown-item" onclick="showCommonModal(1);">Add CoPub</a>
        <a class="dropdown-item" onclick="showCommonModal(2);">Remove CoPub</a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" onclick="showCommonModal(3)">Add CoEditor</a>
        <a class="dropdown-item" onclick="showCommonModal(4)">Remove CoEditor</a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" onclick="showRetention(1)">Add Retention Flag</a>
        <a class="dropdown-item" onclick="showRetention(0)">Remove Retention Flag</a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" onclick="showCommonModal(0);">Change Owner</a>

        <a class="dropdown-item" onclick="getTinyURL();">Get TinyURL</a>


    </div>

    <button class="btn btn-secondary" type="button" id="btnUploadOnBehalf" onclick="addVideo();">Upload on Behalf</button>
    <button class = "btn btn-secondary" type="button" onclick="ExportTable()">Export</button>
</div>
    <table id="tbKalturaMedia" class="display">
        <thead>
        <tr>
            <th><input class="select-checkbox" type="checkbox" id="chkSelectAll" name = "chkSelectAll />"</th>

            <th>thumbnailUrl</th>
            <th>retain</th>
            <th>id</th>
            <th>statusDesc</th>
            <th>name</th>
            <th>description</th>
            <th>createDate</th>
            <th>tags</th>
            <th>durationMin</th>
            <th>entitledUsersPublish</th>
            <th>entitledUsersEdit</th>
            <th>downloadUrl</th>
            <th>categories</th>


        </tr>
        </thead>
        <tbody>

        </tbody>
    </table>

</div>

<div id="overlay" style ="display:none">

    <img src="<?php echo("https://" . $_SERVER["SERVER_NAME"] . "/" . explode("/", $_SERVER["REQUEST_URI"])[1] . "/images/catSpinner.gif"); ?>"
         alt="Loading"/></div>
<br/>
</div>


</body>




<!-- Video Modal -->
<div class="modal fade" id="VideoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-body">
                <div align="center" id="myEmbedTarget" style="width:400px;height:250px;">
            </div>
            <div class="modal-footer">
                <!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                <button  type="button" class="btn btn-default" onclick="HideVideo();">Close</button>
                <!--<button type="button" class="btn btn-primary">Save changes</button> -->
            </div>
        </div>
    </div>
</div>
</div>


<!-- Common Modal Dialog -->
<div class="modal" id="ChangeModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="hChangeTitle" class="modal-title">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div id="tabs">
                    <ul>
                        <li><a href="#tabs-1">User/Group</a></li>
                        <li><a href="#tabs-2">Search For</a></li>
                    </ul>
                    <div id="tabs-1">
                        <div class="ui-widget">

                            <input id="chUser" name="chUser">
                        </div>

                    </div>
                    <div id="tabs-2">


                        <input type="text" id="SearchuserID">
                        <button class="btn btn-primary" type="button" id="btnSearch" onclick="LookupUserSearch();">Search</button>

                        <div>
                            <table id="tbSearchUsers" class="display">
                                <thead>
                                <tr>

                                    <th>name</th>
                                    <th>login_id</th>
                                </tr>

                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>

                    </div>

                </div>



                </div>



            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="DialogAction();">Update</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="TinyUrlModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5  class="modal-title">Tiny URL</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- Target -->
                <input id="tinyurl" value="">
                <!-- Trigger -->
                <button class="btn" data-clipboard-target="#tinyurl">
                   <!-- <i class="icon-copy"></i> -->
                    Copy
                </button>


                </div>

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="RetentionModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5  id="txtRetentionTitle" class="modal-title">Set Title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- Target -->
                <textarea id="txtRetentionReason" rows="5" cols="60"></textarea>
                <input id="txtRetentionAction" hidden/>
                <!-- Trigger -->
                <br>
                <button class="btn btn-secondary" onclick="SetRetention()" >Update
                </button>


            </div>

        </div>
    </div>
</div>
</div>



</html>


