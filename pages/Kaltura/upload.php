<?php

require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/vendor/autoload.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/settings.php";

require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/lib/encrypt/key.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/lib/encrypt/uws_encrypt.php";

require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/lib/logger.php";

require_once($_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/lib/uws_canvas.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/lib/MeekroDB/meekrodb.2.3.class.php");

require_once($_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/pages/Kaltura/Biz/uwsKalturaClient.php");


class datatableData
{
    public $data = array();

}

session_name("UWS");
session_set_cookie_params(
    ['secure' => true,
        'samesite' => 'None'
    ]
);
session_start();

if (!isset($_SESSION['isKalturaPowerUser']))
    $_SESSION['isKalturaPowerUser'] = false;

if ($_SESSION["isKalturaPowerUser"] === false) {

    echo("no access");
    exit;
}

$lms_url = $_SESSION['OAuthDomain'];
$token = $lms_token[$lms_url];


$sharedsecret =  uws_encrypt::encrypt_decrypt('decrypt', $kaltura_tokens[$lms_url]["kaltura_admin_secret"],$mykey);





if (isset($_GET["LoginId"]) == false)
{
    echo ("Missing query parms!");
    exit;
}


$kaltura = new uwsKalturaclient($kaltura_tokens[$lms_url]["kaltura_partner_id"],$sharedsecret, $_GET["LoginId"]);

$the_user_ks_to_use = $kaltura->ksession;

?>
<!DOCTYPE html>
<html>
<head>
    <title>Chunked file upload with Kaltura</title>

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" media="screen" rel="stylesheet" type="text/css" />

    <link href="/<?php echo explode("/",$_SERVER["PHP_SELF"])[1];?>/pages/Kaltura/css/jquery.fileupload-ui.css" media="screen" rel="stylesheet" type="text/css" />
    <link href="/<?php echo explode("/",$_SERVER["PHP_SELF"])[1];?>/pages/Kaltura/css/jquery.fileupload-ui-kaltura.css" media="screen" rel="stylesheet" type="text/css" />

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script type="text/javascript" src="/<?php echo explode("/",$_SERVER["PHP_SELF"])[1];?>/pages/Kaltura/js/jquery.ui.widget.js"></script>
    <script type="text/javascript" src="/<?php echo explode("/",$_SERVER["PHP_SELF"])[1];?>/pages/Kaltura/js/jquery.iframe-transport.js"></script>
    <script type="text/javascript" src="/<?php echo explode("/",$_SERVER["PHP_SELF"])[1];?>/pages/Kaltura/js/webtoolkit.md5.js"></script>
    <script type="text/javascript" src="/<?php echo explode("/",$_SERVER["PHP_SELF"])[1];?>/pages/Kaltura/js/jquery.fileupload.js"></script>
    <script type="text/javascript" src="/<?php echo explode("/",$_SERVER["PHP_SELF"])[1];?>/pages/Kaltura/js/jquery.fileupload-process.js"></script>
    <script type="text/javascript" src="/<?php echo explode("/",$_SERVER["PHP_SELF"])[1];?>/pages/Kaltura/js/jquery.fileupload-validate.js"></script>
    <script type="text/javascript" src="/<?php echo explode("/",$_SERVER["PHP_SELF"])[1];?>/pages/Kaltura/js/jquery.fileupload-kaltura.js"></script>
    <script type="text/javascript" src="/<?php echo explode("/",$_SERVER["PHP_SELF"])[1];?>/pages/Kaltura/js/jquery.fileupload-kaltura-base.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>

    <link rel="stylesheet" href="/<?php echo explode("/",$_SERVER["PHP_SELF"])[1];?>/scripts/jQuery-tagEditor-master/jquery.tag-editor.css"></link>

    <script type="text/javascript" src="/<?php echo explode("/",$_SERVER["PHP_SELF"])[1];?>/scripts/jQuery-tagEditor-master/jquery.caret.min.js"></script>
    <script type="text/javascript" src="/<?php echo explode("/",$_SERVER["PHP_SELF"])[1];?>/scripts/jQuery-tagEditor-master/jquery.tag-editor.js"></script>


    <style>
        #overlay {

            color: #666666;
            position: fixed;
            height: 100%;
            width: 100%;
            z-index: 5000;
            top: 0;
            left: 0;
            float: left;
            text-align: center;
            padding-top: 25%;
        }

        #videoOverlay{

            position: fixed;
            height: 100%;
            width: 100%;
            z-index: 5000;
            top: 0;
            left: 0;
            float: left;
            text-align: center;
            padding-top: 25%;

        }
    </style>



    <script type="text/javascript">

        $(function(){

            console.log('doc ready');
            $('#txtTags').tagEditor();



            var categoryId = -1;

            var widget = $('#uploadHook').fileupload({
                // continue to pass this, even not used, to trigger chunk upload
                maxChunkSize: 3000000,
                dynamicChunkSizeInitialChunkSize: 1000000,
                dynamicChunkSizeThreshold: 50000000,
                dynamixChunkSizeMaxTime: 30,

                host: "https://www.kaltura.com",
                apiURL: "https://www.kaltura.com/api_v3/",
                url: "https://www.kaltura.com/api_v3/?service=uploadToken&action=upload&format=1",
                ks: "<?php echo ($the_user_ks_to_use)?>",
                fileTypes: '*.mts;*.MTS;*.qt;*.mov;*.mpg;*.avi;*.mp3;*.m4a;*.wav;*.mp4;*.wma;*.vob;*.flv;*.f4v;*.asf;*.qt;*.mov;*.mpeg;*.avi;*.wmv;*.m4v;*.3gp;*.jpg;*.jpeg;*.bmp;*.png;*.gif;*.tif;*.tiff;*.mkv;*.QT;*.MOV;*.MPG;*.AVI;*.MP3;*.M4A;*.WAV;*.MP4;*.WMA;*.VOB;*.FLV;*.F4V;*.ASF;*.QT;*.MOV;*.MPEG;*.AVI;*.WMV;*.M4V;*.3GP;*.JPG;*.JPEG;*.BMP;*.PNG;*.GIF;*.TIF;*.TIFF;*.MKV;*.AIFF;*.arf;*.ARF;*.webm;*.WEBM;*.rm;*.RM;*.ra;*.RA;*.RV;*.rv;*.aiff',
                context: '',
                categoryId: categoryId,
                messages: {
                    acceptFileTypes: 'File type not allowed',
                    maxFileSize: 'File is too large',
                    minFileSize: 'File is too small'
                },
                android: "",
                singleUpload: ""

            })
            // file added
                .bind('fileuploadadd',function(e, data){

                    console.log('fileuploaded');
                    var uploadBoxId = widget.fileupload('getUploadBoxId',e,data);
                    data.uploadBoxId = uploadBoxId;
                    var uploadManager = widget.fileupload("getUploadManager");
                    if (!uploadManager.hasWidget($(this)) && !widget.fileupload('option', 'android') && !widget.fileupload('option', 'singleUpload')) {
                        // load the next uploadbox (anyway even if there is an error)
                        widget.fileupload('addUploadBox',e,data);
                    }
                })
                // actual upload start
                .bind('fileuploadsend',function(e, data){
                    console.log('fileuploadsend');
                    var uploadBoxId = widget.fileupload('getUploadBoxId',e,data);
                    var file = data.files[0];
                    var uploadManager = widget.fileupload("getUploadManager");

                    if(file.error === undefined){
                        var context = widget.fileupload('option', 'context');
                        console.log('upload context: ' + context);
                        console.log('now uploading file name: ' + encodeURIComponent(file.name));
                        $("#txtName").val(file.name);
                        //here we should display an edit entry form to allow the user to add/save metadata to the entry
                        $("#uploadbox" + uploadBoxId + " .entry_details").removeClass('hidden');
                    } else {
                        console.log('some kind of error when starting to upload ' + file.error);
                    }
                })
                // upload done
                .bind('fileuploaddone', function(e, data){
                    console.log('fileuploaddone');
                    var uploadBoxId = widget.fileupload('getUploadBoxId',e,data);
                    var file = data.files[0];
                    $("#UploadToken").val( data.uploadTokenId);
                    $("#MediaInfo").show();
                    // console.log('upload complete success: ' + encodeURIComponent(file.name) + '/token/'+ data.uploadTokenId + '/boxId/' + uploadBoxId);
                    //console.log('Next, call the media.add and media.addContent API actions to create your Kaltura Media Entry and associate it with this newly uploaded file. Once media.addContent is called, the transcoding process will begin and your media entry will be prepared for playback and sharing. Use the Kaltura JS client library or call your backend service to execute media.add and media.addContent passing the uploadTokenId.');
                })
                // upload error
                .bind('fileuploaderror', function(e, data){
                    console.log('fileuploaderror');
                    var uploadBoxId = widget.fileupload('getUploadBoxId',e,data);
                    var uploadBox = widget.fileupload('getUploadBox',uploadBoxId);
                    $("#entry_details", uploadBox).addClass('hidden');
                    if (widget.fileupload('option', 'singleUpload')){
                        // load the next uploadbox (if an error occured and it's a single upload do not cause a dead end for the user)
                        widget.fileupload('addUploadBox',e,data);
                    }
                })
                // upload cancelled
                .bind('fileuploadcancel', function(e, data){
                    console.log('fileuploadcancel');
                    var uploadBoxId = widget.fileupload('getUploadBoxId',e,data);
                    var uploadBox = widget.fileupload('getUploadBox',uploadBoxId);
                    $("#entry_details", uploadBox).addClass('hidden');
                    console.log('Upload Cancel');
                });

            // bind to the first upload input
            $("#uploadbox1 #fileinput").bind('change', function (e) {
                $('#uploadHook').fileupload('add', {
                    fileInput: $(this),
                    uploadBoxId: 1
                });
                console.log('file chosen');
            });
        });

        function SubmitEntry ()
        {
            var caption  = false;

            $("#overlay").show();
            if($("#checkbox").is(':checked'))
                caption = true;

            var entryData = "UploadToken=" + $("#UploadToken").val()
                + "&LoginId="+  $("#LoginId").val()
                + "&ks=" + $("#ks").val()
                + "&name=" + $("#txtName").val()
                + "&description=" + $("#txtDescription").val()
                + "&tags=" + $("#txtTags").tagEditor('getTags')[0].tags
                + "&AutoCaption="+ caption;

            var url = "<?php echo  "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/pages/Kaltura/API/V1/addMedia.php";?>";


            $.ajax({
                url : url,
                type: "POST",
                data : entryData,
                success: function(data, textStatus, jqXHR)
                {
                    $("#overlay").hide();

                    $.confirm({
                        title: 'Upload Complete',
                        content: 'Load another!',
                        buttons: {
                            Yes: function () {
                                location.reload();
                            },
                            No: function () {
                                window.location = '/<?php echo explode("/", $_SERVER["PHP_SELF"])[1];?>/pages/Kaltura/index.php';

                            }
                        }
                    });


                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    $.dialog(errorThrown);

                }
            });


        }
    </script>

</head>

<body>

<div id="uploadHook"></div>
<div class="row-fluid">
    <h2>Load media for <?php echo  $_GET["LoginId"]; ?></h2>
    <h3>Step 1 - Choose Media</h3>
    <div id="uploadbox1">
        <div class="well">
            <div class="wrap_me">
                <div class="relative">
                    <div id="uploadbutton">
                        <form id='fileupload'>
				            <span class="btn btn-primary btn-large fileinput-button">
				            	<i class="icon-plus icon-white"></i>&nbsp;Choose a file to upload	    			            <input id="fileinput" class="fileinput" data-uploadboxid="1" type="file" name="fileData" accept=".mts,.MTS,.qt,.mov,.mpg,.avi,.mp3,.m4a,.wav,.mp4,.wma,.vob,.flv,.f4v,.asf,.qt,.mov,.mpeg,.avi,.wmv,.m4v,.3gp,.jpg,.jpeg,.bmp,.png,.gif,.tif,.tiff,.mkv,.QT,.MOV,.MPG,.AVI,.MP3,.M4A,.WAV,.MP4,.WMA,.VOB,.FLV,.F4V,.ASF,.QT,.MOV,.MPEG,.AVI,.WMV,.M4V,.3GP,.JPG,.JPEG,.BMP,.PNG,.GIF,.TIF,.TIFF,.MKV,.AIFF,.arf,.ARF,.webm,.WEBM,.rm,.RM,.ra,.RA,.RV,.rv,.aiff">
				        	</span>
                        </form>
                    </div>
                    <span id="upload-file-info" class="label"></span>
                    <button class="cancelBtn btn hidden pull-right btn-danger" title="Cancel" id="cancelBtn">Cancel</button>
                </div>

                <div id="progress" class="progress hidden">
                    <div id="progressBar" class="bar active"></div>
                    <div class="anchor"><div class="message"></div></div>
                </div>

                <div id="successmsg" class="alert alert-success hidden text-center">
		            <span class="large">
		                <strong>Upload Completed!</strong> </div>
                <p class="uploadMediaDetails">All common video, audio and image formats in all resolutions are accepted by Kaltura. </div>
            <div id="entry_details" class="entry_details hidden">
            </div>
        </div>
    </div>
    <div id="MediaInfo" hidden>
        <H3>Step 2 - Media Info</H3>
        <div class="well">
            <div class="wrap_me">
                <div class="relative">
                    <div class="border border-light p-5">



                        <input type="hidden" id="UploadToken" >
                        <input type="hidden" id="ks" value="<?php echo $the_user_ks_to_use; ?>">
                        <input type="hidden" id="LoginId" value="<?php echo $_GET["LoginId"]; ?>">
                        <input type="text" id="txtName" class="form-control" placeholder="Name" >

                        <textarea class="form-control rounded-0" id="txtDescription" rows="3" placeholder="Description"></textarea>

                        <input type="text" id="txtTags" class="form-control mb-4" placeholder="Tags">

                        <div class="custom-control custom-checkbox mb-4">
                            <input type="checkbox" class="custom-control-input" id="checkbox">
                            <label class="custom-control-label" for="checkbox">Machine Caption</label>
                        </div>

                        <button  class="btn btn-info btn-block" id="btmSubmit" onclick="SubmitEntry();">Submit</button>
                    </div>
                </div>
            </div>
        </div></div></div>


<div id="overlay" hidden>

    <img src="<?php echo("https://".$_SERVER["SERVER_NAME"]."/".explode("/",$_SERVER["REQUEST_URI"])[1]."/images/catSpinner.gif");?>" alt="Loading" /></div><br/>
</div>

</body>
</html>
