<?php

// Init Session
require_once $_SERVER["DOCUMENT_ROOT"]."/".Explode("/",$_SERVER["REQUEST_URI"])[1]."/vendor/autoload.php";

session_name("UWS");
session_set_cookie_params(
    ['secure'=>true,
        'samesite'=> 'None'
    ]
);

session_start();

$_SESSION["isKalturaPowerUser"] = true;


session_commit();

$redirect = "https://".$_SERVER["SERVER_NAME"]."/".explode("/",$_SERVER["REQUEST_URI"])[1]."/pages/Kaltura/index.php";
header("Location: ".$redirect);
exit();