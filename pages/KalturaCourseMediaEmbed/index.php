<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/".Explode("/",$_SERVER["REQUEST_URI"])[1]."/vendor/autoload.php";
require_once $_SERVER["DOCUMENT_ROOT"]."/".Explode("/",$_SERVER["REQUEST_URI"])[1].'/settings.php';
require_once $_SERVER["DOCUMENT_ROOT"]."/".Explode("/",$_SERVER["REQUEST_URI"])[1]."/lib/encrypt/key.php";
require_once $_SERVER["DOCUMENT_ROOT"]."/".Explode("/",$_SERVER["REQUEST_URI"])[1]."/lib/encrypt/uws_encrypt.php";

require_once $_SERVER["DOCUMENT_ROOT"]."/".Explode("/",$_SERVER["REQUEST_URI"])[1]."/pages/KalturaCourse/biz/uws_kaltura_user.php";

session_name("UWS");
session_set_cookie_params(
    ['secure'=>true,
        'samesite'=> 'None'
    ]
);

session_start();


if ( !isset($_SESSION["post"]["custom_uwsa_uuid"]))
{
    echo ("This tool requires the use of cookies. Please enable cookies." );
    http_response_code(403);
    exit();
}

$course_uuid = $_SESSION["post"]["custom_uwsa_uuid"];
$user_login = $_SESSION["post"]["custom_canvas_user_login_id"];

$apiToken = uws_encrypt::encrypt_decrypt('decrypt',$restGatewayToken,$mykey);

$lms_url = $_SESSION['OAuthDomain'];
$token = $lms_token[$lms_url];
$courseID = "CourseMedia-".$_SESSION["launch_params"]["custom_canvas_course_id"];


$sharedsecret =  uws_encrypt::encrypt_decrypt('decrypt', $kaltura_tokens[$lms_url]["kaltura_admin_secret"],$mykey);


# ------------------------------
# START CONFIGURATION SECTION
#

$launch_url = "https://".$kaltura_tokens[$lms_url]["kaltura_partner_id"].".kaf.kaltura.com/browseandembed/index/index";
$key = $kaltura_tokens[$lms_url]["kaltura_partner_id"];
$secret = $sharedsecret;



$launch_data = array(
    "user_id" => $courseID,
    "custom_canvas_course_id"=>$courseID,
    "custom_canvas_user_login_id"=>$courseID,
    "roles" => "Instructor",
    "lis_person_name_full" => $_SESSION["post"]["context_title"],
    "lis_person_name_family" => $_SESSION["post"]["context_title"],
    "lis_person_name_given" => "Course Media",
    "lis_person_contact_email_primary" => $courseID."@wisconsin.edu",
    "lis_person_sourcedid" => "school.edu:user",

    "context_id" => $_SESSION["post"]["context_id"],
    "context_title" => $_SESSION["post"]["context_title"],
    "context_label" => $_SESSION["post"]["context_label"],
    "tool_consumer_instance_guid" => $_SESSION["post"]["tool_consumer_instance_guid"],
    "tool_consumer_instance_description" => $_SESSION["post"]["tool_consumer_instance_name"]
);

#
# END OF CONFIGURATION SECTION
# ------------------------------

$now = new DateTime();

$launch_data["lti_version"] = "LTI-1p0";
$launch_data["lti_message_type"] = "ContentItemSelectionRequest";

# Basic LTI uses OAuth to sign requests
# OAuth Core 1.0 spec: http://oauth.net/core/1.0/

$launch_data["oauth_callback"] = "about:blank";
$launch_data["oauth_consumer_key"] = $key;
$launch_data["oauth_version"] = "1.0";
$launch_data["oauth_nonce"] = uniqid('', true);
$launch_data["oauth_timestamp"] = $now->getTimestamp();
$launch_data["oauth_signature_method"] = "HMAC-SHA1";

# In OAuth, request parameters must be sorted by name
$launch_data_keys = array_keys($launch_data);
sort($launch_data_keys);

$launch_params = array();
foreach ($launch_data_keys as $key) {
    array_push($launch_params, $key . "=" . rawurlencode($launch_data[$key]));
}

$base_string = "POST&" . urlencode($launch_url) . "&" . rawurlencode(implode("&", $launch_params));
$secret = urlencode($secret) . "&";
$signature = base64_encode(hash_hmac("sha1", $base_string, $secret, true));

?>

<html>
<head>

    <script
            src="https://code.jquery.com/jquery-3.4.1.min.js"
            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
            crossorigin="anonymous"></script>

    <script
            src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
            integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
            crossorigin="anonymous"></script>

    <script>
        $(document).ready(function(){
            $('#submit').click();
        });
    </script>

</head>
<!-- <body onload="document.ltiLaunchForm.submit();"> -->
<body>
<form id="ltiLaunchForm" name="ltiLaunchForm" method="POST" action="<?php printf($launch_url); ?>">
    <?php foreach ($launch_data as $k => $v ) { ?>
        <input type="hidden" name="<?php echo $k ?>" value="<?php echo $v ?>">
    <?php } ?>
    <input type="hidden" name="oauth_signature" value="<?php echo $signature ?>">
    <button id="submit" type="submit" style="display: none">Launch</button>
</form>
<body>
</html>