
<?php


$lms_url = $_SESSION['OAuthDomain'];
$token = $lms_token[$lms_url];


$base = $_SERVER["PHP_SELF"];
$pos = strpos($_SERVER["PHP_SELF"], "index.php");
if ($pos !== false)
    $base = rtrim($_SERVER["PHP_SELF"], "/index.php");
else
    $base = rtrim($_SERVER["PHP_SELF"], "/API/V1/OAuthRedirect.php");


// Init Canvas Biz class

$timeZ = new DateTimeZone("America/Chicago");

$canvas = new uws_canvas(uws_encrypt::encrypt_decrypt('decrypt',$token, $mykey), $lms_url);

//Note $render_parms is an array of the LTI post parms.
$courseID = $render_params["course_id"];
//206932; //
//"199283";
// Get Course Users
$course_users = $canvas->get_users($courseID);
$course = $canvas ->get_course($courseID);
$term = $canvas -> get_term($course[0]->term ->id);

//if (is_null($course[0]->term->end_at)== true) {

//    echo("The course's term does not have an end date.");
//    exit();
//}
if (is_null($course_users)== true)
{
    echo ("No available users to extend access.");
    exit();

}



$st = null;
if (is_null($course[0]->term->end_at)== true) {
    $st = strtotime('+5 years');
} else $st =  strtotime( $course[0]->term->end_at);
$date =  new DateTime( "@$st");
date_sub($date, date_interval_create_from_date_string('5 hours'));


$termStudentEnd = null;

if (isset($term[0]->overrides->StudentEnrollment->end_at) !== true) {
    $termStudentEnd = strtotime (date("m/d/y"));}
else  $termStudentEnd = strtotime($term[0]->overrides->StudentEnrollment->end_at);

$termStudentEndDate = new DateTime("@$termStudentEnd");

date_sub($termStudentEndDate, date_interval_create_from_date_string('5 hours'));

?>

<!DOCTYPE html>

<html>

<head>
    <link rel="stylesheet"  href="scripts/common.css">
    <script
            src="https://code.jquery.com/jquery-3.4.1.min.js"
            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
            crossorigin="anonymous"></script>

    <script
            src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
            integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
            crossorigin="anonymous"></script>
    <link href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet" />
    <link src="scripts/common.css"/>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/js/select2.min.js"></script>

    <link rel="stylesheet" type="text/css" href="scripts/overhang.min.css" />
    <script type="text/javascript" src="scripts/overhang.min.js"></script>


    <!-- Add icon library -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <script>



        $( function() {

            var mdate = '<?php echo $date->format('m/d/Y');  ?>';
            var mindate = '<?php
                if ($date !== $termStudentEndDate)
                    echo $termStudentEndDate->format('m/d/Y') ;
                else
                    echo (new \DateTime())->format('m/d/Y');
                ?>';



            $( "#endDate" ).datepicker(
                {
                    constrainInput:true,

                    maxDate: mdate,
                    minDate: mindate

                }

            );
        } );


        $(document).ready(function() {
            $(".jsUsers").select2(); });


        function submitForm (e)
        {
            e.preventDefault();

            var post_url = $("#form_i").attr("action"); //get form action url
            var request_method = $("#form_i").attr("method"); //get form GET/POST method
            var form_data = $("#form_i").serialize(); //Encode form elements for submission


            // Validate there are users!
            var jsUsers = $(".jsUsers").find(':selected');

            if (jsUsers.length == 0)
            {
                $("body").overhang({
                    type: "warn",
                    message: "Must select at least one user!",
                    closeConfirm:true
                });
                isValid = false;
                return;

            }

            // Validate end date has been selected
            var currentDate = $( "#endDate" ).datepicker( "getDate" );
            var minDate = new Date( $( "#endDate" ).datepicker( "option", "minDate" ));
            var maxDate = new Date($( "#endDate" ).datepicker( "option", "maxDate" ));

            if (currentDate == null){currentDate = "";}
            if (currentDate.length  == 0)
            {
                $("body").overhang({
                    type: "warn",
                    message: "Must select end date!",
                    closeConfirm:true
                });
                isValid = false;
                return;

            }



            if (currentDate > maxDate )
            {
                $("body").overhang({
                    type: "warn",
                    message: "Extension can't exceed maximum term date!",
                    closeConfirm:true
                });
                isValid = false;
                return;

            }

            if (currentDate < minDate )
            {
                $("body").overhang({
                    type: "warn",
                    message: "Extension can't start before minimum date !",
                    closeConfirm:true
                });
                isValid = false;
                return;

            }



            var reasonval = document.getElementById("reason").value;
            if (reasonval == "")
            {
                $("body").overhang({
                    type: "warn",
                    message: "Must provide a reason for the incomplete!",
                    closeConfirm:true
                });
                isValid = false;
                return;

            }





            $('.jsUsers').val(null).trigger('change');
            document.getElementById("reason").value = "";
            //$('.reason').val('');

            // Notify User that the request has been made
            //$("body").overhang({
            //    type: "success",
            //    message: "Incomplete Requested"
            //});


            $("#submit").html("<i id=\"spinner\" class=\"fa fa-spinner fa-spin\"></i>Extending Access");


            $.ajax({
                url: post_url,
                crossDomain:true,
                type: request_method,
                data: form_data

            }).done(function (response) { //
                if (response.trim() === "GOOD"){
                    // $("#spinner").removeClass("fa-search").removeClass("fa-spinner fa-spin");
                    // $("#submit").html("Extend Access");
                    $("#submit").html("<i id=\"spinner\" class=\"fa\"></i>Extend Access");

                    $("body").overhang({
                        type: "success",
                        message: "Access Extended"
                    });
                    // $("body").overhang({
                    //    type: "success",
                    //  message: "Complete"
                    console.debug("Enrollment Complete")
                    // });
                    //
                }
                else
                {
                    alert(response);
                    $("body").overhang({
                        type: "error",
                        message: "An error has occured!",
                        closeConfirm: true
                    });

//                        $("#spinner").removeClass("fa-search").removeClass("fa-spinner fa-spin");
                    //                      $("#submit").html("Extend Access");
                    $("#submit").html("<i id=\"spinner\" class=\"fa\"></i>Extend Access");

                }


            });
            return false;

        }

    </script>


</head>
<body>
<h1>Extend Student Access</h1>
<!--<h2>--><?php //echo "Course - ".$course[0]->id." - ".$course[0]->name; ?><!-- </h2>-->

<p>Student's access may be extended for those requiring additional course access <?php
    if (isset($term[0]->overrides->StudentEnrollment->end_at)== true)
    {
        echo ("after <b>".$termStudentEndDate->format('m')."/".$termStudentEndDate->format('d')."/".$termStudentEndDate->format('Y')." ".
            $termStudentEndDate->format('g:i A')."</b>");
    }
    ?>.
    <?php
    if (is_null($course[0]->term->end_at)== false)
    {
        echo (" The maximum extension that may be given is <b>".$date->format('m')."/".$date->format('d')."/".$date->format('Y')." ".$date->format('g:i A')."</b>.");
    }
    ?>
</p>
<p>
    Note: The end date's time will be set to 11:59 PM.
</p>

<?php

?>




<form id="form_i" action="API/V1/enroll_incomplete.php" method="post"  onsubmit="return submitForm(event)">
    <div><input hidden name="courseID" value="<?php echo $courseID ?>"</div>
    <div >
        <label >User:</label>
        <select class="jsUsers" id="jsUsers" name="users[]" multiple="multiple" data-width="100%" >

            <?php foreach($course_users as $course_user){
                echo "<option value='".$course_user->id."'>".$course_user->sis_user_id." - ".$course_user->short_name."</option> \r\n";
            }
            ?>
        </select>
    </div>


    <div>
        <label >End Date: </label>
        <input name="endDate" type="text" id="endDate"/>
    </div>

    <div>
        <label for="reason">Reason for Incomplete:</label>
        <textarea name="reason" id="reason" placeholder="Reason for Extension"></textarea>
    </div>
    <div>
        <button class="btn btn-default" id="submit"> <i id="spinner" style="padding-right: 20px;" class="fa"></i>Extend Access</button>
    </div>


</form>


</body>


</html>

