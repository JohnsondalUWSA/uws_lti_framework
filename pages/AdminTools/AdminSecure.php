<?php
$roles =array("AccountAdmin","Institution Admin");

require_once $_SERVER["DOCUMENT_ROOT"]."/".Explode("/",$_SERVER["REQUEST_URI"])[1]."/vendor/autoload.php";

session_name("UWS");
session_set_cookie_params(
    ['secure'=>true,
        'samesite'=> 'None'
    ]
);

session_start();


require (dirname(__FILE__) . "/../../settings.php");
require_once(dirname(__FILE__).'/../../lib/encrypt/key.php');
require_once(dirname(__FILE__).'/../../lib/encrypt/uws_encrypt.php');
require_once(dirname(__FILE__).'/../../lib/uws_canvas.php');
require_once(dirname(__FILE__).'/../../lib/logger.php');



// Get LMS Domain and Token
$lms_url = $_SESSION['OAuthDomain'];
$token = $lms_token[$lms_url];


if (!isset ($_SESSION['valid']))
{
    http_response_code(403);
    exit();
}

if (isset($_SESSION["post"]["custom_accountid"]) === false)
{
    echo ("This tool requires a custom lti setting 'accountid'" );
    http_response_code(403);
    exit();

}



if (!isset ($_SESSION['isAdmin']))
    $_SESSION['isAdmin'] = false;



if ($_SESSION['isAdmin'] == false)
{
    $accountId = $post_input["custom_accountid"];
    $isAdmin = false;
    $canvas = new uws_canvas(uws_encrypt::encrypt_decrypt('decrypt',$token, $mykey), $lms_url);

    // Work way up accounts till reach the top.
    while (!is_null($accountId))
    {
        $isAdmin = isAdminforAccount ($canvas, $roles, $accountId, $post_input["custom_canvas_user_id"]);

        if ($isAdmin == true)
        {
            $_SESSION['isAdmin'] = true;
            session_commit();
            break;
        }
        else $accountId = getAccountParentID ($canvas, $accountId);
    }



}

function getAccountParentID ($api , $id)
{
    $account = $api -> get("/accounts/".$id, null);
    return $account[0]->parent_account_id;

}

function isAdminforAccount ($api, $_roles, $accountId, $userCanvasID)
{
    $admins = $api -> get("/accounts/".$accountId."/admins", null);
    foreach ($admins as $item)
    {
        if ($item->user ->id == $userCanvasID)
        foreach ($_roles as $role)
        {
            if ($role == $item-> role)
                return true;
        }


    }
    return false;
}

