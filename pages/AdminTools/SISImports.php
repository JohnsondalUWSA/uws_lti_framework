<?php

require_once $_SERVER["DOCUMENT_ROOT"]."/".Explode("/",$_SERVER["REQUEST_URI"])[1]."/vendor/autoload.php";

session_name("UWS");
session_set_cookie_params(
    ['secure'=>true,
        'samesite'=> 'None'
    ]
);

session_start();

if (!isset($_SESSION['isAdmin']))
    $_SESSION['isAdmin'] = false;

if ($_SESSION["isAdmin"]=== false)
{
    echo ("This tool requires admin access" );
    http_response_code(403);
    exit();
}

require (dirname(__FILE__) . "/../../settings.php");
require_once(dirname(__FILE__).'/../../lib/encrypt/key.php');
require_once(dirname(__FILE__).'/../../lib/encrypt/uws_encrypt.php');
require_once(dirname(__FILE__).'/../../lib/uws_canvas.php');
require_once(dirname(__FILE__).'/../../lib/logger.php');


// Validate Custom LTI parms have been set.

if (isset($_SESSION["post"]["custom_accountsisid"]) === false)
{
    echo ("This tool requires a custom lti setting 'accountSISID'" );
    exit ();

}
if (isset($_SESSION["post"]["custom_accountid"]) === false)
{
    echo ("This tool requires a custom lti setting 'accountid'" );
    exit();

}

$campus = "ALL";
// Check to see if account is a sis account and determine campus

if ($_SESSION["post"]["custom_accountid"] !== "1")
{
    if (preg_match ('/UW[A-Z][A-Z][A-Z]-/m',$_SESSION["post"]["custom_accountsisid"] ) === false)
    {
        echo ("This tool can only be used on accounts identified as being sis accounts");
        exit();
    }
    else
    {
        $campus = substr($_SESSION["post"]["custom_accountsisid"],2,3);
    }


}



// Get LMS Domain and Token
$lms_url = $_SESSION['OAuthDomain'];
$token = $lms_token[$lms_url];


// Init Canvas Biz class
// Class uses to make API Calls.

$canvas = new uws_canvas(uws_encrypt::encrypt_decrypt('decrypt',$token, $mykey), $lms_url);


$sisImports = array();

// Note Pulling Last 400 imports.  Data will be sorted by campus below.
$sisImports = $canvas -> get("/accounts/1/sis_imports",null, 900, "sis_imports");


?>

<!DOCTYPE html>

<html>

<head>


    <script
        src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
        crossorigin="anonymous"></script>

    <script
        src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
        integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
        crossorigin="anonymous"></script>
    <link href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet" />

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.1/b-html5-1.6.1/r-2.2.3/datatables.min.css"/>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.1/b-html5-1.6.1/r-2.2.3/datatables.min.js"></script>




    <script>


        $(document).ready( function () {
           var table =  $('#tbsisImports').DataTable(
                {
                    dom: 'Bfrtip',
                    buttons: [
                        'copy', 'excel', 'pdf'
                    ]


                }

            );

           table.order([0,'desc']).draw();
        }


        );

    </script>


</head>
<body>
<h1>SIS IMPORTS</h1>

<table id="tbsisImports" class="display" width="85%">
    <thead>



    <tr>
        <th>ID</th>
        <th>CAMPUS</th>
        <th>STATUS</th>
        <th>PERCENT</th>
        <th>CREATED</th>
        <th>STARTED</th>
        <th>UPDATED</th>
        <th>ENDED</th>
    </tr>
    </thead>

    <tbody>
    <?php foreach ($sisImports as $item)
    {
        if($campus == strtoupper($item->user->sis_user_id) or $campus == "ALL") {
            echo("<tr>");

            echo ("<td> <a href="."/".explode("/",$_SERVER["REQUEST_URI"])[1]."/pages/AdminTools/SISImport.php?id=".$item->id." target=\"blank\">".$item->id."</a></td>");
            echo ("<td>" . $item->user->sis_user_id . "</td>
                    <td>" . $item->workflow_state . "</td>
                    <td><progress max='100' value='" . $item->progress . "'>".$item->progress."</progress></td>
                    <td>" . $item->created_at . "</td>
                    
                    <td>" . $item->started_at . "</td>
                    <td>" . $item->updated_at . "</td>
                    <td>" . $item->ended_at . "</td>");

            echo("</tr>");
        }
        }?>



    </tbody>
</table>


</body>
</html>







