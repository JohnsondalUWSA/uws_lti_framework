<?PHP

require_once  ($_SERVER["CONTEXT_DOCUMENT_ROOT"]."/".explode('/',$_SERVER["PHP_SELF"])[1]."/lib/uws_canvas.php");

class BulkInbox
{
// Canvas host name
    public $lms_url;
    public $token;
    public $canvas;
    public function __construct($lms_url, $token)
    {
        $this -> lms_url = $lms_url;
        $this -> token = $token;
        $this ->canvas = new uws_canvas($token, $lms_url);

    }

    public function get_userID ($sis_user_id)
    {
        $apiCall = "/users/sis_user_id:".$sis_user_id;
        return $user = $this -> canvas -> get($apiCall, null);
    }

    public function addConversation ($sis_user_id , $first_name, $last_name, $surveyLink,$sender,$subject, $body,$preview)
    {
        $userID = null;
        if ($preview === false) {
            $userID=  $this->get_userID($sis_user_id);
            $userID = $userID[0]->id;
        } else $userID = $sis_user_id;

        if (isset($userID)) {

            $subject = str_replace("{firstname}",$first_name,$subject);
            $subject = str_replace("{lastname}",$last_name,$subject);
            $subject = str_replace("{surveylink}",$surveyLink,$subject);

            $body = str_replace("{firstname}",$first_name,$body);
            $body = str_replace("{lastname}",$last_name,$body);
            $body = str_replace("{surveylink}",$surveyLink,$body);



         //   $subject = urlencode($subject);
          //  $body = urlencode($surveyLink);


            $apiCall = "/conversations?recipients[]=".$userID."&as_user_id=".$sender;

            $postfields = array('recipients[]' => $userID,
                'body' => $body,
                'subject' => $subject,
                'force_new' => 'true');
            

           $response = $this-> canvas->post( $apiCall, $postfields);
            if (!isset($response[0]->id))
                return $response;
            else
                return "true";

        }
        else return "SIS USER NOT FOUND - ".$sis_user_id;



    }



}

