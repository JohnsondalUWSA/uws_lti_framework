<?php

session_name("UWS");
session_set_cookie_params(
    ['secure'=>true,
        'samesite'=> 'None'
    ]
);

session_start();

if (!isset($_SESSION['isAdmin']))
    $_SESSION['isAdmin'] = false;

if ($_SESSION["isAdmin"]=== false)
{
    echo ("This tool requires Instutional Admin or higher access!" );
    http_response_code(403);
    exit();
}

?>

<!DOCTYPE html>

<html>

<head>

    <script
            src="https://code.jquery.com/jquery-3.4.1.min.js"
            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
            crossorigin="anonymous"></script>

    <script
            src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
            integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
            crossorigin="anonymous"></script>
    <link href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet" />

    <!-- Add icon library -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/<?php echo explode("/",$_SERVER["PHP_SELF"])[1];?>/scripts/common.css" >
    <script>
        function submitForm (e) {

            $("#submit").html("<i id=\"spinner\" class=\"fa fa-spinner fa-spin\"></i>Processing");
            $("#submit").prop('disabled',true);
            var form = $('#myForm')[0];
            var datab = new FormData(form);

            $.ajax({
                url: "API/V1/BulkInboxLoad.php",
                type: "POST",
                data: datab,
                contentType: false,
                cache: false,
                processData:false,
                success: function(data) {
                    $("#response").html(data);

                    $("#submit").html("<i id=\"spinner\" class=\"fa\"></i>Upload");
                    $("#submit").prop('disabled',false);
                }
            });

        }
</script>

        </head>
<body>
<h3>Bulk Notification Upload Tool</h3>
<p>This tool will upload the CSV to Canvas.  Messages will be placed in each student's inbox contained within the spreadsheet, up to 250 at a time. </p>
<p>Note: More details on using the tool can be found at <a href="https://kb.wisconsin.edu/dle/98196" target="_blank">https://kb.wisconsin.edu/dle/98196</a></p>
<form enctype='multipart/form-data' action='' method='post' id="myForm" >
<div>
    <label>Subject</label><input type="text" name="subject">
</div>
<div>
    <label>Body</label><textarea type="text" name="body"></textarea>
</div>

<div>
    <label>Preview Only</label>
    <input type="checkbox" name="preview">
</div>

    <div>
        <label>Campus Prefix</label>
        <input type="text" maxlength="3" name="CampusPrefix"
    </div>
<div>
<label>Upload CSV file Here</label>

<input size='50' type='file' name='filename' accept=".csv">
</div>
</br>


</form>
<button class="btn btn-default" id="submit" onclick="submitForm();"> <i id="spinner" style="padding-right: 20px;" class="fa"></i>Upload</button>
<div id="response"></div>

</body>