
<?php


// Init Session
require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/vendor/autoload.php";

session_name("UWS");
session_set_cookie_params(
    ['secure' => true,
        'samesite' => 'None'
    ]
);

session_start();
if (!isset($_SESSION['isAdmin']))
    $_SESSION['isAdmin'] = false;

if ($_SESSION["isAdmin"]=== false)
{
    echo ("This tool requires admin access" );
    http_response_code(403);
    exit();
}

?>

<html>
    <head>
        <script
            src="https://code.jquery.com/jquery-3.4.1.min.js"
            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
            crossorigin="anonymous"></script>

        <script
            src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
            integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
            crossorigin="anonymous"></script>


        <link href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet" />

        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.1/b-colvis-1.6.1/b-flash-1.6.1/b-html5-1.6.1/b-print-1.6.1/cr-1.5.2/kt-2.5.1/r-2.2.3/rr-1.2.6/sc-2.0.1/sp-1.0.1/sl-1.3.1/datatables.min.css"/>

        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.1/b-colvis-1.6.1/b-flash-1.6.1/b-html5-1.6.1/b-print-1.6.1/cr-1.5.2/kt-2.5.1/r-2.2.3/rr-1.2.6/sc-2.0.1/sp-1.0.1/sl-1.3.1/datatables.min.js"></script>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>

        <style>
            #overlay {

                color: #666666;
                position: fixed;
                height: 100%;
                width: 100%;
                z-index: 5000;
                top: 0;
                left: 0;
                float: left;
                text-align: center;
                padding-top: 25%;
            }

            #videoOverlay{

                position: fixed;
                height: 100%;
                width: 100%;
                z-index: 5000;
                top: 0;
                left: 0;
                float: left;
                text-align: center;
                padding-top: 25%;

            }
        </style>

        <script>

          var tbUsers;
          var tbEnrollments;

           $(document).ready( function () {
               tbEnrollments = $('#tbEnrollments').DataTable(
                   {
                       "columns":[
                            {"data":"course_id","title":"Course Id",
                                "render": function (oObj, type, full, meta) {
                                    return  "<a href='https://<?php echo $_SESSION["OAuthDomain"];?>/courses/" + full["course_id"] + "' target='_blank'>" + full["course_id"]+ "</a>";
                   }},
                           {"data":"term", "title":"Term"},
                            {"data":"sis_course_id","title":"SIS Course Id"},
                            {"data":"sis_section_id","title":"SIS Section Id"},
                           {"data":"course_name","title":"Course"},
                            {"data":"type","title":"Role"},
                            {"data":"enrollment_state","title":"State"},
                            {"data":"last_activity_at","title":"Last Activity"},
                            {"data":"sis_import_id","title":"SIS Import Id",
                                "render":function(oObj, type, full, meta){
                                if (!full["sis_import_id"])
                                    return "";
                                else
                                    return   "<a href='https://<?php echo $_SERVER["SERVER_NAME"]."/".explode("/",$_SERVER["REQUEST_URI"])[1];?>/pages/AdminTools/SISImport.php?id=" +
                                        full["sis_import_id"] + "' target='_blank'>" + full["sis_import_id"] + "</a>";

                                }

                            }]

                   }

               );
           } );

           $(document).ready( function () {
               tbUsers = $('#tbUsers').DataTable(

                   {select: {
                           style: 'single'
                       },
                       "columns":[
                                {"data":"id", "title":"Canvas ID"},
                                {"data":"login_id", "title":"Login Id"},
                                {"data":"name", "title":"Name"},
                                {"data":"email", "title":"Email"},
                                {"data":"sis_user_id", "title":"SIS User Id"}
                           ]


               }





               );

               $('#tbUsers').on('click', 'tr', function () {
                   var data = tbUsers.row( this ).data();
                   GetUserEnrollments(data["id"]);
               } );

           } );




          $(document).keyup(function(event) {
              if ($("#userID").is(":focus") && event.key == "Enter") {
                  LookupUser();
              }
          });

           function LookupUser()
           {

               tbUsers.clear().draw();
               $("#overlay").show();

               var loookupUser = $("#userID").val();
             var  URL ="<?php echo("https://".$_SERVER["SERVER_NAME"]."/".explode("/",$_SERVER["REQUEST_URI"])[1]."/pages/AdminTools/API/V1/getUserInfo.php?login_id=");?>" + loookupUser;
             tbUsers.ajax.url(URL).load(function( settings, json ) {
                 $('#overlay').hide();
                 if (tbUsers.rows().count()> 0) {
                     tbUsers.row(':eq(0)', { page: 'current' }).select();
                     var data = tbUsers.rows({selected:true}).data();
                     GetUserEnrollments(data[0]["id"]);
                 }
             });
           }

           function GetUserEnrollments(id)
          {
              tbEnrollments.clear().draw();
              $("#overlay").show();

              var loookupUser = id;
              var  URL ="<?php echo("https://".$_SERVER["SERVER_NAME"]."/".explode("/",$_SERVER["REQUEST_URI"])[1]."/pages/AdminTools/API/V1/getUserEnrollments.php?UserID=");?>" + loookupUser;
              tbEnrollments.ajax.url(URL).load(function( settings, json ) {
                  $('#overlay').hide();

              });
          }

        </script>

    </head>
    <body>

    <h3>User Search</h3>

    <div>
        <label>User Search</label>
        <input type="text" id="userID">
        <button type="button" id="btnSearch" onclick="LookupUser();">Search</button>

    </div>
    <div>
        <table id="tbUsers" class="display">
            <thead>
                <tr>

                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
    <hr>
    <div>
        <table id="tbEnrollments" class="display">
            <thead>
            <tr>

            </tr>
            </thead>
            <tbody>

            </tbody>
        </table>

    </div>

    <div id="overlay" hidden>

        <img src="<?php echo("https://".$_SERVER["SERVER_NAME"]."/".explode("/",$_SERVER["REQUEST_URI"])[1]."/images/catSpinner.gif");?>" alt="Loading" /></div><br/>
    </div>
    </body>
</html>


