<?php
require_once(dirname(__FILE__).'/AdminSecure.php');

if (!isset($_SESSION['isAdmin']))
    $_SESSION['isAdmin'] = false;

if ($_SESSION["isAdmin"]=== false)
{
    echo ("This tool requires admin access" );
    http_response_code(403);
    exit();
}
?>

<html>
<head></head>
<style>
    div {
        margin-bottom: 15px;
        padding: 4px 12px;
    }


    .warning {
        background-color: #ffffcc;
        border-left: 6px solid #ffeb3b;
    }
</style>
<body>
<h3>Admin Tools</h3>
<button onclick="location.href='<?php
    echo "/".explode("/",$_SERVER["REQUEST_URI"])[1];
    ?>/pages/AdminTools/BulkInbox.php'" type="button">Bulk Inbox</button>

<button onclick="location.href='<?php
echo "/".explode("/",$_SERVER["REQUEST_URI"])[1];
?>/pages/AdminTools/Terms.php'" type="button">Terms</button>

<button onclick="location.href='<?php
echo "/".explode("/",$_SERVER["REQUEST_URI"])[1];
?>/pages/AdminTools/AccountExport.php'" type="button">Accounts</button>

<button onclick="location.href='<?php
echo "/".explode("/",$_SERVER["REQUEST_URI"])[1];
?>/pages/AdminTools/SISImports.php'" type="button">SIS Status</button>

<button onclick="location.href='<?php
echo "/".explode("/",$_SERVER["REQUEST_URI"])[1];
?>/pages/AdminTools/UserSearch.php'" type="button">User Search</button>


<div class="warning">
    <p><strong>Note:</strong>Accounts and SIS Status are slow loading. Please allow up to 90 seconds to load.</p>
</div>


</body>
</html>
