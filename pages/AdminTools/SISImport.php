<?php

require_once $_SERVER["DOCUMENT_ROOT"]."/".Explode("/",$_SERVER["REQUEST_URI"])[1]."/vendor/autoload.php";

session_name("UWS");
session_set_cookie_params(
    ['secure'=>true,
        'samesite'=> 'None'
    ]
);

session_start();

if (!isset ($_SESSION['valid']))
{
    http_response_code(403);
    exit();
}

if (!isset($_SESSION['isAdmin']))
    $_SESSION['isAdmin'] = false;

if ($_SESSION["isAdmin"]=== false)
{
    echo ("This tool requires admin access" );
    http_response_code(403);
    exit();
}

$import = $_GET["id"];


require (dirname(__FILE__) . "/../../settings.php");
require_once(dirname(__FILE__).'/../../lib/encrypt/key.php');
require_once(dirname(__FILE__).'/../../lib/encrypt/uws_encrypt.php');
require_once(dirname(__FILE__).'/../../lib/uws_canvas.php');
require_once(dirname(__FILE__).'/../../lib/logger.php');




// Get LMS Domain and Token
$lms_url = $_SESSION['OAuthDomain'];
$token = $lms_token[$lms_url];


// Init Canvas Biz class
// Class uses to make API Calls.

$canvas = new uws_canvas(uws_encrypt::encrypt_decrypt('decrypt',$token, $mykey), $lms_url);




$sisImport = $canvas -> get("/accounts/1/sis_imports/".$import,null);

header('Content-Type: application/json');
echo json_encode($sisImport);