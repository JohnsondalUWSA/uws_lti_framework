
<?php

require_once $_SERVER["DOCUMENT_ROOT"]."/".Explode("/",$_SERVER["REQUEST_URI"])[1]."/vendor/autoload.php";

session_name("UWS");
session_set_cookie_params(
    ['secure'=>true,
        'samesite'=> 'None'
    ]
);

session_start();

if (!isset($_SESSION['isAdmin']))
    $_SESSION['isAdmin'] = false;

if ($_SESSION["isAdmin"]=== false)
{
    echo ("This tool requires admin access" );
    http_response_code(403);
    exit();
}

require (dirname(__FILE__) . "/../../settings.php");
require_once(dirname(__FILE__).'/../../lib/encrypt/key.php');
require_once(dirname(__FILE__).'/../../lib/encrypt/uws_encrypt.php');
require_once(dirname(__FILE__).'/../../lib/uws_canvas.php');
require_once(dirname(__FILE__).'/../../lib/logger.php');



// Get LMS Domain and Token
$lms_url = $_SESSION['OAuthDomain'];
$token = $lms_token[$lms_url];


// Init Canvas Biz class
// Class uses to make API Calls.

$canvas = new uws_canvas(uws_encrypt::encrypt_decrypt('decrypt',$token, $mykey), $lms_url);

// Pull Canvas API
$terms = $canvas->get("/accounts/1/terms?include[]=overrides",null);

$i = 0; $rtnValue = array();

foreach ($terms as $item)
{
    $rtnValue = array_merge ($rtnValue, $item->enrollment_terms);

}

function formatDate ($dateTime)
{
    if (isset($dateTime)) {
        $dateout = gmdate("m/d/y H:i",strtotime($dateTime));
        return $dateout;
      }
    else return "";
}



?>

<!DOCTYPE html>

<html>

<head>


    <script
        src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
        crossorigin="anonymous"></script>

    <script
        src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
        integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
        crossorigin="anonymous"></script>
    <link href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet" />

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.1/b-html5-1.6.1/r-2.2.3/datatables.min.css"/>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.1/b-html5-1.6.1/r-2.2.3/datatables.min.js"></script>




    <script>


       $(document).ready( function () {
         $('#tbTerms').DataTable(
             {
                 dom: 'Bfrtip',
                 buttons: [
                     'copy', 'excel', 'pdf'
                 ]


             }

         );
       } );

    </script>


</head>
<body>
<h1>Terms</h1>

<table id="tbTerms" class="display" width="85%">
    <thead>



    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Sis</th>
        <th>Start</th>
        <th>End</th>
        <th>Teacher Start</th>
        <th>Teacher End</th>
        <th>Student Start</th>
        <th>Student End</th>
        <th>Designer Start</th>
        <th>Designer End</th>
        <th>TA Start</th>
        <th>TA End</th>
    </tr>
    </thead>

    <tbody>
        <?php foreach ($rtnValue as $item)
            {
                echo ("<tr>");
                echo ("<td>".$item->id."</td>
                    <td>".$item->name."</td>
                    <td>".$item->sis_term_id."</td>
                    <td>".formatDate($item->start_at)."</td>
                    <td>".formatDate($item->end_at)."</td>");

                if (property_exists($item->overrides,"DesignerEnrollment"))
                { echo (
                    "<td>".formatDate($item->overrides->TeacherEnrollment->start_at)."</td>
                    <td>".formatDate($item->overrides->TeacherEnrollment->end_at)."</td>
                    <td>".formatDate($item->overrides->StudentEnrollment->start_at)."</td>
                    <td>".formatDate($item->overrides->StudentEnrollment->end_at)."</td>
                    <td>".formatDate($item->overrides->DesignerEnrollment->start_at)."</td>
                    <td>".formatDate($item->overrides->DesignerEnrollment->end_at)."</td>
                    <td>".formatDate($item->overrides->TaEnrollment->start_at)."</td>
                    <td>".formatDate($item->overrides->TaEnrollment->end_at)."</td>"
        );

                }else echo ("<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>");
                echo ("</tr>");
            }?>



    </tbody>
</table>


</body>
</html>



