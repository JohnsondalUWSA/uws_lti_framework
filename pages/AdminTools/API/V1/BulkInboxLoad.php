<?php
use Analog\Analog;


{

    if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
        http_response_code(403);
        exit();

    }


    require_once $_SERVER["DOCUMENT_ROOT"]."/".Explode("/",$_SERVER["REQUEST_URI"])[1]."/vendor/autoload.php";

    session_name("UWS");
    session_set_cookie_params(
        ['secure'=>true,
            'samesite'=> 'None'
        ]
    );

    session_start();

    if (isset($encoded_session))
        $_SESSION = unserialize(($encoded_session));

    if (!isset ($_SESSION['valid']))
    {
        http_response_code(403);
        exit();
    }

    if (!isset($_SESSION['isAdmin']))
        $_SESSION['isAdmin'] = false;

    if ($_SESSION["isAdmin"]=== false)
    {
        echo ("This tool requires admin access" );
        http_response_code(403);
        exit();
    }

    $launchCanvasUserID = $_SESSION["launch_params"]["custom_canvas_user_id"];

    require_once ($_SERVER["CONTEXT_DOCUMENT_ROOT"]."/".explode('/',$_SERVER["PHP_SELF"])[1]."/pages/AdminTools/Biz/BulkInbox.php");
    require_once ($_SERVER["CONTEXT_DOCUMENT_ROOT"]."/".explode('/',$_SERVER["PHP_SELF"])[1]."/settings.php");
    require_once ($_SERVER["CONTEXT_DOCUMENT_ROOT"]."/".explode('/',$_SERVER["PHP_SELF"])[1]."/lib/encrypt/key.php");
    require_once ($_SERVER["CONTEXT_DOCUMENT_ROOT"]."/".explode('/',$_SERVER["PHP_SELF"])[1]."/lib/encrypt/uws_encrypt.php");
    require_once ($_SERVER["CONTEXT_DOCUMENT_ROOT"]."/".explode('/',$_SERVER["PHP_SELF"])[1]."/lib/logger.php");

    require_once ($_SERVER["CONTEXT_DOCUMENT_ROOT"]."/".explode('/',$_SERVER["PHP_SELF"])[1]."/lib/MeekroDB/meekrodb.2.3.class.php");


    //$_POST["subject"]

// Set MySQL Info
    DB::$host = $mysql_server;
    DB::$user = $mysql_user;
    DB::$password = uws_encrypt::encrypt_decrypt('decrypt', $mysql_password,$mykey);
    DB::$dbName = $mysql_db;

    // Init Logger Class
    $logger = new uws_logger($mysql_server, $mysql_db,$mysql_user, uws_encrypt::encrypt_decrypt('decrypt',$mysql_password,$mykey));

    // Note Logger Calls inits Static Analog Class.  Call Analog direct to log events.
    Analog::log("BulkInbox Launch".$_SESSION["post"]["custom_canvas_user_login_id"], Analog::DEBUG);

    $lms_url = $_SESSION['OAuthDomain'];
    $token = $lms_token[$lms_url];

    $msg = "";

    $valid = true;
    $subject ="";
    $body = "";

    if (isset($_POST["subject"])) {
        $subject = $_POST["subject"];
    }else {
        $valid = false;
        echo ("The subject must be provided!");
        exit();

    }

    if(isset($_POST["body"])) {
        $body = $_POST["body"];
    }else {
        $valid = false;
        echo ("The body must be provided!");
        exit();
    }
    $preview = 'off';
    if (isset($_POST["preview"]))
        $preview = $_POST["preview"];

    if (isset($_POST["CampusPrefix"]))
        $campus_prefix = $_POST["CampusPrefix"];
    else
        $campus_prefix = "";



$handle=null;
$headers=null;
$linecount = 0;

    if ($preview !=="on") {
        if (isset($_FILES['filename']['tmp_name'])) {

            if ($_FILES['filename']['size'] === 0)
            {
                echo "Must provide a file";
                exit();

            }
            // Get Row Count
            $fh = fopen($_FILES['filename']['tmp_name'], 'rb');
            while (fgets($fh) !== false) $linecount++;
            fclose($fh);

            //if ($linecount > 250) {
            //    echo "File contains more than 100 records.";
            //    exit();


           // }

            $handle = fopen($_FILES['filename']['tmp_name'], "r");
            $headers = fgetcsv($handle, 1000, ",");

            $valid = true;
            // Validate File Headers

            if ($valid and isset($headers[0])) {
                if ($headers[0] != "STUDENTID")
                    $valid = false;
            }

            if ($valid and isset($headers[1])) {
                if ($headers[1] != "FIRSTNAME")
                    $valid = false;
            }
            if ($valid and isset($headers[2])) {
                if ($headers[2] != "LASTNAME")
                    $valid = false;
            }

            if ($valid and isset($headers[3])) {
                if ($headers[3] != "SURVEYLINK")
                    $valid = false;
            }
        }
    }

    if ($valid != true and $preview !== 'on' )
        $msg = "File / Subject / Body not vaild .  Either file is not a CSV or the header of file does not match expected format.  THe header should be STUDENTID,FIRSTNAME,LASTNAME,LOGINID_PORTAL";
    else {
        $NSSE = new BulkInbox($lms_url, uws_encrypt::encrypt_decrypt('decrypt', $token, $mykey));

        if ($valid === true and $preview === "on") {
            // Preview
            $rtnValue = $NSSE->addConversation($launchCanvasUserID , $_SESSION["post"]["lis_person_name_given"], $_SESSION["post"]["lis_person_name_family"], "https://www.wisconsin.edu", $_SESSION["post"]["custom_nsseid"], $subject, $body,true);
            echo "Preview Sent";

        } else {

            // Process Files
            DB::insert('nsse_import', array(
                'LOGINID' => $_SESSION["post"]["custom_canvas_user_login_id"],
                'ISID' => $_SESSION["post"]["custom_campusid"]

            ));

            $NESS_ID = DB::insertId();

            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $good = 0;
                $message = '';
                $rtnValue = $NSSE -> addConversation($campus_prefix.$data[0],$data[1],$data[2], $data[3],  $_SESSION["post"]["custom_nsseid"],$subject,$body,false);

                if ($rtnValue != "true") {
                    $good = 0;
                    $message = $rtnValue;

                } else $good = 1;

                DB::insert('nsse_import_details', array(
                    'firstname' => $data[1],
                    'lastname' => $data[2],
                    'loginportal' => $data[3],
                    'studentid' => $data[0],
                    'success' => $good,
                    'message' => $message,
                    'NESS_ID' => $NESS_ID
                ));


                //$data[0];
                //$data[1];
            }
            fclose($handle);
        }

        echo ('Upload Complete, messages have been logged to server');

    }


}