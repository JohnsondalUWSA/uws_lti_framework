<?php

require_once $_SERVER["DOCUMENT_ROOT"]."/".Explode("/",$_SERVER["REQUEST_URI"])[1]."/vendor/autoload.php";
require_once $_SERVER["DOCUMENT_ROOT"]."/".Explode("/",$_SERVER["REQUEST_URI"])[1]."/settings.php";

require_once $_SERVER["DOCUMENT_ROOT"]."/".Explode("/",$_SERVER["REQUEST_URI"])[1]."/lib/encrypt/key.php";
require_once $_SERVER["DOCUMENT_ROOT"]."/".Explode("/",$_SERVER["REQUEST_URI"])[1]."/lib/encrypt/uws_encrypt.php";

require_once $_SERVER["DOCUMENT_ROOT"]."/".Explode("/",$_SERVER["REQUEST_URI"])[1]."/lib/logger.php";

require_once  ($_SERVER["DOCUMENT_ROOT"]."/".Explode("/",$_SERVER["REQUEST_URI"])[1]."/lib/uws_canvas.php");
require_once ($_SERVER["DOCUMENT_ROOT"]."/".Explode("/",$_SERVER["REQUEST_URI"])[1]."/lib/MeekroDB/meekrodb.2.3.class.php");

class datatableData
{
    public $data = array();

}

session_name("UWS");
session_set_cookie_params(
    ['secure'=>true,
        'samesite'=> 'None'
    ]
);
session_start();

if (!isset ($_SESSION['valid']))
{
    http_response_code(403);
    exit();
}

if (!isset($_SESSION['isAdmin']))
    $_SESSION['isAdmin'] = false;

if ($_SESSION["isAdmin"]=== false)
{
    echo ("This tool requires admin access" );
    http_response_code(403);
    exit();
}

$lms_url = $_SESSION['OAuthDomain'];
$token = $lms_token[$lms_url];

$canvas = new uws_canvas(uws_encrypt::encrypt_decrypt('decrypt', $token,$mykey), $lms_url);

$getURL = "/users/".$_REQUEST["UserID"]."/enrollments";
$enrollments = $canvas->get ($getURL, null);

$rtnValue = new datatableData();

// Remove results not containing a name or login_id
foreach ($enrollments as $enrollment)
{
        $course = $canvas ->get_course($enrollment-> course_id);
        if (isset($course[0]->term->sis_term_id))
            $enrollment -> term = $course[0]->term->sis_term_id." : ".$course[0]->term->name;
        else
            $enrollment -> term = $course[0]->term->name;

        $enrollment -> course_name = $course[0]->name;
        array_push ($rtnValue -> data, $enrollment);



}



$jsonrtn = json_encode(($rtnValue));
header('Content-type: application/json');
echo $jsonrtn;

