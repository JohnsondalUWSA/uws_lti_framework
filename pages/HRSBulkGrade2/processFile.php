<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/".Explode("/",$_SERVER["REQUEST_URI"])[1]."/vendor/autoload.php";

require_once $_SERVER["DOCUMENT_ROOT"]."/".Explode("/",$_SERVER["REQUEST_URI"])[1].'/settings.php';
require_once $_SERVER["DOCUMENT_ROOT"]."/".Explode("/",$_SERVER["REQUEST_URI"])[1].'/lib/encrypt/key.php';
require_once $_SERVER["DOCUMENT_ROOT"]."/".Explode("/",$_SERVER["REQUEST_URI"])[1].'/lib/encrypt/uws_encrypt.php';
require_once $_SERVER["DOCUMENT_ROOT"]."/".Explode("/",$_SERVER["REQUEST_URI"])[1].'/lib/uws_canvas.php';

session_name("UWS");
session_set_cookie_params(
    ['secure'=>true,
        'samesite'=> 'None'
    ]
);

session_start();

//Check Post to make sure it contains a file
if (!isset($_FILES["uploadedFile"]))
{
    echo ('Missing File!');
    exit();
}

if (!isset($_FILES["uploadedFile"]["type"]))
{
    echo ('File type unknown!');
    exit();

}

if ($_FILES["uploadedFile"]["type"] != 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
{
    echo ('File type must be a spreadsheet!');
    exit();

}

$lastRun=null;
if (isset($_POST["LastRun"]))
{
    $lastRun = strtotime($_POST["LastRun"]);
}
else {echo ("Missing Date!"); exit();}

// Open Uploaded Sheet
// Get Colunn ID's

$xlsx =  new SimpleXLSX( $_FILES['uploadedFile']['tmp_name'] );

$rows = $xlsx ->rows();
$headerRow = 0;


$colIDName = array_search("Learner Name  ",$rows[$headerRow]);
$colIDCourse = array_search("Course ",$rows[$headerRow]);
$colIDDone = array_search("Progress Status ",$rows[$headerRow]);
$colLMSID =  array_search("SSO Id ",$rows[$headerRow]);
$colCompletedAT = array_search ("Completed At ",$rows[$headerRow]);



if ($colIDCourse == false)
{
    echo ('Missing Course Column!');
    exit();

}

if ($colIDDone== false)
{
    echo ('Missing Done Column!');
    exit();

}

if ($colLMSID == false)
{
    echo ('Missing LMS Learning ID Column!');
    exit();

}


// Open Mapping Sheet
$xlsxConfig = new SimpleXLSX("config.xlsx");
$configHeader = 2;

$configRows = $xlsxConfig -> rows();

$colEverFi= array_search("Everfi",$configRows[$configHeader]);
$colCanvasID =array_search("Canvas ID",$configRows[$configHeader]);
$colAssignmentID = array_search("AssignmentID",$configRows[$configHeader]);
$colPassingID =  array_search("Passing",$configRows[$configHeader]);
$colActive =  array_search("Active",$configRows[$configHeader]);





$lms_url = $_SESSION['OAuthDomain'];
$token = $lms_token[$lms_url];

$_SESSION['token'] = uws_encrypt::encrypt_decrypt('decrypt',$token, $mykey);

$canvas = new uws_canvas(uws_encrypt::encrypt_decrypt('decrypt',$token, $mykey), $lms_url);

// Get Assignment Points from Config
for ($k = $configHeader+ 1; $size = count($configRows),$k< $size; $k++)
{
    $points= getAssignmentPoints($canvas, $configRows[$k][$colCanvasID],$configRows[$k][$colAssignmentID]);
    if ($points === false)
    {
        echo ("Config File out of Date!");
        exit();

    }
    else
        array_push($configRows[$k] ,$points);

}


$updated = array();
$notEnrolled = array();


for ($x = $headerRow+ 1; $size = count($rows), $x <$size; $x++) {
    $courseConfig = getCourseConfig($rows[$x][$colIDCourse],$configRows, $colEverFi);

    $lmsCanvasID =$rows[$x][$colLMSID];
    $cmpStatus = $rows[$x][$colIDDone];

    $dv = $rows[$x][$colCompletedAT];

    $dateComplete = null;
    if (trim($dv) !="")
        $dateComplete = strtotime($rows[$x][$colCompletedAT]);
    else $dateComplete=0;

    if ($dateComplete > $lastRun)
    if (substr($lmsCanvasID,0,5) == "scone") {
        //$lmsCanvasID =  substr($lmsCanvasID,strrpos($lmsCanvasID,".",-1)+1);
        $lmsCanvasID = substr($lmsCanvasID, strrpos($lmsCanvasID, ".", -1) + 1);
        $enrolled = isEnrolled($canvas, $lmsCanvasID, $courseConfig[$colCanvasID]);

        echo("<b>" ."Row: ".$x." ". $rows[$x][$colIDName] . "</b> : CourseInfo" . $courseConfig[$colEverFi] . "<br>");

        if ($enrolled == false) {
            echo("-----<b><mark>User Not Enrolled</mark></b><br>");
            array_push($notEnrolled,$rows[$x]);
        } else {
            if ($cmpStatus != 'Completed') {
                echo "----- User Not Complete - Status Not Updated. <br>";

            } else {
                $newValue = false;
                $assignment = getUserAssignment($canvas, $lmsCanvasID, $courseConfig[$colCanvasID], $courseConfig[$colAssignmentID]);


                if ($courseConfig[count($courseConfig) - 1] == 0) {
                    if ($assignment->grade != "complete")
                        $newValue = true;
                } else {

                    // This test only applies to the current grade book                                                                                                                                    if ($courseConfig[count($courseConfig)-1] != $assignment -> score)
                    if ($courseConfig[$colPassingID] > $assignment->score) {
                        $newValue = true;
                    }
                }



                if ($newValue == true) {

                    $update = UpdateUserAssignment($canvas, $lmsCanvasID, $courseConfig[$colCanvasID], $courseConfig[$colAssignmentID], $courseConfig[$colPassingID]);
                    if ($update == true) {
                        echo("---- <b><mark>Completion Updated!</mark></b> <br>");
                        array_push($updated, $rows[$x]);
                    } else
                         echo ("--- <b>Error Updating Record!</b>");
                }


            }
        }
    }
}

echo "<p> <h2>The following users were updated</h2>";
foreach ($updated as $item)
{
    echo $item[$colLMSID]. " - ". $item[$colIDName]."<br>";


}
    echo "</p>";

echo "<p> <h2>The following users are not enrolled ('These users likely unenrolled after completing the course')</h2>";
foreach ($notEnrolled as $item)
{
    echo $item[$colLMSID]. " - ". $item[$colIDName]."<br>";


}
echo "</p>";

// Loop through array check canvas submission against file, if file is complete and not complete in Canvas mark complete.

// if student doesn't exist in course ignore record.


// output report with exceptions.

function getCourseConfig ($course, $config , $ColEverFi)
{
    $rtnValue = null;
    foreach ($config as $item )
    {
        if ($item[$ColEverFi] == $course)
            return $item;
    }
    return $rtnValue;

}



function isEnrolled ($canvasAPI, $lmsID, $courseID)
{

    $result  = $canvasAPI->get("/courses/".$courseID."/users/".$lmsID,null);


    if (isset ($result[0]->errors[0]->message))
        return false;
    else
        if (count($result) > 0)
            return true;
        else
            return false;

}

function getUserAssignment($canvasAPI, $lmsID, $courseID,$assignmentID)
{

    $result  = $canvasAPI->get( "/courses/".$courseID."/assignments/".$assignmentID.'/submissions/'.$lmsID,null);


    if (isset ($result[0]->errors[0]->message))
        return false;
    else
        return $result[0];

}

function getAssignmentPoints($canvasAPI, $courseID,$assignmentID)
{
    $result  = $canvasAPI->get( "/courses/".$courseID."/assignments/".$assignmentID,null);

    if (isset ($result[0]->errors[0]->message))
        return false;
    else
    {

        return $result[0]-> points_possible;
    }
}

function UpdateUserAssignment($canvasAPI, $lmsID, $courseID,$assignmentID, $passing)
{
    $result = $canvasAPI ->grade_assignment ($courseID, $assignmentID, $lmsID);
    //$postfields = array('grade_data['.$lmsID.'][posted_grade]' => 'complete');

    //$result = $canvasAPI->post("/courses/:".$courseID."/assignments/".$assignmentID."/submissions/update_grades",$postfields);


    if (isset ($result[0]->errors[0]->message))
        return false;
    else
        return true;

}
