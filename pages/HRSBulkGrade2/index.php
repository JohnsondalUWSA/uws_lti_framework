<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/".Explode("/",$_SERVER["REQUEST_URI"])[1]."/vendor/autoload.php";

session_name("UWS");
session_set_cookie_params(
    ['secure'=>true,
        'samesite'=> 'None'
    ]
);

session_start();



/*
if (!isset($_SESSION['isAdmin']))
    $_SESSION['isAdmin'] = false;

if ($_SESSION["isAdmin"]=== false)
{
    echo ("This tool requires admin access" );
    http_response_code(403);
    exit();
}
*/







?>

<html>
<head></head>
<style>
    div {
        margin-bottom: 15px;
        padding: 4px 12px;
    }


    .warning {
        background-color: #ffffcc;
        border-left: 6px solid #ffeb3b;
    }


     #overlay {

         color: #666666;
         position: fixed;
         height: 100%;
         width: 100%;
         z-index: 5000;
         top: 0;
         left: 0;
         float: left;
         text-align: center;
         padding-top: 25%;
     }

</style>

<script
        src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
        crossorigin="anonymous"></script>

<script
        src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
        integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
        crossorigin="anonymous"></script>

<script>

    $(document).ready(function(){
        $("form").submit(function(){
            document.getElementById("overlay").style.display = "block";
        });
    });




</script>



<body>
<h3>HRS Foundry Sync Tool</h3>
<form  method="POST" action="processFile.php" enctype="multipart/form-data">

    <div>
        <?php $date =
        $newDate = date("Y-m-d"); ?>


        <label for="LastRun">Starting Completion Date</label>
        <input type="date" id="LastRun" name="LastRun" value ="<?php echo $newDate;?>">
        <br>
        <span>Upload Score File:</span>

        <input type="file" name="uploadedFile" />
    </div>

    <input type="submit" name="uploadBtn" value="Upload" />
</form>

<div id="overlay" style ="display:none">

    <img src="<?php echo("https://" . $_SERVER["SERVER_NAME"] . "/" . explode("/", $_SERVER["REQUEST_URI"])[1] . "/images/catSpinner.gif"); ?>"
         alt="Loading"/></div>
<br/>
</div>

</body>
</html>
