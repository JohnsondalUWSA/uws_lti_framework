<?php

// This is a Example of how to get the developer key token for a given user
// Init Session

require_once $_SERVER["DOCUMENT_ROOT"]."/".Explode("/",$_SERVER["REQUEST_URI"])[1]."/vendor/autoload.php";

session_name("UWS");
session_set_cookie_params(
    ['secure'=>true,
        'samesite'=> 'None'
    ]
);
session_start();



//  OAuthRedirectTo -  Need to tell OAuthRedirect where to send the user too after the OAuth Process Completes
$_SESSION["OAuthRedirectTo"] = "SampleAppOAuthAPI/index.php";

// Commit Value
session_commit();
// Redirect for token generation.
require("API/V1/OAuth.php");

?>

