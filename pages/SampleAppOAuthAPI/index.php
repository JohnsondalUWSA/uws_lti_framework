<?php


require_once $_SERVER["DOCUMENT_ROOT"]."/".Explode("/",$_SERVER["REQUEST_URI"])[1]."/vendor/autoload.php";

session_name("UWS");
session_set_cookie_params(
    ['secure'=>true,
        'samesite'=> 'None'
    ]
);

session_start();




// OAuth Handler for Token Management //
require_once $_SERVER["DOCUMENT_ROOT"]."/".Explode("/",$_SERVER["REQUEST_URI"])[1]."/lib/OAuthHandler.php";
require_once $_SERVER["DOCUMENT_ROOT"]."/".Explode("/",$_SERVER["REQUEST_URI"])[1]."/lib/uws_canvas.php";

// Return Unauthorized for users not having a session
if (!isset ($_SESSION['OAuthKey'])) {
    echo('No Session!');
    http_response_code(401);
    exit();
}

// Get API Domain
$OAuthDomain = $_SESSION['OAuthDomain'];
$OAuthKey = $_SESSION['OAuthKey'];

$redirect = "Location: https://" . $OAuthDomain . "/login/oauth2/auth?client_id=" . $_SESSION["OAuthClientID"] . "&response_type=code&state=" . $OAuthKey .
    "&redirect_uri=https://" . $_SERVER['SERVER_NAME'] . str_replace("index.php", "API/V1/OAuthRedirect.php", $_SERVER["REQUEST_URI"]);
$_SESSION["OAuthRedirect"] = $redirect;

// Init Oauth Handler
$OAuthInfo = new OAuthHandler($OAuthDomain, $_SESSION["OAuthClientID"], $_SESSION["OAuthClientKey"], $redirect);

//Does user have established Token?
$HasToken = $OAuthInfo->GetSavedToken($_SESSION['launch_params']['custom_canvas_user_id'], $_SESSION["OAuthClientID"]);
$TokenOK = false;

// Return Unauthorized for users not having a session
if (!$HasToken) { echo "No Token!"; http_response_code(401); exit();}

// Check to see if Token is good.  Note: CheckToken will attempt to refresh the token
$TokenOK = $OAuthInfo->CheckToken();

if (!$TokenOK) { echo("Bad Token!!!"); http_response_code(401); exit();}

$canvas = new uws_canvas($OAuthInfo->access_token, $OAuthDomain);
$rep = $canvas ->get("/users/self",null);
$rtnValue = json_encode(($rep));
echo($rtnValue);


