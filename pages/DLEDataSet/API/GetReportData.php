<?php


require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/vendor/autoload.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . '/settings.php';
require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/lib/encrypt/key.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/lib/encrypt/uws_encrypt.php";

require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/pages/DLEDataSet/biz/dleAPIData.php";


require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/lib/uws_Token/uws_token_util.php";

require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/lib/jsonToCsv/jsonToCsv.php";

use \JsonMachine\JsonDecoder\ExtJsonDecoder;
use \JsonMachine\JsonMachine;

$token = null;
$cmd = null;
$db=null;
$params = array();

// Convert $_GET;
foreach ($_GET as $key => $value) {

    switch ($key) {
        case "token":
            $token = $value;
            break;
        case "cmd":
            $cmd = $value;
            break;
        case "db":
            $db = $value;
            break;

        default:
        {
            if(startsWith($key,"p") !== false)
            {
                $keyName = substr($key,1);
                $params[$keyName] = $value;
                //array_merge($params, array($keyName => $value));
            }
       }
    }
}







// Rest Server Info (GATEWAY TO SQL)
$apiToken = uws_encrypt::encrypt_decrypt('decrypt', $restGatewayToken, $mykey);



$tokenInfo = uws_token_util::getToken($token,0,$restGateway,$apiToken,$db);
if (isset($tokenInfo["errMSG"]))
{
    echo ($tokenInfo["errMSG"]);
    exit();


}

$params["CanvasAccountID"]= $tokenInfo["accountID"];

$outputType = "";
if (isset($_GET["outType"]) === true)
    $outputType = $_GET["outType"];
else
    $outputType = "json";


$params["StoreProcedureOutputType"] = $outputType;

// Start API Client
$apiClient = new dleAPIData($restGateway, $apiToken, $tokenInfo["databaseName"], $tokenInfo["accountID"],$cmd, $params);


header("Access-Control-Allow-Origin: *");
header("Cache-Control: no-cache, must-revalidate");

switch ($outputType) {
    case "table":
        $rtnData = $apiClient->PullData();

        header('Content-type: application/text');
       // $jData = JsonMachine::fromString($rtnData);
        //echo jsonToTable(json_decode($rtnData,true));
       echo $rtnData;

       //jsonToTable($jData);
        break;
    case "csv":
        $rtnData = $apiClient->DownloadData("Report.csv");
        break;
    case "xls":

         $apiClient->DownloadData("Report.xlsx");
        break;
    default:
        header('Content-disposition: attachment; filename=report.json');
        header('Content-type: application/json');
        $rtnData = $apiClient->PullData();

ob_clean();;
        echo $rtnData;

}



function startsWith( $haystack, $needle ) {
    $length = strlen( $needle );
    return substr( $haystack, 0, $length ) === $needle;
}

function jsonToTable ($data)
{
    $table = '<table id="tbReport" class="table table-striped table-bordered dt-responsive" width="90%">';

    $table = $table."<thead><tr>";
  /*  if (!isset($data[0]))
    {
        $table = $table."<th>Message</th>";
        $table = $table."</th></thead><tbody>";
        $table = $table."<tr><td>NoData</td></tr>";
        $table = $table."</body></table>";
        return $table;


    }

    $keys = array_keys($data[0]);
    foreach ($keys as $key)
    {
        $table = $table."<th>".$key."</th>";


    }
  */
    $table = $table."</th></thead><tbody>";

    foreach ($data as $row) {
        $table .= '
        <tr valign="top">';
        foreach ($row as $key => $value)
        {
            $table = $table."<td>".$value."</td>";

        }

        $table=$table."</tr>";

    }
    $table .= '
    </body></table>
    ';
    return $table;
}