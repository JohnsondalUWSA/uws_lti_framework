<?php


require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/vendor/autoload.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . '/settings.php';
require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/lib/encrypt/key.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/lib/encrypt/uws_encrypt.php";

require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/pages/DLEDataSet/biz/dleAPIData.php";


require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/lib/uws_Token/uws_token_util.php";

require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/lib/jsonToCsv/jsonToCsv.php";

$cmd = null;
$db = null;

// Convert $_GET;
foreach ($_GET as $key => $value) {

    switch ($key) {

        case "cmd":
            $cmd = $value;
            break;

        default:
        {
            }
        }

}



session_name("UWS");
session_set_cookie_params(
    ['secure'=>true,
        'samesite'=> 'None'
    ]
);

session_start();


if ( !isset($_SESSION["post"]["custom_uwsa_tool_name"]))
{
    echo ("This tool requires the use of cookies. Please enable cookies." );
    http_response_code(403);
    exit();
}
else
{
    if ($_SESSION["post"]["custom_uwsa_tool_name"] != "DLEDataSet")
    {
        echo ("Bad tool lauch!" );
        http_response_code(403);
        exit();

    }
}

$isAdmin = false;

if (isset($_SESSION["post"]["roles"]))
    if (strpos($_SESSION["post"]["roles"], "Administrator") !== false)
        $isAdmin = true;

if ($isAdmin == false)
{
    echo ("This tool requires account administrator level access." );
    http_response_code(403);
    exit();
}

$accountId = "1";

if (!isset ($_SESSION["post"]["custom_uwsa_account_id"]))
{
    echo ("Tool configuration information is missing!");
    http_response_code((403));
    exit();
}else $accountId = $_SESSION["post"]["custom_uwsa_account_id"];


$apiToken = uws_encrypt::encrypt_decrypt('decrypt',$restGatewayToken,$mykey);


$dataBase = $lmsReporting[$_SESSION['OAuthDomain']];
$params = array();

$params["procedureName"] = $cmd;


// Rest Server Info (GATEWAY TO SQL)
$apiToken = uws_encrypt::encrypt_decrypt('decrypt', $restGatewayToken, $mykey);


// Start API Client
$apiClient = new dleAPIData($restGateway, $apiToken,$dataBase, $accountId, "aspReportingGetStoreProcedureParms", $params);

$rtnData = $apiClient->PullData();

//$rtnDataCSV =


header("Access-Control-Allow-Origin: *");
header("Cache-Control: no-cache, must-revalidate");
header('Content-type: application/json');

echo $rtnData;