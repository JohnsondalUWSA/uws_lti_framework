<?php
require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/vendor/autoload.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . '/settings.php';
require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/lib/encrypt/key.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/lib/encrypt/uws_encrypt.php";

require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/lib/uws_Token/uws_token_util.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/pages/DLEDataSet/biz/dleAPIData.php";

require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/lib/uws_canvas.php";

require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/lib/uws_Token/uws_token_util.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/lib/jsonToCsv/jsonToCsv.php";


session_name("UWS");
session_set_cookie_params(
    ['secure'=>true,
        'samesite'=> 'None'
    ]
);

session_start();

// Get LMS Domain and Token
$lms_url = $_SESSION['OAuthDomain'];
$token = $lms_token[$lms_url];

$canvas = new uws_canvas(uws_encrypt::encrypt_decrypt('decrypt',$token, $mykey), $lms_url);

$isAdmin = $canvas -> isAdminRole($_SESSION["post"]["custom_uwsa_account_id"],$_SESSION["post"]["custom_uwsa_roles"]);

if ($isAdmin === false)
{
    echo ("This tool requires administrator level access" );
    http_response_code(403);
    exit();

}


$dataBase = $lmsReporting[$_SESSION['OAuthDomain']];
$params = array();

$params["accountID"] = $_SESSION["post"]["custom_uwsa_account_id"] ;

// Rest Server Info (GATEWAY TO SQL)
$apiToken = uws_encrypt::encrypt_decrypt('decrypt', $restGatewayToken, $mykey);
// Start API Client
$apiClient = new dleAPIData($restGateway, $apiToken,$dataBase, "1", "aspReportListing", $params);

$token = uws_token_util::generateToken($_SESSION["post"]["custom_canvas_user_login_id"],$_SESSION["post"]["custom_uwsa_account_id"],$restGateway,$apiToken,$dataBase);


$reportList =json_decode ( $apiClient->PullData());



?>
<html>
<head>


    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.serializeJSON/3.1.1/jquery.serializejson.min.js"
            integrity="sha512-czywXrb/msTMh+lhgSe/vQ0GT0OraNiD8Knd7A7fMqEjDmQxljn/b39skl45eu+iyG0wxC9SuqcaUatZ4S0kdA=="
            crossorigin="anonymous"></script>

    

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW"
            crossorigin="anonymous"></script>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/css/select2.min.css" rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/js/select2.min.js"></script>

    <link rel="stylesheet" type="text/css"
          href="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.23/b-1.6.5/b-colvis-1.6.5/b-html5-1.6.5/cr-1.5.3/fc-3.3.2/fh-3.1.7/r-2.2.7/sb-1.0.1/sp-1.2.2/sl-1.3.1/datatables.min.css"/>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>

    <!--<script type="text/javascript"
            src="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.23/b-1.6.5/b-colvis-1.6.5/b-html5-1.6.5/cr-1.5.3/fc-3.3.2/fh-3.1.7/r-2.2.7/sb-1.0.1/sp-1.2.2/sl-1.3.1/datatables.min.js"></script>
-->
    <script src="<?php echo "https://" . $_SERVER["SERVER_NAME"] . "/" . explode("/", $_SERVER["PHP_SELF"])[1]; ?>/scripts/clipboard.js/clipboard.js"></script>
    <script src="<?php echo "https://" . $_SERVER["SERVER_NAME"] . "/" . explode("/", $_SERVER["PHP_SELF"])[1]; ?>/scripts/overhang.min.js"></script>
    <link href="<?php echo "https://" . $_SERVER["SERVER_NAME"] . "/" . explode("/", $_SERVER["PHP_SELF"])[1]; ?>/scripts/overhang.min.css" rel="stylesheet"/>

    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

    <script src="https://use.fontawesome.com/6b9533ac9b.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.6/clipboard.min.js"></script>

    <link href="https://cdn.datatables.net/1.10.23/css/dataTables.bootstrap.min.css"/>
    <link href="https://cdn.datatables.net/responsive/2.2.7/css/responsive.bootstrap.min.css">
    <script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.23/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.7/js/responsive.bootstrap.min.js"></script>


    <style>
        #overlay {

            color: #666666;
            position: fixed;
            height: 100%;
            width: 100%;
            z-index: 5000;
            top: 0;
            left: 0;
            float: left;
            text-align: center;
            padding-top: 25%;
        }


        table.table {
            margin-left: 30px;
        }

        .clippy {
            margin-top: -3px;
            position: relative;
            top: 3px;
            height: 16px;
        }
    </style>
    <script>

        $(document).ready(function () {
            $('form').on('submit', function (e) {

                e.preventDefault();


            });

            $("#lstReports").select2();

            var clipboard = new ClipboardJS('.btnClip');

            clipboard.on('success', function(e) {
                console.info('Action:', e.action);
                console.info('Text:', e.text);
                console.info('Trigger:', e.trigger);

                $("body").overhang({type:"success",message: "Copied!"});
                e.clearSelection();
            });

            clipboard.on('error', function(e) {
                console.error('Action:', e.action);
                console.error('Trigger:', e.trigger);
            });

            $('#lstReports').on('select2:select', function (e) {
                $("#cmd").val( $("#lstReports").val());
                verifyProcedure();
                $("#rptData").html("");
            });

        });

        var counter = 0;
        var limit = 5;

        function addInput(divName, parmName) {
            if (counter == limit) {
                alert("You have reached the limit of adding " + counter + " inputs");
            } else {
                var newdiv = document.createElement('div');

                newdiv.innerHTML = "        <div class='row'>\n" +
                    "  <div class='col-sm-4'  >    <label for=\"parm" + (counter + 1) + "name\">Parmameter Name</label>\n" +
                    "        <input class = 'form-control' type=\"text\" id=\"parm" + (counter + 1) + "name\" name=\"parm" + (counter + 1) + "name\" value=\"" + parmName.substring(1) + "\" readonly></div>\n" +
                    "\n" +

                    "      <div class='col-sm-4' > <label for=\"parm" + (counter + 1) + "value\">Value</label> <input class = 'form-control' type=\"text\" id=\"parm" + (counter + 1) + "value\" name=\"parm" + (counter + 1) + "value\">\n" +
                    "        </div></div>\n";

                document.getElementById(divName).appendChild(newdiv);
                counter++;

            }
        }

        function runReport() {
            var link = getUrl("table")
            if (link == false)
                return;
            ShowBusy(true);
            $.get(link, function (data, status) {

                $("#rptData").html(data);
                $("#tbReport").DataTable({"pageLength":100});
                ShowBusy(false);
                //alert("Data: " + data + "\nStatus: " + status);
            });

        }

        function getUrl(method) {

            var reportURL = "<?php echo "https://" . $_SERVER["SERVER_NAME"] . str_replace("index.php", "", $_SERVER["REQUEST_URI"]); ?>API/GetReportData.php?token=<?php echo $token ?>";


            var cmd = $("#cmd").val();
            if (cmd == "")
            {
                $("body").overhang({
                    type: "error",
                    message: "Please Select a Dataset!",
                    closeConfirm: true
                });
                return false;
            }

            reportURL = reportURL + "&cmd=" + cmd + "&db=<?php echo $dataBase ?>";

            var i;

            if (counter > 0)
                for (i = 0; i < counter; i++) {
                    var parmName = "";
                    var parmValue = "";

                    parmName = $("#parm" + (i + 1).toString() + "name").val();
                    parmValue = $("#parm" + (i + 1).toString() + "value").val();

                    if (parmValue == "")
                    {
                        $("body").overhang({
                            type: "error",
                            message: "Not all parameters contain values, please review them.",
                            closeConfirm: true
                        });
                        return false;

                    }

                    reportURL = reportURL + "&p" + parmName + "=" + parmValue;
                }

            reportURL = reportURL + "&outType=" + method;
            return encodeURI(reportURL);

        }

        function ShowBusy(running) {
            if (running)
                document.getElementById("overlay").style.display = "block";
            else
                document.getElementById("overlay").style.display = "none";

        }

        function getLink(linkType) {
            var reportURL = getUrl(linkType);
            if (reportURL == false)
                return;
            $("#txtReportUrl").val(reportURL);

            $('#TinyUrlModal').modal('show');


        }

        function DownloadUrl() {
            var downloadURL = $("#txtReportUrl").val();
            window.open(downloadURL, "_blank");
        }

        function verifyProcedure()
        {
            var cmd = $("#cmd").val();

            var cmdURL = "<?php echo "https://" . $_SERVER["SERVER_NAME"] . "/" . explode("/", $_SERVER["PHP_SELF"])[1]; ?>/pages/DLEDataSet/API/GetReportParms.php?cmd=" + cmd;


            $("#Parms").empty();
            counter = 0;

            $.getJSON( cmdURL, function( data ) {
               for (item in data)
               {
                   if (data[item].PARAMETER_NAME != "@CanvasAccountID")
                     addInput("Parms",data[item].PARAMETER_NAME);

               }
            });


        }



    </script>


</head>
<body>
<div class="container-fluid">
    <h3>DLE Datasets</h3>
    <p> This tool will allow you to run and export data from the DLE server. Choose a dataset and populate parameters as needed.</p>
    <form>
        <div class="row">
            <div class="form-control">

            <label for="lstReports">Select Dataset From List
            <select id="lstReports" style="width:350px !important;" >
                <option data-id=""></option>
                <?php
                    foreach ($reportList as $item)
                    {
                        echo "<option value=\"".$item->cmd."\">".$item->name."</option>";


                    }

                ?>

            </select></label>

            </div>
            <div class="form-control" hidden>
                <div class="col-mb-4"><label for="cmd">Store Procedure</label></div>
<div class="input-group col-mb-4">
                <div class="col-mb-4"><input class="form-control" type="text" id="cmd" name="cmd" value=""></div>
                <div class="input-group-append">
                    <button class="btn btn-outline-dark" onclick="verifyProcedure()" >Verify</button>
                </div>

</div>
            </div>
        </div>
    <!--    <div class="row">
            <div class="col-sm-4">
                <button class="btn btn-outline-dark" onclick="addInput('Parms');">Add Parmameters</button>
            </div>
        </div> -->
        <div class="row">
            <div id="Parms" name="Parms"></div>
        </div>


    </form>

    <div class="row">

        <div class="col-sm-8">
            <button class = "btn btn-outline-dark" onclick="runReport();">Preview <i class="fa fa-table" aria-hidden="true"></i></button>
            <button class = "btn btn-outline-dark"  onclick="getLink('json');">JSON <i class="fa fa-link" aria-hidden="true"></i></button>
            <button class = "btn btn-outline-dark" onclick="getLink('csv');">CSV <i class="fa fa-link" aria-hidden="true"></i></button>
            <button class = "btn btn-outline-dark" onclick="getLink('xls');">XLSX <i class="fa fa-link" aria-hidden="true"></i></button>


        </div>
    </div>
</div>

<div class="row" id="rptData">


</div>

<div id="overlay" style="display:none">

 <img src="<?php echo("https://" . $_SERVER["SERVER_NAME"] . "/" . explode("/", $_SERVER["REQUEST_URI"])[1] . "/images/catSpinner.gif"); ?>"
         alt="Loading"/>
</div>
<br/>
</div>


<div class="modal hide fade" id="TinyUrlModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="rtpTitle">Export Link/Download</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="$('#TinyUrlModal').modal('hide');">
                    <i class="fa fa-window-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="input-group sm-3">
                    <input id="txtReportUrl" type="text" class="form-control" aria-label="Copy URL"
                           aria-describedby="basic-addon2" value="Text Copy " readonly>
                    <div class="input-group-append">
                        <button class="btn btn-outline-secondary btnClip" type="button" data-clipboard-target="#txtReportUrl">
                        <i class="fa fa-clipboard"></i>
                        </button>

                        <button class="btn btn-outline-secondary" type="button" ><i class="fa fa-download" onclick="DownloadUrl()"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


</body>


</html>
