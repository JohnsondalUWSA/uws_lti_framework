<?php


class dleAPIData
{
    private $apiToken ;
    private $lmsDomain;
    private $dataBase ;
    private $_params ;
    private $_accountID;
    private $_storeProcName;
    private $_restGateway;

    public function setStoreProcName($storeProcName): void
    {
        $this -> _storeProcName = $storeProcName;

    }

    public function setParms ($params ):void
    {
        $this-> _params = $params;

    }
    public function __construct($gateway, $token , $dataBase, $accountID, $storeProcName, $params)
    {
        $this ->_accountID = $accountID;
        $this ->_storeProcName = $storeProcName;
        $this -> _params = $params;

        $this -> apiToken = $token;
        $this -> dataBase = $dataBase;
        $this -> _restGateway = $gateway;


    }


    public function PullData ()
    {

        $this->_params["StoreProcedureName"] =$this ->_storeProcName;
        $this->_params["StoreProcedureDBConfig"] = $this ->dataBase;
        //$this->_params["noParm"] = "";

// Get Current Settings
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL =>  $this->_restGateway,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $this->_params,
            CURLOPT_HTTPHEADER => array(
                "Authorization: ".$this ->apiToken
            ),
            CURLOPT_SSL_VERIFYHOST=> false,
            CURLOPT_SSL_VERIFYPEER=> false

        ));

        $response = curl_exec($curl);

        if ($response === false)
        {
            $curError = curl_error($curl);
            return "Error communicating with Rest API Server: ". $curError."</br>";

        }

       // $data = json_decode($response,true);
        curl_close($curl);

        return $response;


    }

    public function DownloadData ($fileName)
    {
        set_time_limit(0);
        # filePointer
//        $fileHandle = tmpfile();
        #Open the file for writing
        //$fileHandle = fopen("php://temp","w+");


        $this->_params["StoreProcedureName"] =$this ->_storeProcName;
        $this->_params["StoreProcedureDBConfig"] = $this ->dataBase;
        //$this->_params["noParm"] = "";

// Get Current Settings
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL =>  $this->_restGateway,
  //          CURLOPT_FILE=>$fileHandle,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $this->_params,
            CURLOPT_HTTPHEADER => array(
                "Authorization: ".$this ->apiToken
            ),
            CURLOPT_SSL_VERIFYHOST=> false,
            CURLOPT_SSL_VERIFYPEER=> false

        ));


        $response = curl_exec($curl);

        if($response === false)
        {
            $errMessage =  curl_error($curl);
            $response= $errMessage;
        }

        curl_close($curl);

        ob_clean();
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename='.$fileName);
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');

        echo $response;
        return true;


    }



}

?>