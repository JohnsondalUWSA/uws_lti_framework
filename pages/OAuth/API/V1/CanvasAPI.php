<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/".Explode("/",$_SERVER["REQUEST_URI"])[1]."/vendor/autoload.php";

$mySession = new fkooman\SeCookie\Session(
    fkooman\SeCookie\SessionOptions::init()->withName('UWSSID'),
    fkooman\SeCookie\CookieOptions::init()->withSameSiteNone()
);
$mySession->start();

$encoded_session = $mySession -> get("session");
$SESSION = array();

if (isset($encoded_session))
    $SESSION = unserialize(($encoded_session));

// OAuth Handler for Token Management /
require_once dirname(__FILE__) . "/../../../../lib/OAuthHandler.php";
require_once dirname(__FILE__) . "/../../../../lib/canvas-php-curl/class.curl.php";

// Return Forbidden if not a post
if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
    echo('Must be called via POST');
    http_response_code(403);
    exit();
}

// Return bad request if no query
if (!isset ($_POST['query']))
{
    echo ("No Query!");
    http_response_code(400);
    exit();

}

$query = $_POST['query'];

// Return Unauthorized for users not having a session
if (!isset ($SESSION['OAuthKey']))
{
    echo ('No Session!');
    http_response_code(401);
    exit();
}

// Get API Domain
$OAuthDomain =  $SESSION['OAuthDomain'];
$OAuthKey = $SESSION['OAuthKey'];

//$_SESSION["OAuthClientID"] = $client_id;
//$_SESSION["OAuthClientKey"] = $client_key;


$redirect = "Location: https://".$OAuthDomain."/login/oauth2/auth?client_id=".$SESSION["OAuthClientID"]."&response_type=code&state=".$OAuthKey."&redirect_uri=https://".$_SERVER['SERVER_NAME'].str_replace("index.php","API/V1/OAuthRedirect.php",$_SERVER["REQUEST_URI"]);
$SESSION["OAuthRedirect"] = $redirect;

// Validate has Token

// Init Oauth Handler
$OAuthInfo = new OAuthHandler($OAuthDomain, $SESSION["OAuthClientID"],$SESSION["OAuthClientKey"], $redirect);

//Does user have established Token?
$HasToken = $OAuthInfo -> GetSavedToken( $SESSION['launch_params']['custom_canvas_user_id'] );
$TokenOK = false;

// Return Unauthorized for users not having a session
if (!$HasToken )
{
    echo "No Token!";
    http_response_code(401);
    exit();
}

// Check to see if Token is good.  Note: CheckToken will attempt to refresh the token
$TokenOK = $OAuthInfo -> CheckToken();

if (!$TokenOK)
{
    echo ("Bad Token!!!");
    http_response_code(401);
    exit();

}

// Init Canvas Curl Class

$token = "Authorization: Bearer ".$OAuthInfo->access_token;
$cURL = new Curl($token, $OAuthDomain);

$repsonse = $cURL->get($query);

$cURL->closeCurl();

$rtnValue = json_encode(($repsonse));
echo ($rtnValue);


?>