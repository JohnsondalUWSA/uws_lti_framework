<?php

$base = $_SERVER["PHP_SELF"];
$pos =strpos( $_SERVER["PHP_SELF"], "index.php");
if ($pos !== false)
    $base = rtrim($_SERVER["PHP_SELF"],"/index.php");
else
    $base = rtrim($_SERVER["PHP_SELF"],"/API/V1/OAuthRedirect.php");

?>

<!DOCTYPE html>

<html>

<head>
    <link rel="stylesheet"  href="scripts/common.css">
    <script
            src="https://code.jquery.com/jquery-3.4.1.min.js"
            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
            crossorigin="anonymous"></script>

    <script
            src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
            integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
            crossorigin="anonymous"></script>
    <link href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet" />
    <link src="scripts/common.css"/>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>



    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" integrity="sha384-xrRywqdh3PHs8keKZN+8zzc5TX0GRTLCcmivcbNJWm2rs5C8PRhcEn3czEjhAO9o" crossorigin="anonymous"></script>

    <link href="<?php echo ($base); ?>/scripts/jquery.json-viewer-master/json-viewer/jquery.json-viewer.css" rel="stylesheet" />
    <script src="<?php echo ($base); ?>/scripts/jquery.json-viewer-master/json-viewer/jquery.json-viewer.js"></script>

<script>
    var baseURL = "<?php
            echo ($base);
        ?>";

    var data = {
        "foobar": "foobaz"
    };


    $(document).ready(function(){
        $('#json-renderer').jsonViewer(data);

        $("#form_i button").click(function (ev) {
            ev.preventDefault()
            if ($(this).attr("value") == "btnExecute") {
                var request_method = $("#form_i").attr("method"); //get form GET/POST method
                var form_data = $("#form_i").serialize(); //Encode form elements for submission
                var post_url = baseURL + $("#form_i").attr("action"); //get form action url

                $.ajax({
                    url: post_url,
                    crossDomain:true,
                    type: request_method,
                    data: form_data

                }).done(function(response) {

                        $('#json-renderer').jsonViewer(JSON.parse(response));

                    }


                    );



            }
            if ($(this).attr("value") == "btnSave") {
                alert("Second button is pressed - Form did not submit")
            }
        });
    });

</script>


</head>

<body>
<div class="row">
    <div class="col-md-10">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#">API Query Tool</a>
            <form id="form_i" class="form-inline" style="width:75%!important;" action="/pages/OAuth/API/V1/CanvasAPI.php" method="post"  >
                <input id="query" name="query" class="form-control" type="text" value="/users/self" aria-label="API" style="width:55%!important; ">
                <button id="btnExecute" value ="btnExecute"class="btn btn-outline-success" type="submit">Execute</button>
                <button id="btnSave"  value = "btnSave" class="btn btn-outline-success" type="submit">Save Query</button>

            </form>
        </nav>

    </div>
</div>
<div class="row">
    <div class="col-xs-6 col-md-2">
        <h6><bold>Queries</bold></h6>
        <div class="overflow-auto" style="height: 70%">
        <ul class="list-group" >
            <li class="list-group-item">First item</li>
            <li class="list-group-item">Second item</li>
            <li class="list-group-item">Third item</li>
            <li class="list-group-item">First item</li>
            <li class="list-group-item">Second item</li>
            <li class="list-group-item">Third item</li>
            <li class="list-group-item">First item</li>
            <li class="list-group-item">Second item</li>
            <li class="list-group-item">Third item</li>
            <li class="list-group-item">First item</li>
            <li class="list-group-item">Second item</li>
            <li class="list-group-item">Third item</li>
            <li class="list-group-item">First item</li>
            <li class="list-group-item">Second item</li>
            <li class="list-group-item">Third item</li>
        </ul>
        </div>
    </div>
    <div class="col-md-10">
        <pre id="json-renderer">Output</pre>

    </div>
</div>


</body>
