<?php


    // Init
    require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/vendor/autoload.php";
    require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/pages/MasterCourse/local-settings.php";
    require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/pages/MasterCourse/toolkit.php";
    require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/pages/MasterCourse/masterApp.php";

    require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/settings.php";
    require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/lib/encrypt/key.php";
    require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/lib/encrypt/uws_encrypt.php";
    require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/lib/uws_canvas.php";
    require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/lib/MeekroDB/meekrodb.2.3.class.php";



    session_name("UWS");
    session_set_cookie_params(['secure'=>true, 'samesite'=> 'None']);

    session_start();


    // Set MySQL Info
    DB::$host = $mysql_server;
    DB::$user = $mysql_user;
    DB::$password = uws_encrypt::encrypt_decrypt('decrypt', $mysql_password,$mykey);
    DB::$dbName = $mysql_db;

    $lms_url = $_SESSION['OAuthDomain'];
    $token = $lms_token[$lms_url];

    if (!isset($lms_url )) {
        echo "Your browser is blocking session information.  Please verify that you are using a <a href='https://community.canvaslms.com/docs/DOC-10720-what-are-the-browser-and-computer-requirements-for-canvas'>supported browser</a> and cross-site tracking is enabled. ";
        die();
    }


    $canvas = new uws_canvas(uws_encrypt::encrypt_decrypt('decrypt', $token,$mykey), $lms_url);

//	ini_set('display_errors',1);
//	error_reporting(E_ALL);

header('Access-Control-Allow-Origin: *');


	$app = new masterApp($id);

	$app->init();



$app->display_header("Sandbox Course Creation Tool");

	echo '<div class="container" style="margin-left: 0px; padding-left: 0px;">';

	// Grab Variables
	$domain = $_SESSION["post"]["custom_canvas_api_domain"];
	$userid = $_SESSION["post"]["custom_canvas_user_id"];
	$lastname = $_SESSION["post"]["lis_person_name_family"];
	$firstname = $_SESSION["post"]["lis_person_name_given"];
	$ext_roles = explode(',',$_SESSION["post"]["ext_roles"]);

	// Temp Override for Debugging
   // 	$domain = 'uwwtw.instructure.com';

	//Security Check - Verify LTI Roles
	$authorized = false;
	if (empty($ext_roles)) {
		die('Must be called via LTI');
	} else {
		foreach ($ext_roles as $role) {
			if (in_array($role,$authorized_roles)) {
				$authorized = true;
				continue;
			}
		}
	}
	if ($authorized === false) {
		die('User role not authorized.');
	}

	
	// POST Check
	if (isset($_POST['createcourse'])) {
		$course_name = filter_input(INPUT_POST,'name',FILTER_SANITIZE_STRING,FILTER_FLAG_NO_ENCODE_QUOTES);
		$course_code = filter_input(INPUT_POST,'code',FILTER_SANITIZE_STRING,FILTER_FLAG_NO_ENCODE_QUOTES);
		$course_suborg = filter_input(INPUT_POST,'suborg',FILTER_SANITIZE_NUMBER_INT);
		$canvas_user_id = filter_input(INPUT_POST,'custom_canvas_user_id',FILTER_SANITIZE_NUMBER_INT);

		//Validate Data
		if (empty($course_name) || empty($course_code) || empty($course_suborg) || empty($canvas_user_id)) {
			?>
			<div class="alert alert-danger" role="alert">
				<p><strong>Course Creation Error - MISSING DATA!</strong> We ran into an error while attempting to create your course.  Please contact support for additional assistance.</p>
				<p>Error Details:</p>
				<ul>
					<li>Course Name: <em><?php echo $course_name; ?></em></li>
					<li>Course Code: <em><?php echo $course_code; ?></em></li>
					<li>Department ID: <em><?php echo $course_suborg; ?></em></li>
					<li>User ID: <em><?php echo $canvas_user_id; ?></em></li>
					<li>Domain: <em><?php echo $domain; ?></em></li>
				</ul>
			</div>
			<?php							
		} else {
			// Create Course
			$create_course = $canvas->create_course($course_suborg,  $course_name, $course_code);
			if (isset($create_course['error'])) {
				?>
				<div class="alert alert-danger" role="alert">
					<p><strong>Course Creation Error!</strong> We ran into an error while attempting to create your course.  Please contact support for additional assistance.</p>
					<p>Error Details:</p>
					<ul>
						<li>Course Name: <em><?php echo $course_name; ?></em></li>
						<li>Course Code: <em><?php echo $course_code; ?></em></li>
						<li>Department ID: <em><?php echo $course_suborg; ?></em></li>
						<li>User ID: <em><?php echo $canvas_user_id; ?></em></li>
						<li>Domain: <em><?php echo $domain; ?></em></li>
					</ul>
				</div>
				<?php				
			} else {			
				$enroll_user = $canvas->create_course_enrollment($create_course['id'], $canvas_user_id);
				if (isset($enroll_user['error'])) {
					?>
					<div class="alert alert-danger" role="alert">
						<p><strong>User Enrollment Error!</strong> We were able to create your course, but ran into an error when enrolling your user.  Please contact support for additional assistance.</p>
						<p>Error Details:</p>
						<ul>
							<li>Course Name: <em><?php echo $course_name; ?></em></li>
							<li>Course Code: <em><?php echo $course_code; ?></em></li>	
							<li>Course ID: <em><?php echo $create_course['id']; ?></em></li>	
							<li>Department ID: <em><?php echo $course_suborg; ?></em></li>
							<li>User ID: <em><?php echo $canvas_user_id; ?></em></li>
							<li>Domain: <em><?php echo $domain; ?></em></li>
						</ul>
					</div>
					<?php
				} else {
					// Course Create - User Added
					?>
					<div class="alert alert-success" role="alert">
						<strong>Course Created!</strong> We have setup your new Sandbox course and it's ready!  <a href="https://<?php echo $domain; ?>/courses/<?php echo $create_course['id']; ?>" target="_blank">Let's Go!</a>
					</div>
					<?php

				}
			}
		}
	} 


	//Check for Master Course ID
	if (isset($mastercourses[$domain])) {
		$sub_accounts_from_canvas = json_decode(json_encode($canvas->get('/accounts/'.$mastercourses[$domain].'/sub_accounts?recursive=true', null)),true);


        $subaccounts = array();
        foreach($sub_accounts_from_canvas as $sub) {
            $subobj = new stdClass();
            $subobj->name = $sub['name'];
            $subobj->id = $sub['id'];
            $subobj->parent = $sub['parent_account_id'];

            if ($sub['parent_account_id'] == $mastercourses[$domain]) {
                //Top Level Accounts
                $subaccounts[$sub['id']]['obj'] = $subobj;
            } else {
                foreach ($subaccounts as $subkey=>$subvalue) {
                    //Second Level Accounts
                    if ($sub['parent_account_id'] == $subkey) {
                        $subaccounts[$subkey]['subs'][$sub['id']]['obj'] = $subobj;
                    }
                }
            }
        }
    } else {
        die("We are unable to determine your home institution.  Please log out and re-login into Canvas using your institution's login page. If problems persist contact your local support.");
    }

##############################
## Sort First & Second Level
##############################
$sort = array();
$sortchild = array();
foreach ($subaccounts as $key=>$value) {
    $sort[$key] = $value['obj']->name;
    if (isset($value['subs'])) {
        foreach ($value['subs'] as $subkey=>$subvalue) {
            $sortchild[$key][$subkey] = $subvalue['obj']->name;
        }
    }
}
//Sort by Name (First Level)
asort($sort);
$new = array();
foreach ($sort as $key=>$value) {
    $new[$key] = $subaccounts[$key];
}
$subaccounts = $new;

//Sort by Name (Second Level)
$tmp = array();
foreach ($sortchild as $key=>$sort) {
    asort($sort);
    $tmp[$key] = $sort;
}
$new = array();
$subaccountsnew = $subaccounts;
foreach ($subaccounts as $key=>$value) {
    if (isset($subaccounts[$key]['subs'])) {
        $subaccountsnew[$key]['subs'] = array();
        foreach($tmp[$key] as $key2=>$value2) {
            $subaccountsnew[$key]['subs'][$key2] = $subaccounts[$key]['subs'][$key2];
        }
    }
}
$subaccounts = $subaccountsnew;



	##############################
	## Build Form
	##############################
	?>
		<p class="lead">Use the form below to create an additional Sandbox course for content development.</p>

		<form method="post" id="courseCreateForm">
		<input type="hidden" name="custom_canvas_user_id" value="<?php echo $userid; ?>" />
		<input type="hidden" name="custom_canvas_api_domain" value="<?php echo $domain; ?>" />
		<input type="hidden" name="lis_person_name_family" value="<?php echo $lastname; ?>" />
		<input type="hidden" name="lis_person_name_given" value="<?php echo $firstname; ?>" />
		<input type="hidden" name="ext_roles" value="<?php echo implode(',',$ext_roles); ?>" />
		<div class="form-group">
			<label for="name">Course Name</label>
			<input type="text" class="form-control" name="name" id="name" aria-describedby="nameHelp" placeholder="Enter course name" />
			<small id="nameHelp" class="form-text text-muted">The course name as it would appear on the Dashboard page.</small>
		</div>
		<div class="form-group">
			<label for="name">Course Code</label>
			<input type="text" class="form-control" name="code" id="code" aria-describedby="codeHelp" placeholder="Enter course code" />
			<small id="codeHelp" class="form-text text-muted">The course code or short name to be used for the course.</small>
		</div>	
		<div class="form-group">
			<label for="suborg">Choose Department</label>
			<select class="form-control" name="suborg" id="suborg" aria=describedby="suborgHelp">
			<option value="null">Select Department</option>
			<?php
			foreach ($subaccounts as $key=>$value) {
				if (isset(((object)$value)->subs)) {
					echo '<optgroup label="'.((object)$value)-> obj->name.'">';
					foreach (((object)$value)->subs as $subkey=>$subvalue) {
						echo '<option value="'.((object)$subvalue)->obj->id.'">'.((object)$subvalue)->obj->name.'</option>';
					}
					echo '</optgroup>';
				} else {
					echo '<option value="'.((object)$value)->obj->id.'">'.((object)$value)->obj->name.'</option>';
				}
			}
			?>
			</select>
			<small id="codeHelp" class="form-text text-muted">Select the best department for the course you are creating.</small>
	
		</div>
		<button type="submit" name="createcourse" class="btn btn-primary">Create Course</button>
		</form>
	</div>

	<script type="text/javascript">
		$( document ).ready( function () {
			$.validator.addMethod("valueNotEquals", function(value, element, arg){
				return arg !== value;
			}, "Value must not equal arg.");
			$( "#courseCreateForm" ).validate( {
				rules: {
					name: "required",
					code: "required",
					suborg: { valueNotEquals: "null" }
				},
				messages: {
					name: "Please enter your course name",
					code: "Please enter your course code",
					suborg: "Please select your department"
				},
				errorElement: "em",
				errorPlacement: function ( error, element ) {
					// Add the `help-block` class to the error element
					error.addClass( "invalid-feedback" );

					if ( element.prop( "type" ) === "checkbox" ) {
						error.insertAfter( element.parent( "label" ) );
					} else {
						error.insertAfter( element );
					}
				},
				highlight: function ( element, errorClass, validClass ) {
					$( element ).addClass( "is-invalid" ).removeClass( "is-valid" );
				},
				unhighlight: function (element, errorClass, validClass) {
					$( element ).addClass( "is-valid" ).removeClass( "is-invalid" );
				}
			} );

		} );
	</script>
	<?php
	$app->display_footer("");

?>
	
