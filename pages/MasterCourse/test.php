<?php

echo "Your browser is blocking session information.  Please verify that you are using a <a href='https://community.canvaslms.com/docs/DOC-10720-what-are-the-browser-and-computer-requirements-for-canvas'>supported browser</a> and cross-site tracking is enabled. ";
die();
?>