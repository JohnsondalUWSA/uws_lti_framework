<?php

class masterApp{
	
	public  $id;

	
	function __construct($id) {
	
		$this->id  = htmlspecialchars($id);

	
	}
	
	/**
	 * init function.
	 *
	 * Start session and either store or check the existence of required session vars
	 * 
	 * @access public
	 * @return void
	 */
	 
	public function init() {

	
	}
	
	/**
	 * get_header function.
	 * 
	 * Get the app HTML header
	 *
	 * @access public
	 * @param bool|string $title (default: false) Optionnaly display the app title
	 * @return string The HTML header of the app canvas
	 */
	 
	public function get_header($title=false){
		
		$html = '<!DOCTYPE html>
					<html lang="en">
						<head>
							<meta charset="utf-8">
							<title></title>
						   <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
							<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
							<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
							<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js" crossorigin="anonymous"></script>
							<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js" crossorigin="anonymous"></script>
							<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
						</head>
						<body>';
		if(is_string($title) && !empty($title)){
			$html .= '				<div style="margin-top: 20px;"><h2>'.htmlspecialchars(strip_tags($title)).'</h2></div>';			
		}
		
		return $html;
		
	}
	
	/**
	 * display_header function.
	 * 
	 * Display the app HTML header
	 *
	 * @access public
	 * @param bool|string $title (default: false) Optionnaly display the app title
	 * @return void
	 */
	 
	public function display_header($title=false){
		
		echo $this->get_header($title);
		
	}
	
	/**
	 * get_footer function.
	 * 
	 * Get the app HTML footer
	 *
	 * @access public
	 * @param bool|string $copy (default: false) Optionnaly display the app copywrite/footer text
	 * @return void
	 */
	 
	public function get_footer($copy=false){
		
		$html = '				
								<footer>'.htmlspecialchars(strip_tags($copy)).'</footer>
						</body>
					</html>';
		
		return $html;
		
	}
	
	/**
	 * display_footer function.
	 * 
	 * Display the app HTML footer
	 *
	 * @access public
	 * @param bool|string $copy (default: false) Optionnaly display the app copywrite/footer text
	 * @return void
	 */
	 
	public function display_footer($copy=false){
		
		echo $this->get_footer($copy);
		
	}

}

