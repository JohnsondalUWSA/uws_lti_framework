<?php

use League\Plates\Engine;
use Analog\Analog;


require_once $_SERVER["DOCUMENT_ROOT"]."/".Explode("/",$_SERVER["REQUEST_URI"])[1]."/vendor/autoload.php";

require_once $_SERVER["DOCUMENT_ROOT"]."/".Explode("/",$_SERVER["REQUEST_URI"])[1]."/lib/encrypt/key.php";
require_once $_SERVER["DOCUMENT_ROOT"]."/".Explode("/",$_SERVER["REQUEST_URI"])[1]."/lib/encrypt/uws_encrypt.php";

require_once $_SERVER["DOCUMENT_ROOT"]."/".Explode("/",$_SERVER["REQUEST_URI"])[1]."/lib/logger.php";

require_once $_SERVER["DOCUMENT_ROOT"]."/".Explode("/",$_SERVER["REQUEST_URI"])[1]."/lib/kaltura/KalturaClient.php";



class uws_kaltura_user
{

    public $kclient;
    public $ksession;
    public $kconf;
    public $partnerID;
    public $sharedSecret;

    public function __construct($partnerID, $sharedSecret, $userID)
    {
        $this->partnerID = $partnerID;
        $this->sharedSecret = $sharedSecret;

        $privileges= array();

        $privileges[] = "actionslimit:-1";
        $privileges[] = "firstName:Course";

        $privileges[] = "lastName:Media";
        //$privileges[] = "email:anon@anon.com";

        $privileges[] = "role:privateOnlyRole";



// transform array privileges to string

        $privilegesStr = implode(",", $privileges);


        $user = $userID;


        $this->kconf = new KalturaConfiguration($this->partnerID);
        $this->kconf->setServiceUrl("http://www.kaltura.com");
        $this->kclient = new KalturaClient($this->kconf);

        $this->ksession= $this->kclient->generateSessionV2 ($this->sharedSecret,$userID,KalturaSessionType::USER,$this->partnerID,20,$privilegesStr);

        if (!isset($this->ksession)) {
            die("Could not establish Kaltura session. Please verify that you are using valid Kaltura partner credentials.");
        }


    }

    public function getMyMediaLink ()
    {
        // build iFrame URL with KS

        return  'https://'.$this->partnerID.'.kaf.kaltura.com/hosted/index/my-media/ks/' . $this->ksession;

    }
}