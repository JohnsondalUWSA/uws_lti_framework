<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/".Explode("/",$_SERVER["REQUEST_URI"])[1]."/vendor/autoload.php";
require_once $_SERVER["DOCUMENT_ROOT"]."/".Explode("/",$_SERVER["REQUEST_URI"])[1].'/settings.php';
require_once $_SERVER["DOCUMENT_ROOT"]."/".Explode("/",$_SERVER["REQUEST_URI"])[1]."/lib/encrypt/key.php";
require_once $_SERVER["DOCUMENT_ROOT"]."/".Explode("/",$_SERVER["REQUEST_URI"])[1]."/lib/encrypt/uws_encrypt.php";

require_once $_SERVER["DOCUMENT_ROOT"]."/".Explode("/",$_SERVER["REQUEST_URI"])[1]."/pages/KalturaCourse/biz/uws_kaltura_user.php";

session_name("UWS");
session_set_cookie_params(
    ['secure'=>true,
        'samesite'=> 'None']


);

session_start();


$course_uuid = substr($_SESSION["post"]["custom_uswa_root"],0,5)."-".$_SESSION["post"]["custom_canvas_course_id"];
$user_login = $_SESSION["post"]["custom_canvas_user_login_id"];

$apiToken = uws_encrypt::encrypt_decrypt('decrypt',$restGatewayToken,$mykey);

// Get Current Settings
$curl = curl_init();

curl_setopt_array($curl, array(
    CURLOPT_URL => $restGateway,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 20, //Set timout to 20 seconds
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS => array('StoreProcedureName' => 'asp_get_context','StoreProcedureDBConfig' => 'BBC','extID' => $course_uuid,'userloginID' => $user_login),
    CURLOPT_HTTPHEADER => array(
        "Authorization: ".$apiToken
    ),
   CURLOPT_SSL_VERIFYHOST=> false,
   CURLOPT_SSL_VERIFYPEER=> false

));

$response = curl_exec($curl);

if ($response === false)
{
    echo "Error communicating with Rest API Server: ". curl_error($curl)."</br>";
}

$currentPerf = json_decode($response,true);
curl_close($curl);


$lms_url = $_SESSION['OAuthDomain'];
$token = $lms_token[$lms_url];
$courseID = "uBBCOLLAB-".$_SESSION["launch_params"]["custom_canvas_course_id"];


$sharedsecret =  uws_encrypt::encrypt_decrypt('decrypt', $kaltura_tokens[$lms_url]["kaltura_admin_secret"],$mykey);

$kaltura = new uws_kaltura_user($kaltura_tokens[$lms_url]["kaltura_partner_id"],$sharedsecret,$courseID);

$mediaurl = $kaltura->getMyMediaLink();

// Get Current Settings
$curlB = curl_init();

curl_setopt_array($curlB, array(
    CURLOPT_URL => $restGateway,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 20, // Set Timout to  20 seconds
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS => array('StoreProcedureName' => 'asp_getRecordingLog','StoreProcedureDBConfig' => 'BBC','extCourseID' => $course_uuid),
    CURLOPT_HTTPHEADER => array(
        "Authorization: ".$apiToken
    ),
    CURLOPT_SSL_VERIFYHOST=> false,
    CURLOPT_SSL_VERIFYPEER=> false

));



$responseB = curl_exec($curlB);


if ($responseB === false)
{
    echo "Error communicating with Rest API Server: ". curl_error($curlB)."</br>";
}

$currentLog = json_decode($responseB,true);
curl_close($curlB);


?>

<html>
<head>
    <script
        src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
        crossorigin="anonymous"></script>

    <script
        src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
        integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
        crossorigin="anonymous"></script>

    <!-- Bootstrap 4.0 -->
    <!-- CSS only -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <!-- JS, Popper.js, and jQuery -->
    <!-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    -->
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>

    <!--
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"  integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
-->

    <link href="/<?php echo explode("/", $_SERVER["PHP_SELF"])[1]; ?>/pages/Kaltura/css/jquery.fileupload-ui.css"
          media="screen" rel="stylesheet" type="text/css"/>
    <link href="/<?php echo explode("/", $_SERVER["PHP_SELF"])[1]; ?>/pages/Kaltura/css/jquery.fileupload-ui-kaltura.css"
          media="screen" rel="stylesheet" type="text/css"/>

    <link href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet"/>

    <link rel="stylesheet" type="text/css"
          href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.1/b-colvis-1.6.1/b-flash-1.6.1/b-html5-1.6.1/b-print-1.6.1/cr-1.5.2/kt-2.5.1/r-2.2.3/rr-1.2.6/sc-2.0.1/sp-1.0.1/sl-1.3.1/datatables.min.css"/>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript"
            src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.1/b-colvis-1.6.1/b-flash-1.6.1/b-html5-1.6.1/b-print-1.6.1/cr-1.5.2/kt-2.5.1/r-2.2.3/rr-1.2.6/sc-2.0.1/sp-1.0.1/sl-1.3.1/datatables.min.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>

    <script src="https://cdnapi.kaltura.com/p/2370711/sp/237071100/embedIframeJs/uiconf_id/42910141/partner_id/2370711"></script>

    <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.4/clipboard.min.js"></script>


    <!--  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.
-->
    <style>
        #tbLog {
            font-family: Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        #tbLog td, #tbLog th {
            border: 1px solid #ddd;
            padding: 8px;
        }

        #tbLog tr:nth-child(even){background-color: #f2f2f2;}

        #tbLog tr:hover {background-color: #ddd;}

        #tbLog th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #4CAF50;
            color: white;
        }
    </style>
<script>
    $(document).ready(function(){
       $("#myMedia").prop('checked',<?php if($currentPerf[0]["doNotIncludeInMyMedia"]==0 ){echo 'false';}else {echo 'true';} ?>);
       $("#Archive").prop('checked',<?php if($currentPerf[0]["doNotPullBBC"]==0 ){echo 'false';}else {echo 'true';} ?>);
    });


</script>

</head>
<body>


<h5>Course Media Setting</h5>
<hr>
<form action="update.php" method="post">
    <fieldset>
        <legend style="font-size: medium;">User Preferences</legend>
        <input type="checkbox" id="myMedia" name="myMedia" value="1">
        <label for="myMedia"> Do not include group/course media in Kaltura My Media</label><br>
    </fieldset>
<hr>
    <fiedset>
        <legend style="font-size: medium;">Course Settings (Affects all recordings for this course)</legend>
      <input type="checkbox" id="Archive" name="archive" value="1">
      <label for="Archive">Do not copy Blackboard Collaborate recordings to Kaltura Course Media</label><br><br>
    </fiedset>
    <input type="submit" value="Update">
</form>

<h5>Blackboard Collaborate Ultra Log</h5>
<hr>
<table id="tbLog">

    <thead>
    <tr>
        <th hidden>Extended Course ID</th>
        <th>Recording ID</th>
        <th>Recording Name</th>
        <th>Start Time</th>
        <th>End Time</th>
        <th>Kaltura ID</th>
        <th>Date Deleted from BBC</th>
    </tr>


    </thead>
<tbody>
<?php
if (isset ($currentLog))
foreach ($currentLog as $item)
{
    echo ("<tr>");
    echo ("<td hidden>".$item["CourseID"]."</td>");
    echo ("<td>".$item["RecordingID"]."</td>");
    echo ("<td>".$item["RecordingName"]."</td>");
    echo ("<td>".$item["starttime"]."</td>");
    echo ("<td>".$item["endTime"]."</td>");
    echo ("<td>".$item["kalturaEntryID"]."</td>");
    echo ("<td>".$item["PurgeDate"]."</td>");

    echo ("</tr>");


}
?>

</tbody>

</table>

</body>
</html>