<?php


require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/vendor/autoload.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . '/settings.php';
require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/lib/encrypt/key.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/lib/encrypt/uws_encrypt.php";


require_once($_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/pages/Kaltura/Biz/uwsKalturaClient.php");

session_name("UWS");
session_set_cookie_params(
    ['secure' => true,
        'samesite' => 'None'
    ]
);

session_start();

$myMedia = '0';
$archive ='0';

if(isset($_POST["myMedia"]))
{
    $myMedia = '1';
}


if(isset($_POST["archive"]))
{
    $archive = '1';
}

$course_uuid = substr($_SESSION["post"]["custom_uswa_root"],0,5)."-".$_SESSION["post"]["custom_canvas_course_id"];


$user_login = $_SESSION["post"]["custom_canvas_user_login_id"];
$courseID = "CourseMedia-G-".$course_uuid;


$apiToken = uws_encrypt::encrypt_decrypt('decrypt', $restGatewayToken, $mykey);

// Get Current Settings
$curl = curl_init();

curl_setopt_array($curl, array(
    CURLOPT_URL => $restGateway,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 20,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS => array('StoreProcedureName' => 'asp_context_user_preference','StoreProcedureDBConfig' => 'BBC',
        'extID' => $course_uuid,'userloginID' => $user_login, 'doNotIncludeInMyMedia'=>$myMedia),
    CURLOPT_HTTPHEADER => array(
        "Authorization: ".$apiToken
    ),
    CURLOPT_SSL_VERIFYHOST=> false,
    CURLOPT_SSL_VERIFYPEER=> false

));

$response = curl_exec($curl);

$updateError = false;
if ($response === false)
{
    $updateError = true;
    echo "Error communicating with Rest API Server: ". curl_error(($curl))."</br>";
}

curl_close($curl);


//Set Course Settings

$curl = curl_init();

curl_setopt_array($curl, array(
    CURLOPT_URL => $restGateway,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 20,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS => array('StoreProcedureName' => 'asp_update_context_preference','StoreProcedureDBConfig' => 'BBC',
        'extID' => $course_uuid,'userloginID' => $user_login, 'doNotPullBBC'=>$archive),


    CURLOPT_HTTPHEADER => array(
        "Authorization: ".$apiToken
    ),
    CURLOPT_SSL_VERIFYHOST=> false,
    CURLOPT_SSL_VERIFYPEER=> false

));

$response = curl_exec($curl);


if ($response === false)
{
    $updateError = true;

    echo "Error communicating with Rest API Server: ". curl_error($curl)."</br>";
}
curl_close($curl);


$lms_url = $_SESSION['OAuthDomain'];
$token = $lms_token[$lms_url];

$sharedsecret =  uws_encrypt::encrypt_decrypt('decrypt', $kaltura_tokens[$lms_url]["kaltura_admin_secret"],$mykey);

$kaltura = new uwsKalturaclient($kaltura_tokens[$lms_url]["kaltura_partner_id"],$sharedsecret,null, null);

$kaltura ->addGroup($courseID);

if ($myMedia == 1)
{
    $kaltura ->delGroupMember($courseID,$user_login);
}
else $kaltura ->addGroupMember($courseID,$user_login);

if ($updateError === false) {
    $redirect = "https://" . $_SERVER["SERVER_NAME"] . "/" . explode("/", $_SERVER["REQUEST_URI"])[1] . "/pages/KalturaCourse/index.php";
    header("Location: " . $redirect);
    exit();
}
