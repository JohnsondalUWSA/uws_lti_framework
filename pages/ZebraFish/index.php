<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/".Explode("/",$_SERVER["REQUEST_URI"])[1]."/vendor/autoload.php";
require_once $_SERVER["DOCUMENT_ROOT"]."/".Explode("/",$_SERVER["REQUEST_URI"])[1].'/settings.php';
require_once $_SERVER["DOCUMENT_ROOT"]."/".Explode("/",$_SERVER["REQUEST_URI"])[1]."/lib/encrypt/key.php";
require_once $_SERVER["DOCUMENT_ROOT"]."/".Explode("/",$_SERVER["REQUEST_URI"])[1]."/lib/encrypt/uws_encrypt.php";

require_once $_SERVER["DOCUMENT_ROOT"]."/".Explode("/",$_SERVER["REQUEST_URI"])[1]."/pages/ZebraFish/settings.php";


session_name("UWS");
session_set_cookie_params(
    ['secure'=>true,
        'samesite'=> 'None'
    ]
);

session_start();

if ( !isset($_SESSION["post"]["custom_canvas_user_login_id"]))
{
    echo ("This tool requires the use of cookies. Please enable cookies." );
    http_response_code(403);
    exit();
}
;
# START CONFIGURATION SECTION
#
$launch_url = $zlaunchurl;
$key = $zkey;
$sharedsecret =uws_encrypt::encrypt_decrypt('decrypt', $zsecret,$mykey);



$lms_url = $_SESSION['OAuthDomain'];
$token = $lms_token[$lms_url];

# ------------------------------
# START CONFIGURATION SECTION
#


$secret = $sharedsecret;


// Tsugi Test
//$launch_url = "https://www.tsugi.org/lti-test/tool.php";

//$key = "12345";
//$secret = "secret";

$launch_data = $_SESSION["post"];

unset($launch_data["oauth_signature"]);

foreach ($launch_data as $value)
{
    $value = clean($value);

}
#
# END OF CONFIGURATION SECTION
# ------------------------------

$now = new DateTime();

$launch_data["lti_version"] = "LTI-1p0";
$launch_data["lti_message_type"] = $_SESSION["post"]["lti_message_type"];



# Basic LTI uses OAuth to sign requests
# OAuth Core 1.0 spec: http://oauth.net/core/1.0/

$launch_data["oauth_callback"] = "about:blank";
$launch_data["oauth_consumer_key"] = $key;
$launch_data["oauth_version"] = "1.0";
$launch_data["oauth_nonce"] = uniqid(mt_rand(1,1000));
$launch_data["oauth_timestamp"] = time();
$launch_data["oauth_signature_method"] = "HMAC-SHA1";

ksort($launch_data);
$launch_params = array();
$launch_data_keys = array_keys($launch_data);


foreach ($launch_data_keys as $key) {
    array_push($launch_params, $key . "=" . rawurlencode($launch_data[$key]));
}


$base_string = "POST&" . rawurlencode($launch_url) . "&" . rawurlencode(implode("&", $launch_params));
$secret = rawurlencode($secret) . "&";
$signature = base64_encode(hash_hmac("sha1", $base_string, $secret, true));

function clean($string) {
    $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

    return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}

?>

<html>
<head>

    <script
            src="https://code.jquery.com/jquery-3.4.1.min.js"
            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
            crossorigin="anonymous"></script>

    <script
            src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
            integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
            crossorigin="anonymous"></script>

    <script>
        $(document).ready(function(){
            $('#submit').click();
        });
    </script>

</head>
<!-- <body onload="document.ltiLaunchForm.submit();"> -->
<body>
<form id="ltiLaunchForm" name="ltiLaunchForm" method="POST" action="<?php printf($launch_url); ?>">
    <?php foreach ($launch_data as $k => $v ) { ?>
        <input type="hidden" name="<?php echo $k ?>" value="<?php echo $v ?>">
    <?php } ?>
    <input type="hidden" name="oauth_signature" value="<?php echo $signature ?>">
    <button id="submit" type="submit" style="display: none;" >Launch</button>
</form>
<body>
</html>