
<?php

// Get LMS Domain and Token
$lms_url = $_SESSION['OAuthDomain'];
$token = $lms_token[$lms_url];


// Init Canvas Biz class
// Class uses to make API Calls.

$canvas = new uws_canvas(uws_encrypt::encrypt_decrypt('decrypt',$token, $mykey), $lms_url);


// Retrieve Course ID from LTI Launch
$courseID = $_SESSION["post"]["custom_canvas_course_id"];

// Pull Canvas API
$course = $canvas->get_course($courseID);

$myJSON = json_encode($course);

echo $myJSON ;



