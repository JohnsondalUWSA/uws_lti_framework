<?php

$dbName = "CanvasDataUWS";
$dbFileLocation = "D:\SQLData\\";
$year = 2017;
$month = 10;

$intervals = "";
$partCnt = 0;

while ($year < 2022)
{
    while ($month < 13)
    {
        $intervals = $intervals. "'".$year."-".sprintf("%02d",$month)."',\r\n";


        $month++;
        $partCnt++;
    }
    $month = 1;
    $year++;
}

echo "<br> GO <br> USE MASTER <br>";


for ($x = 0; $x<=$partCnt; $x++)
{
    echo "<br>";
    echo "GO";
    echo "<br>";
    echo "ALTER DATABASE ".$dbName." ADD FILEGROUP ".$dbName."_requests_".sprintf("%03d",$x)."\r\n";
    echo "<br> GO";

    echo "<br> ALTER DATABASE ".$dbName." ADD FILE (NAME = ".$dbName."_requests_".sprintf("%003d",$x).",
        FILENAME = '".$dbFileLocation."\\".$dbName."_requests_".sprintf("%03d",$x).".ndf', 
        SIZE = 8192KB , FILEGROWTH = 65536KB) TO FILEGROUP ".$dbName."_requests_".sprintf("%03d",$x)."\r\n";
    echo "<br>";



}

$partScheme = "CREATE PARTITION SCHEME prtSchemeRequests 
    AS PARTITION requestRange
    TO (";


$partList = "";


for ($x=0;$x<=$partCnt;$x++)
{
$partScheme  = $partScheme.$dbName."_requests_".sprintf("%03d",$x).",\r\n";
}
$partScheme = substr($partScheme,0,-3). ");";

echo  "<br> GO <br> Use ".$dbName." <br> ";
echo "<br> GO <br>";
echo "CREATE PARTITION FUNCTION requestRange (varchar(256)  )
AS RANGE LEFT FOR VALUES (".substr($intervals,0,-3).");";

echo "<br> GO";

echo "<br>". $partScheme;



