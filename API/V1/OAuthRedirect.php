<?php

require_once dirname(__FILE__) . "/../../lib/OAuthHandler.php";

require_once dirname(__FILE__) . "/../../vendor/autoload.php";

session_name("UWS");
session_set_cookie_params(
    ['secure'=>true,
        'samesite'=> 'None'
    ]
);

session_start();


// validate code
if (isset($_REQUEST["error"]))
{
    echo ("Developer Key - Oauth Error - ".$_REQUEST["error"]);
    exit;
}

$_SESSION["OauthCode"] = $_REQUEST["code"];
$_SESSION["OauthState"] = $_REQUEST["state"];



// Init Oauth Handler
$OAuthInfo = new OAuthHandler($_SESSION["OAuthDomain"], $_SESSION["OAuthClientID"], $_SESSION["OAuthClientKey"], $_SESSION["OAuthRedirect"]);

if (!$OAuthInfo -> getRefreshToken($_SESSION["OauthCode"]))
{
    echo ("Failed Auth!");
    exit();

}


$_SESSION["OAuthDevKeyInfo"] = $OAuthInfo->OAuthTokenInfo;

$re = "../../pages/".$_SESSION["OAuthRedirectTo"];

session_commit();
require($re);


    //= "OAuth/index.php";