<?php

require('settings.php');
require('HRSValidateAccess.php');
require('HRSEnrollmentGrade.php');

// Lib to Decrypt token in settings file.
require("../../../lib/encrypt/uws_encrypt.php");
require("../../../lib/encrypt/key.php");

$authtoken = "";

// This comes from the headers
if (isset($_POST["AuthToken"]))
    $authtoken = "authorization:Bearer " . $_POST["AuthToken"];
else {
    echo("missing authorization");
    exit ();

}

// query parms
$sis_course_id = "";
$startDateTime = "";
$endDateTime = "";


if (isset($_POST["course"]))
    $sis_course_id = $_POST["course"];
else {
    echo("Missing course information");
    exit();

}


if (isset($_POST["start"])) {
    $startDateTime = dateConvertToUTC(str_replace ("T"," ", $_POST["start"]));
    if ($startDateTime == false) {
        echo("Start is not in the proper format.  Please supply it in 'Y-m-d H:i' ");
        exit();

    }
} else {
    echo("Missing Start Time");
    exit();

}


if (isset($_POST["end"])) {
    $endDateTime = dateConvertToUTC(str_replace("T", " " , $_POST["end"]));
    if ($endDateTime == false) {
        echo("End is not in the proper format.  Please supply it in 'Y-m-d H:i' ");
        exit();
    }
} else {
    echo("Missing End Time");
    exit();

}


// Validate Access by getting course Info using requester Canvas Token
$hrsValidateAccess = new HRSValidateAccess($authtoken, $CanvasURLTraining);

$courseInfo = $hrsValidateAccess->ValidateCourseAccess($sis_course_id);

// EXIT IS API DIES
if (isset($courseInfo[0]->errors[0]->message)) {
    echo("Unable to validate token or Course is not Found :( :(");
    echo($courseInfo[0]->errors[0]->message);
    exit();
}

// Decrypt Service Account Tokens
$TrainingToken = uws_encrypt::encrypt_decrypt('decrypt', $trainingToken, $mykey);
$SISToken = uws_encrypt::encrypt_decrypt('decrypt', $sisToken, $mykey);

//Init Canvas Curld

// Get Grade Change History Period  // Note we only want to keep the latest record
$gradeChangeApi = "/audit/grade_change/courses/" . $courseInfo[0]->id . "?start_time=" . $startDateTime . "&end_time=" . $endDateTime;

$trainingCanvasCurl = new uws_canvas($TrainingToken, $CanvasURLTraining);
$SISCanvasCurl = new uws_canvas($SISToken, $CanvasURLSIS);

$GradeChanges = $trainingCanvasCurl->get($gradeChangeApi, null);

//echo var_dump($GradeChanges);

$grades = array();
foreach ($GradeChanges as $eItem)
    foreach ($eItem->events as $item) {
        if (isNewStudent($grades, $item->links->student)) {
            $eGrade = new HRSEnrollmentGrade();

            $enrollment = getEnrollment($trainingCanvasCurl, $courseInfo[0]->id, $item->links->student);
            $user = getUser($SISCanvasCurl, $item->links->student);
            $comm = getCommChannels($SISCanvasCurl, $item->links->student);

            $eGrade->CANVAS_COURSE_ID = $courseInfo[0]->id;
            $eGrade->CANVAS_USER_ID = $item->links->student;
            $eGrade->COURSE_NAME = $courseInfo[0]->name;
            $eGrade->SIS_COURSE_ID = $courseInfo[0]->sis_course_id;
            $eGrade->SIS_USER_ID = $user[0]->sis_user_id;
            $eGrade->SIS_INTERGRATION_ID = $user[0]->integration_id;
            $eGrade->USERNAME = $user[0]->name;


            if (isset($enrollment[0]->created_at))
                $eGrade->ENROLLMENT_CREATED_AT = dateConvert($enrollment[0]->created_at);
            if (isset ($enrollment[0]->last_activity_at))
                $eGrade->ENROLLMENT_LAST_ACTIVITY = dateConvert($enrollment[0]->last_activity_at);
            if (isset ($item->created_at))
                $eGrade->GRADE_LAST_UPDATE = dateConvert($item->created_at);


            $eGrade->GRADE_NAMED_SCORE = $item->grade_after;

            if (isset($comm[0]->address))
                $eGrade->EMAIL1 = $comm[0]->address;
            if (isset($comm[1]->address))
                $eGrade->EMAIL2 = $comm[1]->address;
            if (isset($comm[2]->address))
                $eGrade->EMAIL3 = $comm[2]->address;
            if (isset($comm[3]->address))
                $eGrade->EMAIL4 = $comm[3]->address;

            array_push($grades, $eGrade);

        }

        //$eGrade ->EMAIL1

    }

//echo var_dump($grades);


// Function to get Users Enrollment
function getEnrollment($curl, $CanvasCourseID, $CanvasUserID)
{
    $apiURL = "/courses/" . $CanvasCourseID . "/enrollments?user_id=" . $CanvasUserID;

    return $curl->get($apiURL, null);
}

// Function to get Users Login Details
function getUser($curl, $CanvasUserID)
{
    $apiURL = "/users/" . $CanvasUserID;

    return $curl->get($apiURL, null);

}

// Function to get User Comm Channels
function getCommChannels($curl, $CanvasUserID)
{
    $apiURL = "/users/" . $CanvasUserID . "/communication_channels";
    return $curl->get($apiURL, null);

}

function isNewStudent($grades, $studentID)
{
    foreach ($grades as $item) {
        if ($item->CANVAS_USER_ID == $studentID)
            return false;

    }

    return true;
}


function dateConvert($date)
{
    $newDate = new DateTime($date, new DateTimeZone('UTC'));
    $newDate->setTimezone(new DateTimeZone('America/Chicago'));
    return $newDate->format('Y-m-d H:i:s');
    //date("Y-m-d H:i:s", strtotime('-5 hours',strtotime($date)));

}

function dateConvertToUTC($date)
{

    $newdate = DateTime::createFromFormat('Y-m-d H:i', $date, new DateTimeZone('America/Chicago'));
    if ($newdate == false) {
        return false;

    }
    $newdate->setTimezone(new DateTimeZone('UTC'));
    return $newdate->format(DATE_ISO8601);
}

//Create Header
$gradeC = new HRSEnrollmentGrade();
$reflect = new ReflectionClass($gradeC);
$props   = $reflect->getProperties(ReflectionProperty::IS_PUBLIC | ReflectionProperty::IS_PROTECTED);
$headings = array();

foreach ($props as $prop) {
    $headings []  = $prop->name;
}

// Open the output stream
$fh = fopen('php://output', 'w');

// Start output buffering (to capture stream contents)
ob_start();

fputcsv($fh, $headings);
$gradesOutput = json_decode(json_encode($grades),true);
// Loop over the * to export
if (!empty($gradesOutput)) {
    foreach ($gradesOutput as $item) {
        fputcsv($fh, $item,",",'"');
    }
}

// Get the contents of the output buffer
$string = ob_get_clean();

$filename = 'GradeHistory_' . date('Ymd') . '_' . date('His');

// Output CSV-specific headers
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: private", false);
header("Content-Type: application/octet-stream");
header("Content-Disposition: attachment; filename=".$filename.".csv;");
header("Content-Transfer-Encoding: binary");

exit($string);


header("Access-Control-Allow-Origin: *");
header('Content-type: application/json');
echo json_encode($grades);
