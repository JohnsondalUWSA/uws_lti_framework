<?php


?>

<html>
<head>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>

    <script>
        $("input").on("change", function() {
            this.setAttribute(
                "data-date",
                moment(this.value, "Y-m-d H:i")
                    .format( this.getAttribute("data-date-format") )
            )
        }).trigger("change")

    </script>

</head>
<body>
<h2>UWS-TD Training Grade History Report</h2>
<p>Note: This can be a long running report and requires you to generate and manage a token.</p>

<form method="post" action="GetGradeHistoryCSV.php">
    Course SIS ID: <input type="text" name="course"><br>
    Start Date: <input type="datetime-local" data-date="" data-date-format="Y-m-d H:i" name ="start"><br>
    End Date: <input type="datetime-local" data-date="" data-date-format="Y-m-d H:i" name ="end"><br>
    API Token: <input type="password" name="AuthToken"><br>
    <input type="submit">
</form>

</body>

</html>
