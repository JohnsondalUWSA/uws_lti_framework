<?php

require ('../../../lib/uws_canvas.php');

class HRSValidateAccess
{
    private $mdomain = "";
    private $mtoken = "";
    private $mtokenRaw = "";

    const api_course = "/courses/sis_course_id:{0}";

    public function __construct($token, $domain)
    {
        $this->mdomain = $domain;

        $this->mtoken = $token;

    }

    public function ValidateCourseAccess ($sis_course_id)
    {

        $apiCall = str_replace("{0}",$sis_course_id,self::api_course);
        $cURL = new Curl($this->mtoken, $this->mdomain);


        $results = $cURL -> get($apiCall);

        $cURL->closeCurl();

        return $results;


    }



}