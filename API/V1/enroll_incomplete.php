<?php

require_once('../../settings.php');
require_once('../../lib/encrypt/key.php');
require_once ('../../lib/encrypt/uws_encrypt.php');
require '../../vendor/autoload.php';
require_once  ('../../lib/uws_canvas.php');
require_once ('../../lib/MeekroDB/meekrodb.2.3.class.php');


session_name("UWS");
session_set_cookie_params(
    ['secure'=>true,
        'samesite'=> 'None'
    ]
);

session_start();
// Set MySQL Info
DB::$host = $mysql_server;
DB::$user = $mysql_user;
DB::$password = uws_encrypt::encrypt_decrypt('decrypt', $mysql_password,$mykey);
DB::$dbName = $mysql_db;

$lms_url = $_SESSION['OAuthDomain'];
$token = $lms_token[$lms_url];



$canvas = new uws_canvas(uws_encrypt::encrypt_decrypt('decrypt', $token,$mykey), $lms_url);

// Validate Data
// CourseID, UserList, EndDate, Reason
//
$templates = new League\Plates\Engine('../../templates');


// Validate Post
if (empty($_POST)) {
    $nodata["msg"]="This Page must be called via a Post";
    echo($templates->render('msg', $nodata));
    exit();
}

// Validate Cookie
//if (!isset($_COOKIE["UWS-LTI"]))
if (!isset ($_SESSION['valid']))
{
    $noSession["msg"]="Invalid Session";
    echo($templates->render('msg', $noSession));
    exit();

}
else
{
    if (!$_SESSION['valid']== true)
    {
        $noSession["msg"]="Invalid Session";
        echo($templates->render('msg', $noSession));
        exit();


    }


}

//$cookiedata  =unserialize($_COOKIE["UWS-LTI"]);


foreach ($_POST["users"] as $user) {


    // Lookup Users Name
            $userInfo = $canvas->get_user($user)[0];
    // Create Course Section



            $section = $canvas->create_section($_SESSION["post"]["custom_canvas_course_id"], $_POST["endDate"], "Extend - " . $userInfo->name);
    // Exit if section id is null
            if (is_null($section->id)){ echo ("Unable to create enrollment for -".$userInfo->name); exit();}
    // Enroll User In Section
             $enrollment = $canvas->create_enrollment($section->id, $user);
    // Log Transaction
                DB::insert('tbenrollmentlog', array(
                    'AdminUserID' => $_SESSION["post"]["custom_canvas_user_id"],
                    'AdminName' => $_SESSION["post"]["lis_person_name_full"],
                    'AdminRole' => $_SESSION["post"]["roles"],
                    'CourseID' => $_SESSION["post"]["custom_canvas_course_id"],
                    'EnrollUserID' => $user,
                    'Reason' => $_POST["reason"],
                    'EndDate' => $_POST["endDate"]

                ));





    echo ("GOOD");
}
