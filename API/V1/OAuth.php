<?php

require_once dirname(__FILE__) . "/../../vendor/autoload.php";
session_name("UWS");
session_set_cookie_params(
    ['secure'=>true,
        'samesite'=> 'None'
    ]
);

session_start();




// Setting File - in local dir
require_once 'OAuthSettings.php';

// OAuth Developer Key Handler
require_once dirname(__FILE__) . "/../../lib/OAuthHandler.php";


// Verify User has Valid LTI SESSION
if (!isset ($_SESSION['OAuthKey']))
{
    $nodata["msg"]="This Page must be called via a Post";
    echo($nodata["msg"]);
    exit();
}

// Get API Domain
$OAuthDomain =  $_SESSION['OAuthDomain'];
//$OAuthKey = $_SESSION['OAuthKey'];
$client_id = $dev_token[$OAuthDomain][0];
$OAuthKey  = $dev_token[$OAuthDomain][1];

$_SESSION["OAuthClientID"] = $client_id;
$_SESSION["OAuthClientKey"] = $OAuthKey;

$redirect = "Location: https://".$OAuthDomain."/login/oauth2/auth?client_id=".$client_id."&response_type=code&state=".$OAuthKey."&redirect_uri=https://".$_SERVER['SERVER_NAME'].str_replace("index.php","API/V1/OAuthRedirect.php",$_SERVER["REQUEST_URI"]);
$_SESSION["OAuthRedirect"] = $redirect;

session_commit();
// Init Oauth Handler
$OAuthInfo = new OAuthHandler($OAuthDomain, $client_id, $OAuthKey, $redirect);

//Does user have established Token?
$HasToken = $OAuthInfo -> GetSavedToken( $_SESSION['launch_params']['custom_canvas_user_id'], $client_id );
$TokenOK = false;

if ($HasToken == true)
{
    $TokenOK = $OAuthInfo -> CheckToken();

}

if (!$TokenOK)
{   $HasToken == false;}
// If Token as been set, send user to OAuthRedirectTo (Post OAuth Page)


if ($HasToken) {
    $_SESSION["OAuthDevKeyInfo"] = $OAuthInfo->OAuthTokenInfo;

    session_commit();
    $re = dirname(__FILE__)."/../../pages/" . $_SESSION["OAuthRedirectTo"];
    require($re);
}else
{
   session_commit();
    header($redirect);


}

