<?php

use League\Plates\Engine;
use Analog\Analog;

require_once $_SERVER["DOCUMENT_ROOT"]."/".Explode("/",$_SERVER["REQUEST_URI"])[1]."/vendor/autoload.php";
require_once $_SERVER["DOCUMENT_ROOT"]."/".Explode("/",$_SERVER["REQUEST_URI"])[1]."/lib/kaltura/KalturaClient.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/settings.php";

require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/lib/encrypt/key.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/lib/encrypt/uws_encrypt.php";

require_once ("PlayerSource.php");


$config;
$client;
$ks ="";


if (!isset($_POST["authorization"]) )
{

    http_response_code(401);
    exit();
}

if (!isset($_POST["entryId"]))
{
    http_response_code((400));
    exit();
}

$duration = $_POST["duration"];
$mediaEntry = $_POST["entryId"];

$tokenInfo = str_rot13($_POST["authorization"]);

$tokenid ="";
$token = "";

if (strpos($tokenInfo,":") !== false) {
    $tokenid = explode(":", $tokenInfo)[0];
    $token = explode(":", $tokenInfo)[1];

}
else {http_response_code(401); exit();}

//Get Encrypted Secret
$sharedsecret =  uws_encrypt::encrypt_decrypt('decrypt', $kaltura_admin_secret,$mykey);

// Init Kaltura Config
$config = new KalturaConfiguration($kaltura_partner_id);
$config -> setServiceUrl($kaltura_service_url);
$client = new KalturaClient($config);

$result = "";
$ks = "";

// Start Widget Session
try {
    //$result = $client->session->startWidgetSession("_" . $kaltura_partner_id, 86400);
    //$ks = $result->ks;
    $ks = $client->session->start(
        $sharedsecret,
        "djohnson@uwsa.edu",
        KalturaSessionType::ADMIN,
        $kaltura_partner_id);
    $client->setKS($ks);

// Start Session using Hash Token
//    $client->setKs($ks);
//    $tokenHash = hash('sha256', $ks . $token);
//    $result = $client->appToken->startSession($tokenid, $tokenHash, null, KalturaSessionType::USER, 0, "");
//    //$result = $client->appToken->startSession($tokenid, $tokenHash, null, null, null, null);
//    $ks = $result-> ks;

}
catch (Exception $e)
{
    error_log("Kaltura.API: ".$e-> getMessage());
    http_response_code(401); exit();
}



$rtnValue = new PlayerCaption();

try {
    $captionPlugin = KalturaCaptionClientPlugin::get($client);
    $filter = new KalturaAssetFilter();
    $filter->entryIdEqual = $mediaEntry;
    $pager = new KalturaFilterPager();
    $pager->pageSize = 500;

    $result = $captionPlugin->captionAsset->listAction($filter, $pager);

    $isfirst = true;
    $captionsJson = "";
    foreach ($result->objects as $captionAsset) {
        $captionItem = new PlayerCaption();
        $captionItem->SetData($captionAsset->id,
            $captionPlugin->captionAsset->getUrl($captionAsset->id),
                $captionAsset->language,
            $duration);
        if ($isfirst)
        {
        $captionsJson = "[".$captionsJson . json_encode($captionItem);
            $isfirst = false;}
        else {
            $captionsJson = $captionsJson .",". json_encode($captionItem);

        }


    }
    $captionsX = json_decode($captionsJson . "]", true);
    $rtnValue= $captionsX;
}
catch (Exception $e)
{
    error_log("Kaltura.API.GetCaptions: " . $e->getMessage());
    http_response_code(500);
    exit();

}



header("Access-Control-Allow-Origin: *");
header('Content-type: application/json');
echo json_encode($rtnValue);







