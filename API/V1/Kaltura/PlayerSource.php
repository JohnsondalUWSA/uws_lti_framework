<?php


class PlayerSource
{
    public $entryId="";
    public $name="";
    public $description = "";
    public $duration = "";

    public $status = "";
   // public $thumbnail = "";
    public $sources = "";
    public $captions = "";
    // public $thumbNails = "";
    public $SessionId = "";
    public $errMessage = "";




}

class PlayerFlavor
{

    public $flavorParamsId;
    public $flavorId ;
    public $status;
    public $width;
    public $height;

    public $src;
    public $entryID;
    public $extension;
    private $sessionID;



    function SetData($serviceURL , $ks, $partnerID, $EntryID, $Extension,  $FlavorParamsId, $FlavorId, $Width, $Height, $status  )
    {

        $this-> flavorId = $FlavorId;
        $this-> flavorParamsId = $FlavorParamsId;
        $this-> width = $Width;
        $this-> height = $Height;
        $this-> entryID =$EntryID;
        $this -> extension = $Extension;
        $this -> sessionID = $ks;
        $this -> status = $status;


        // Generate src URL docs
        //https://vpaas.kaltura.com/documentation/Deliver-and-Distribute-Media/how-retrieve-download-or-streaming-url-using-api-calls.html
        //[serviceUrl]/p/[YourPartnerId]/sp/0/playManifest/entryId/[YourEntryId]/format/[StreamingFormat]/protocol/[Protocol]/flavorParamId/[VideoFlavorId]/ks/[ks]/video.[ext].

        $this ->src = $serviceURL ."/p/".$partnerID. "/sp/0/playManifest/entryId/".$this ->entryID.
            "/format/url/protocol/https/flavorParamId/".$this ->flavorParamsId."/ks/".$ks."/video.".$this->extension;
        return;
    }

}

class PlayerCaption
{
    public $captionID ;
    public $captionLang;
    public $captionURL;
    public $captionWebVTTURL;

    private $duration;

    public function SetData ($captionID, $captionURL, $captionLang, $duration )
    {
        $this -> captionURL = $captionURL;
        $this -> captionID = $captionID;
        $this -> captionLang = $captionLang;
        $this -> duration = $duration;

        $this -> captionWebVTTURL = str_replace("serve/captionAssetId/".$captionID,
            "servewebvtt/captionAssetId/".$captionID."/segmentDuration/".$duration."/segmentIndex/1",
        $captionURL);



    }


}

class PlayerThumbNail
{
    public $thumbNailID ;
    public $thumbNailURL;
    public $thumbHeight;
    public $thumbWidth;
    public $thumbDescription;

    public function SetData ($thumbNailID, $thumbNailURL, $thumbHeight, $thumbWidth, $thumbDescription )
    {
        $this -> thumbNailID = $thumbNailID;
        $this -> thumbNailURL = $thumbNailURL;
        $this -> thumbHeight = $thumbHeight;
        $this -> thumbWidth = $thumbWidth;
        $this -> thumbDescription = $thumbDescription;
        return;

    }


}

