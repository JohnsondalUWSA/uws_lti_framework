<?php

use League\Plates\Engine;
use Analog\Analog;

require_once $_SERVER["DOCUMENT_ROOT"]."/".Explode("/",$_SERVER["REQUEST_URI"])[1]."/vendor/autoload.php";
require_once $_SERVER["DOCUMENT_ROOT"]."/".Explode("/",$_SERVER["REQUEST_URI"])[1]."/lib/kaltura/KalturaClient.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/settings.php";

require_once ("PlayerSource.php");


$config;
$client;
$ks ="";


if (!isset($_POST["authorization"]) )
{

    http_response_code(401);
    exit();
}

if (!isset($_POST["entryId"]))
{
    http_response_code((400));
    exit();
}

$mediaEntry = $_POST["entryId"];

$tokenInfo = str_rot13($_POST["authorization"]);

$tokenid ="";
$token = "";

if (strpos($tokenInfo,":") !== false) {
    $tokenid = explode(":", $tokenInfo)[0];
    $token = explode(":", $tokenInfo)[1];

}
else {http_response_code(401); exit();}

//Get Encrypted Secret

// Init Kaltura Config
$config = new KalturaConfiguration($kaltura_partner_id);
$config -> setServiceUrl($kaltura_service_url);
$client = new KalturaClient($config);

$result = "";
$ks = "";

// Start Widget Session
try {

    $result = $client -> session -> startWidgetSession("_".$kaltura_partner_id);
    $ks = $result-> ks;
    $tokenHash = hash('sha256', $ks . $token);
    $client-> setKs($ks);
    $result = $client->appToken->startSession($tokenid, $tokenHash, null, KalturaSessionType::USER, 0, "");

}
catch (Exception $e)
{
    error_log("Kaltura.API: ".$e-> getMessage());
    http_response_code(401); exit();
}



$rtnValue = new PlayerSource();
try {
// Get Top Level Media Info
    $filter = new KalturaMediaEntryFilter();
    $pager = new KalturaFilterPager();
    $filter-> idEqual=$mediaEntry;


        $result = $client->media->listAction($filter, $pager);

        if ($result->totalCount == 0) {
            $rtnValue->errMessage = "ENTRY_ID_NOT_FOUND";
            $rtnValue->entryId = $mediaEntry;
            header('Content-type: application/json');
            echo json_encode($rtnValue);
            exit();
        }

        $mediaResult = $result->objects[0];

        // $mediaResult = $client->media->get($mediaEntry, -1);


    $rtnValue->entryId = $mediaResult->id;
    $rtnValue->name = $mediaResult->name;
    $rtnValue->description = $mediaResult->description;
//    $rtnValue->thumbnail = $mediaResult->thumbnailUrl;
    $rtnValue->SessionId = $ks;
    $rtnValue->duration = $mediaResult -> duration;
    $rtnValue->status = $mediaResult-> status;

}
catch (Exception $e)
{
    if ($e->getCode() == "ENTRY_ID_NOT_FOUND")
    {
        $rtnValue->errMessage = "ENTRY_ID_NOT_FOUND";
        $rtnValue->entryId = $mediaEntry;
        header('Content-type: application/json');
        echo json_encode($rtnValue);
        exit();
    }
    else {
        error_log("Kaltura.API.LookupEntry: " . $e->getMessage());
        http_response_code(500);
        exit();
    }
}



//GetFlavor Details
$rtnValue-> sources = [];

try {
    $flavors = $client->flavorAsset->getByEntryId($mediaEntry);
    foreach ($flavors as $flavor) {

        $item = new PlayerFlavor();
        $item->SetData($kaltura_service_url, $ks, $kaltura_partner_id, $mediaEntry,
            $flavor->fileExt, $flavor->flavorParamsId, $flavor->id, $flavor->width, $flavor->height, $flavor-> status);
        array_push($rtnValue->sources, $item);
    }

}
catch (Exception $e)
{
    error_log("Kaltura.API.GetFlavors: " . $e->getMessage());
    http_response_code(500);
    exit();

}

header("Access-Control-Allow-Origin: *");
header('Content-type: application/json');
echo json_encode($rtnValue);







