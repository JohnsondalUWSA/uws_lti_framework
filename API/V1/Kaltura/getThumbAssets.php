<?php

use League\Plates\Engine;
use Analog\Analog;

require_once $_SERVER["DOCUMENT_ROOT"]."/".Explode("/",$_SERVER["REQUEST_URI"])[1]."/vendor/autoload.php";
require_once $_SERVER["DOCUMENT_ROOT"]."/".Explode("/",$_SERVER["REQUEST_URI"])[1]."/lib/kaltura/KalturaClient.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/" . Explode("/", $_SERVER["REQUEST_URI"])[1] . "/settings.php";

require_once ("PlayerSource.php");


$config;
$client;
$ks ="";


if (!isset($_POST["authorization"]) )
{

    http_response_code(401);
    exit();
}

if (!isset($_POST["entryId"]))
{
    http_response_code((400));
    exit();
}

$mediaEntry = $_POST["entryId"];

$tokenInfo = str_rot13($_POST["authorization"]);

$tokenid ="";
$token = "";

if (strpos($tokenInfo,":") !== false) {
    $tokenid = explode(":", $tokenInfo)[0];
    $token = explode(":", $tokenInfo)[1];

}
else {http_response_code(401); exit();}

// Init Kaltura Config
$config = new KalturaConfiguration($kaltura_partner_id);
$config -> setServiceUrl($kaltura_service_url);
$client = new KalturaClient($config);

$result = "";
$ks = "";

// Start Widget Session
try {

    $result = $client -> session -> startWidgetSession("_".$kaltura_partner_id);
    $ks = $result-> ks;
    $tokenHash = hash('sha256', $ks . $token);
    $client-> setKs($ks);
    $result = $client->appToken->startSession($tokenid, $tokenHash, null, KalturaSessionType::USER, 0, "");

}
catch (Exception $e)
{
    error_log("Kaltura.API: ".$e-> getMessage());
    http_response_code(401); exit();
}



$rtnValue = "";

try {
// Get ThumbNails
    $filter = new KalturaAssetFilter();
    $filter->entryIdEqual = $mediaEntry;
    $pager = new KalturaFilterPager();
    $pager->pageSize = 500;

    $thumbResult = $client->thumbAsset->listAction($filter, $pager);

    $thumbJson = "[";
    $first = true;
    foreach ($thumbResult->objects as $thumbNail) {
        $thumbItem = new PlayerThumbNail();
        $thumbItem->SetData($thumbNail->id, $client->thumbAsset->getUrl($thumbNail->id, 0, new KalturaThumbParams()), $thumbNail->height, $thumbNail->width, $thumbNail->description);

        if ($first) {
            $thumbJson = $thumbJson . json_encode($thumbItem);
            $first = false;
        }
            else
        {
            $thumbJson =$thumbJson . ",".json_encode(($thumbItem));
        }
    }

    $rtnValue = json_decode($thumbJson . "]", true);
}
catch (Exception $e)
{
    error_log("Kaltura.API.Thumbnail: " . $e->getMessage());
    http_response_code(500);
    exit();


}

header("Access-Control-Allow-Origin: *");
header('Content-type: application/json');
echo json_encode($rtnValue);







